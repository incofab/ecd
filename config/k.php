<?php
use Philo\Blade\Blade;

errorhandling();

function view($view = null, array $data = []) {
	// Initialize blade engine
	$views_dir = __DIR__ . '/../resources/views';  
	$cache_dir = __DIR__ . '/../resources/cache';
	
	$blade = new Blade($views_dir, $cache_dir);
	
	$content = $blade->view()->make(str_ireplace('/', '.', $view), $data)->render();
	
	return $content;
}

function redirect_($to) 
{
	if(empty($to)) $to = $_SERVER['REQUEST_URI'];
	
	header("Location: $to");
	
	exit();
} 

/**
 * The URL from where the request is coming from
 */
function getReferer() { return $_SERVER['HTTP_REFERER']; }

function assets($pathAfterPublic, $dontAddVersion = false) 
{
    $file = ADDR . "public/" . ltrim($pathAfterPublic) 
        . ($dontAddVersion ? '' : ('?version=' . APP_VERSION));
    
	return $file;
}

function getRandomAvatar()
{
	$dir = APP_DIR . '../public/img/avatar';

	$avatars = scandir($dir);
	
	$value = $avatars[array_rand($avatars)];
	
	while (empty(pathinfo($value, PATHINFO_EXTENSION))) 
	{
		$value = $avatars[array_rand($avatars)];
	}
	
	return assets("img/avatar/$value");
}

function getProfileImage($profileImgName) 
{
	
	if(empty($profileImgName)) 
	{
		return getRandomAvatar();
	}
	
	return assets("img/profile/$profileImgName");
}

function markSelected($option, $value, $stringToReturn = ' selected ') 
{
	if($option == $value) return $stringToReturn;
	
	return '';
}

/**
 * Checks if key exists in the given array
 * @param array $post
 * @param string $key
 * @return mixed the value if true, returns default value otherwise
 */
function getValue($post, $key, $default = NULL) 
{
	return (isset($post[$key]) ? $post[$key] : $default);
}
/**
 * Removes malicious tags from HTML text
 * @param string $html
 * @return string a filtered html string
 */
function purifyHTML($html)
{
	return $html;
}

function generatePin($numOfPins) 
{
	return \App\Core\CodeGenerator::generateRandomDigits($numOfPins);
}

function cryptPassword($password) {
// 	$salt = '$2a$07$' . substr(md5(uniqid(rand(), true)), 0, 22);
// 	return crypt($password, $salt);	
	return password_hash($password, PASSWORD_BCRYPT);
}
/**
 * Compares both passwords
 * @param unknown $hashedPassword
 * @param unknown $userInput
 * @return boolean true if both passwords match, false otherwise
 */
function comparePasswords($hashedPassword, $userInput) {
	return hash_equals($hashedPassword, crypt($userInput, $hashedPassword));
}

function errorhandling() {
	if(DEV) return;
	set_error_handler("myErrorHandler");
	return;
// 	if(DEV){
// 		ini_set("display_errors" , true );
// 		ini_set('display_start_up_errors', true);
// 		error_reporting(E_ERROR);
// 	}else{
// 		set_error_handler("myErrorHandler");
// 	}
}

function formatValidationErrors($errorsContainer, $asHTML = false) 
{
	if(!is_array($errorsContainer) || empty($errorsContainer)) return [];
	
	$errorArr = [];
	$html = '<div class="text-left">';
	foreach ($errorsContainer as $errors) 
	{
	    foreach ($errors as $error) 
	    {
    	    $errorArr[] = $error;
    	    
	        $html .= '<p><i class="fa fa-star"></i> ' . $error . '</p>';
	    }
	}
	$html .= '</div>';
	return $asHTML ? $html : $errorArr;
}
/**
 * Displays all the validation errors returned by the form validator
 * @param unknown $errors
 */
function showFormValidationErrors($errors) {
	if(!is_array($errors)) return;
	$str = '<p>';
	foreach ($errors as $error) {
		$str .= "<p class='error alert alert-danger'>$error[0]</p>";
	}
	$str .= '</p><br />';
	echo $str;
}

/**
 * get access token from header
 * */
function getBearerToken() {

	/**
	 * Get hearder Authorization
	 * */
	function getAuthorizationHeader(){
		$headers = null;
		if (isset($_SERVER['Authorization'])) {
			$headers = trim($_SERVER["Authorization"]);
		}else if (isset($_SERVER['HTTP_AUTHORIZATION'])) { //Nginx or fast CGI
			$headers = trim($_SERVER["HTTP_AUTHORIZATION"]);
		} elseif (function_exists('apache_request_headers')) {
			$requestHeaders = apache_request_headers();
			// Server-side fix for bug in old Android versions (a nice side-effect of this fix means we don't care about capitalization for Authorization)
			$requestHeaders = array_combine(array_map('ucwords', array_keys($requestHeaders)), array_values($requestHeaders));
			//print_r($requestHeaders);
			if (isset($requestHeaders['Authorization'])) {
				$headers = trim($requestHeaders['Authorization']);
			}
		}
		return $headers;
	}

	$headers = getAuthorizationHeader(); 
	// HEADER: Get the access token from the header
	if (!empty($headers)) {
		if (preg_match('/Bearer\s(\S+)/', $headers, $matches)) {
			return $matches[1];
		}
	}
	return null;
}
function getAllLevels() {
	return [
			LEVEL_1, LEVEL_2, LEVEL_3, LEVEL_4, 
			LEVEL_5, LEVEL_6, LEVEL_7, LEVEL_8, 
			LEVEL_9, LEVEL_10, LEVEL_11, LEVEL_12, 
			LEVEL_13, LEVEL_14, LEVEL_15, LEVEL_16, 
			LEVEL_17, LEVEL_18, LEVEL_19, LEVEL_20, 
	];
}
function getLevel($levelNo) {
	$allLevels = getAllLevels();
	if(empty($allLevels[$levelNo - 1])) killPage('Invalid level number selected');
	
	return $allLevels[$levelNo - 1];
}

function myErrorHandler($errno, $errstr, $errfile, $errline) {

	$stackTrace = debug_backtrace();
	// Just take the first 3 in the stack trace
	$stackTrace = array_slice($stackTrace, 0, 3);
	
	$errorArr = [
			'error No' => $errno,
			'Error Message' => $errstr,
			'File' => $errfile,
			'Line Number' => $errline,
			'Stack Trace' => $stackTrace,
	];

	dlog($errorArr);

	$msg = [
			'message' => 'A fatal error occured. Please contact administrator',
			'success' => false,
	];
	
	switch ($errno) {
		case E_ERROR:
		case E_USER_ERROR:
			exit(json_encode($msg, JSON_PRETTY_PRINT));
// 			exit("FATAL error $errstr at $errfile:$errline");
			break;
	}
	
}

function getAddr($routeName = null, $params = '') 
{
    $options = '';
    
    if (is_array($params) && !empty($params))
    {
        $options = '/' . implode('/', $params);
    }
    else
    {
        $options = $params ? "/$params" : '';
    }
    
    if(empty($routeName)) return rtrim($_SERVER['REQUEST_URI'], '/') . $options;
    
    global $routesArr, $routesArrAPI;
    
    if (!empty($routesArr[$routeName]) || !empty($routesArrAPI[$routeName]))
    {
        $routeAddr = !empty($routesArr[$routeName]) ? $routesArr[$routeName] : $routesArrAPI[$routeName];
        
        return rtrim(ADDR, '/') . $routeAddr . $options;
    }
    else 
    {
        throw new Exception("Route: ($routeName) not found in route collections");
    }
}
	

function dDie($param) 
{
	echo '<pre>'; print_r($param); die();
}
function dEcho($param) 
{
    echo '<pre>'; 
	
	print_r($param);
	
	echo '</pre> <br />';
}
function dlog($msg) {
	$str = '';
	
	if (is_array($msg)) $str = json_encode($msg, JSON_PRETTY_PRINT);
	
	else $str = $msg;

	error_log(
			'*************************************' . K_NEWLINE .
			'     Date Time: ' . date('Y-m-d h:m:s') . K_NEWLINE .
			'------------------------------------' . K_NEWLINE .
			$str . K_NEWLINE . K_NEWLINE .
			'*************************************' . K_NEWLINE,
				
			3, APP_DIR . '../public/errorlog.txt');
	
}

function errorlog($msg) {
	$str = '';
	
	if (is_array($msg)) $str = json_encode($msg, JSON_PRETTY_PRINT);
	
	else $str = $msg;
	
	error_log('------------------------------------' . K_NEWLINE .
			
			$str . K_NEWLINE . '------------------------------------' . K_NEWLINE,
			
			3, APP_DIR . '../public/errorlog.txt');
	
}

	