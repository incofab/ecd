
@extends('ccd.layout')

@section('content')

	 <div class="templatemo-content-widget white-bg ">
	
	 @include('ccd.common.message')
		
	 	<div class="panel-heading templatemo-position-relative">
	 	
	 		<h2>{{ $courseName }} Summary for Chapter <?= $previewData[CHAPTER_NO] ?></h2>
	 		
			<div>
				<label for="" >Title</label><br />
				<label for=""><?= $previewData[TITLE] ?></label>
			</div>
			<div>
				<label for="" >Description</label><br />
				<label for=""><?= $previewData[DESCRIPTION] ?></label>
			</div>
			<br />	 		
			<div >
				<label for="" >Chapter Summary</label><br />
				<div class="text-body">
					<?= $previewData[SUMMARY] ?>
					<div class="clearfix"></div>
				</div>
			</div> 		 		
	 	</div>
		
		<a class="templatemo-blue-button width-20" href="<?= getAddr('ccd_edit_chapter', 
							[$courseName, $previewData[TABLE_ID]]) ?>">Edit</a>


		
	</div>

@stop