
@extends('ccd.layout')

@section('content')

<div class="templatemo-content-widget white-bg" >
	<div class="clearfix">
		 <a href="<?= getAddr('ccd_register_course') ?>" class="templatemo-blue-button width-20 pull-left" >
				<span>Register New</span>					
		 </a>
		 <?php /*
		 <a href="<?= getAddr('ccd_upload_content') ?>" class="templatemo-blue-button width-20 pull-right" >
				<span>Upload Content</span>					
		 </a>
		 */?>
	</div>
	<br />
	<div id="table_nav">
    	<div class="row">
    		<div class="col-sm-6">
        	 	<p class="form-group">
        			<span class="small">Search</span>&nbsp;&nbsp;
        				<input type="text" placeholder="search table" class="form-control" 
        					onkeyup="filter_table(this, 'table_')" />
        	 	</p>
    	 	</div>
    		<div class="col-sm-6">
        	 	<p class="form-group">
        			<span class="small">No. of Items per Page</span>
        			<select name="" id="select_num_of_rows" class="form-control"
        				onchange="paginate.reArrangePage(this, 'table_', 'paginate_button')">
        				<option >10</option><option >20</option>
        				<option >30</option><option >40</option>
        				<option >50</option><option >60</option>
        			</select>
        		</p>
        	</div>
		</div>
    	<div class="panel panel-default templatemo-content-widget white-bg no-padding templatemo-overflow-hidden">
         	<div class="panel-heading templatemo-position-relative">
         		<h2 class="text-uppercase">All registered courses</h2>
         	</div>
    		<table class="table table-responsive table-striped table-bordered" id="table_">
    			<thead>
    				<tr>
    					<td><b>No.</b></td>
                        <td><b>Course Code</b></td>
                        <td><b>Course Fullname</b></td>
                        <td><b>Description</b></td>
                        <td><b>Preview</b></td>
                        <td><b>Course Summary</b></td>
                        <td><b>Edit</b></td>
                        <td><b>Delete</b></td>
    				</tr>
    			</thead>
    			<tbody>
    			<?php $i = 0;  ?>
    			@foreach($allRegdCourses as $regdCourse) 
				<tr> 
					<td><?= ++$i; ?></td> 
					<td><?= $regdCourse[COURSE_CODE] ?></td>
					<td><?= $regdCourse[COURSE_TITLE] ?></td>
					
					<td title="<?= $regdCourse[DESCRIPTION] ?>" ><?= (strlen($regdCourse[DESCRIPTION]) > 20) 
							? substr($regdCourse[DESCRIPTION], 0, 20) . '...' 
							: $regdCourse[DESCRIPTION] ?></td>
							
					<td><a href="<?= getAddr('ccd_all_sessions', [$regdCourse[COURSE_CODE]]) ?>">Questions</a></td>
				
					<td><a href="<?= getAddr('ccd_view_all_chapter', [$regdCourse[COURSE_CODE]]) ?>" 
							>View Course Summary</a></td>
				
					<td><a href="<?= getAddr('ccd_edit_course', [$regdCourse[TABLE_ID]]) ?>">Edit</a></td>
					<td><a href="<?= getAddr('ccd_delete_course', [$regdCourse[TABLE_ID]]) ?>"
								onclick="return confirm('WARNING: This will delete this course and every questions/summary recorded under it. Continue?')"
									>Delete</a></td>
				</tr>
    			@endforeach
    			</tbody>
    		</table>   
    		<!-- Load pagination and it's buttons -->   
    		<br />
    		<div id="paginate_button">
    			<script type="text/javascript">
    			window.onload = function() {
    				paginate.init('table_', 'paginate_button');
    			}
    			</script>
    		</div> 
    		<br />	 
    	</div>
    </div>
</div>
@stop


