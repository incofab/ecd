<?php

$uploadURL = getAddr('ccd_upload_question_images', [$courseName, $year_id]);

// dDie($post->toArray());
?>

@extends('ccd.layout')

@section('content')
<style>
    #num-div{
        position: fixed; top: 0; right: 0; 
        background-color: #333333aa;
        color: #efefef;
        font-weight: bold;
        font-size: 1.5em;
        padding: 5px 15px;
    }
</style>
	<div id="num-div">No: <span id="num">{{$num}}</span></div>
	<div class="templatemo-content-widget white-bg ">
		<div class="row">
			<div class="col-sm-3">
                <a href="<?= getAddr('ccd_all_session_questions', [$courseName, $year_id]) ?>" 
                 	class="templatemo-blue-button width-20" title="Go back to question listings" >
                		<i class="fa fa-arrow-left"></i> <span>Back</span>					
                </a>
			</div>
			<div class="col-sm-9 clearfix">
				<div class="pull-right">
					@include('ccd.courses._list_topics')
				</div>
			</div>
		</div>
        <br />
        <br />
        <header class="text-center">
        	<h2>Enter Question</h2><hr />
        </header>
		<?= (empty($errorMsg) ? '' : "<p class=\"report\">$errorMsg</p><hr />" ) ?>
		<form action="" method="post" name="record-question" id="record-question-form">
			<?= (empty($errors[QUESTION_NO]) ? '' : "<p class=\"report\">{$errors[QUESTION_NO][0]}</p>" ) ?>
			<div class="row">
				<div class="col-sm-6">
        			<div class="form-group">
        				<label for="" >No: </label>
        				<input type="number" name="<?= QUESTION_NO ?>" value="<?= $num ?>" id="question-no" 
        					onchange="questionNumberChanged(this)" required="required" class="form-control w-25"/>
        			</div>
				</div>
				<div class="col-sm-6">
        			<div class="form-group">
        				<label for="" >Topic: </label>
        				<input type="text" name="topic_title" value="<?= getValue($post, 'topic_title')?>" readonly="readonly" class="form-control"/>
        				<input type="hidden" name="<?= TOPIC_ID ?>" value="<?= getValue($post, TOPIC_ID)?>" class="form-control" />
        			</div>
				</div>
			</div>
			<?= (empty($errors[QUESTION]) ? '' : "<p class=\"report\">{$errors[QUESTION][0]}</p>" ) ?>
			<div class="form-group">
				<label for="" >Question</label><br />
				<textarea name="<?= QUESTION ?>" id="" cols="60" rows="6" class="useEditor" 
					><?= getValue($post, QUESTION)?></textarea>
			</div>
			<br/>
			<div class="form-group row">
				<div class="col-md-6">
					<?= (empty($errors[OPTION_A]) ? '' : "<p class=\"report\">{$errors[OPTION_A][0]}</p>" ) ?>
					<label for="" >A</label><br />
					<textarea name="<?= OPTION_A ?>" cols="30" rows="3" class="useEditor"
						><?= getValue($post, OPTION_A)?></textarea>
				</div>
				<div class="col-md-6">
					<?= (empty($errors[OPTION_B]) ? '' : "<p class=\"report\">{$errors[OPTION_B][0]}</p>" ) ?>
					<label for="" >B</label><br />
					<textarea name="<?= OPTION_B ?>" cols="30" rows="3"  class="useEditor"
						><?= getValue($post, OPTION_B)?></textarea>
				</div>
			</div> 
			<br/>
			<div class="form-group row">
				<div class="col-md-6">
					<?= (empty($errors[OPTION_C]) ? '' : "<p class=\"report\">{$errors[OPTION_C][0]}</p>" ) ?>
					<label for="" >C</label><br />
					<textarea name="<?= OPTION_C ?>" cols="30" rows="3" class="useEditor"
						><?= getValue($post, OPTION_C)?></textarea>
				</div>
				<div class="col-md-6">
					<?= (empty($errors[OPTION_D]) ? '' : "<p class=\"report\">{$errors[OPTION_D][0]}</p>" ) ?>
					<label for="" >D</label><br />
					<textarea name="<?= OPTION_D ?>" cols="30" rows="3" class="useEditor" ><?= getValue($post, OPTION_D)?></textarea>
				</div>
			</div> 
			<br/>
			<div class="form-group row">
				<div class="col-md-6">
					<?= (empty($errors[OPTION_E]) ? '' : "<p class=\"report\">{$errors[OPTION_E][0]}</p>" ) ?>
					<label for="" >E</label><br />
					<textarea name="<?= OPTION_E ?>" cols="30" rows="3" class="useEditor"><?= getValue($post, OPTION_E)?></textarea>
				</div>
			</div> 
			<br/>
			<?= (empty($errors[ANSWER]) ? '' : "<p class=\"report\">{$errors[ANSWER][0]}</p>" ) ?>
			<div class="form-group">
				<label for="" >Select Answer: &emsp;</label>
				<select name="<?= ANSWER ?>" id="" required="required" class="form-control w-25">
					<option value="">select Answer</option>
					<option  <?= (getValue($post, ANSWER) == 'A') ? 'selected="selected"' : '' ?> >A</option>
					<option  <?= (getValue($post, ANSWER) == 'B') ? 'selected="selected"' : '' ?> >B</option>
					<option  <?= (getValue($post, ANSWER) == 'C') ? 'selected="selected"' : '' ?> >C</option>
					<option  <?= (getValue($post, ANSWER) == 'D') ? 'selected="selected"' : '' ?> >D</option>
					<option  <?= (getValue($post, ANSWER) == 'E') ? 'selected="selected"' : '' ?> >E</option>
				</select>
			
			</div>
			 <br/>
			<?= (empty($errors[ANSWER_META]) ? '' : "<p class=\"report\">{$errors[ANSWER_META][0]}</p>" ) ?>
			<div class="form-group">
				<label for="" >Explanation of the answer [optional]</label><br />
				<textarea name="<?= ANSWER_META ?>" id="" cols="60" rows="6" class="useEditor"><?= getValue($post, ANSWER_META)?></textarea>
			</div> 
			<br /> 
			<div class="form-group">
				@if(isset($edit))
					<input type="hidden" name="update_question" value="true" />
					<input type="hidden" name="<?= TABLE_ID ?>" value="<?= $table_id ?>" />
				@else
					<input type="hidden" name="add_new_question" value="true" />
				@endif
				<input type="hidden" name="<?= SESSION_ID ?>" value="<?= $year_id ?>" />
				<input type="hidden" name="<?= COURSE_CODE ?>" value="<?= $courseName ?>" />
				@if(!empty($next))
				<input type="hidden" name="next" value="<?= $next ?>" />
				@endif
				<input type="submit" value="{{isset($edit) ? 'Update' : 'submit'}}" class="templatemo-blue-button width-20" /><br /><br />
			</div>
		</form>
	</div>
	@include('ccd.common.tinymce')
@if(!isset($edit))
<script type="text/javascript">
	var currentQuestionNo = {{$num}};
	$('form[name="record-question"]').on('submit', function(e) {
		if(!confirm('Are you sure?')) return false;
		tinyMCE.triggerSave();
		showLoading();
		$.ajax({
		    type: "POST",
		    url: "{{getAddr('ccd_new_session_question_api')}}",
		    data: $(this).serialize(),
		    dataType: "json",
		    success: function(res) {
		    	dismissLoading();
			    if(!res.success) return alert(res.message);
			    currentQuestionNo++;
			    clearForm(currentQuestionNo);
			    $('body, html').animate({scrollTop: 0}, 800);
		    },
		    error: function(jqHRX, textStatus) {
		    	dismissLoading();
		    	alert("Request failed: " + textStatus + ' : ' + jqHRX.responseText);
		    	console.log(jqHRX);
		    	console.log(textStatus);
		        dismissLoading();
		    }
		});
		return false;
	});
	function clearForm(currentQuestionNo) {
		tinyMCE.editors.forEach(function(editor){
			editor.setContent('');
		});
		$('form[name="record-question"] .form-control').val('');
		$('form[name="record-question"] #question-no').val(currentQuestionNo).trigger('change');
		if(window.resetDropDown) resetDropDown();
	}

	function questionNumberChanged(obj) {
		$('#num').text($(obj).val());
	}

</script>
@endif
			
@stop