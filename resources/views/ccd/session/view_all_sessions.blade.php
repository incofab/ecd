
@extends('ccd.layout')

@section('content')

	 <div class="templatemo-content-widget white-bg ">
	
	 <br />
	
	 <a href="<?= getAddr('ccd_register_session', $courseName) ?>" class="templatemo-blue-button width-20" >
			<span>Add New Session</span>					
	</a>
	<br class="clear" />
	<br />
	<br />
		
	<div id="table_nav">
	<div class="row">
		<div class="col-sm-6">
    	 	<p class="form-group">
    			<span class="small">Search</span>
				<input type="text" placeholder="search table" class="form-control" onkeyup="filter_table(this, 'table_')" />
    	 	</p>
		</div>
		<div class="col-sm-6">
    	 	<p class="form-group">
    			<span class="small">No. of Items per Page</span>
    			<select name="" id="select_num_of_rows" class="form-control"
    				onchange="paginate.reArrangePage(this, 'table_', 'paginate_button')">
    				<option >10</option><option >20</option>
    				<option >30</option><option >40</option>
    				<option >50</option><option >60</option>
    			</select>
    		</p>
		</div>
	</div>
	</div>
	
	 
	<div class="panel panel-default templatemo-content-widget white-bg no-padding templatemo-overflow-hidden">
		
 	<div class="panel-heading templatemo-position-relative">
 		<h2 class="text-uppercase">Available Sessions for {{$courseName}}</h2>
 	</div>
	<div class="table-responsive">
		<table class="table table-striped table-bordered" id="table_">
			<thead>
				<tr>
					<td><b>No.</b></td>
                    <td><b>Year</b></td>
                    <td><b>General Instructions</b></td>
                    <td><b>Edit</b></td>
                    <td><b>Questions</b></td>
                    <td><b>Delete</b></td>
                    <td><b>Preview</b></td>
				</tr>
			</thead>
			
			<tbody>
			 
			<?php $i = 0;  ?>
			@foreach($allCoursesYears as $course)
			 
				<tr> 
					<td><?= ++$i; ?></td> 
					<td><?= $course[SESSION] ?></td>
					
					<td title="<?= $course[GENERAL_INSTRUCTIONS] ?>" ><?= (strlen($course[GENERAL_INSTRUCTIONS]) > 20) 
							? substr($course[GENERAL_INSTRUCTIONS], 0, 20) . '...' 
							: $course[GENERAL_INSTRUCTIONS] ?></td>
							
					<td><a href="<?= getAddr('ccd_edit_session', [$courseName, $course[TABLE_ID]]) ?>">Edit</a></td>
					<td><a href="<?= getAddr('ccd_all_session_questions', [$courseName, $course[TABLE_ID]]) ?>">Record Questions</a></td>
					<td><a href="<?= getAddr('ccd_preview_session', [$courseName, $course[TABLE_ID]]) ?>">Preview</a></td>
					<td><a href="<?= getAddr('ccd_delete_session', [$courseName, $course[TABLE_ID]]) ?>"
								onclick="return confirm('Are you sure?')" >Delete</a></td>
					
				</tr>
				
			@endforeach
			                  
			</tbody>
			
		</table>   
		<!-- Load pagination and it's buttons -->   
		<br />
		<div id="paginate_button">
			<script type="text/javascript">
			window.onload = function() {
				paginate.init('table_', 'paginate_button');
			}
			</script>
		</div> 
		<br />	 
	</div> 
	</div>

	</div>

@stop