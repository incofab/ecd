
@extends('ccd.layout')

@section('content')

<div class="templatemo-content-widget white-bg">
 	<div class="panel-heading templatemo-position-relative">
 		<h2>{{ $courseName }} questions for the session {{ $session }}</h2>
 	</div>
	@foreach($allCourseYearQuestions as $questionObj)
		@include('ccd.common.preview_single_question')
	@endforeach
</div>

<script type="text/javascript">
var imgBaseDir = '<?= ADDR.\App\Parser\GenericParse::IMG_OUTPUT_PUBLIC_PATH; ?>';
$(function(){
	$('.question-container').find('img').each(function(i, ele) {
		var $img = $(ele);
    	var src = $img.attr('src');
    	var imgPath = imgBaseDir+'{{$courseName}}/{{$year}}/'+src;
    	$img.attr('src', imgPath);
	});
});
</script>
@stop