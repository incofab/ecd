
<!-- ***** Awesome Features Start ***** -->
<section class="awesome-feature-area bg-white section_padding_0_50 clearfix" id="developers" >
    <div class="container">
        <div class="row">
            <div class="col-12">
                <!-- Heading Text -->
                <div class="section-heading text-center">
                    <h2>API And Website Integration</h2>
                    <div class="line-shape"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <!-- Single Feature Start -->
            <div class="col-12 col-sm-6 col-lg-4">
                <div class="single-feature">
<!--                     <i class="ti-palette" aria-hidden="true"></i> -->
                    <img src="{{assets('img/icons/link.png')}}" alt="API Gateway" class="img-icon" />
                    <h5>API Gateway</h5>
                    <p>
                    	Developers and Site owners can integrate <b class="sit-title">{{SITE_TITLE}}</b> 
                    	to your website using our APIs and start receiving payments with airtime. 
                    	Integration is easy, click on the Developers section in the user dashboard 
                    	to generate your API credentials. <br />
                    	Goto to <a href="https://github.com/cheetahpay/Cheetapay">github repo</a> for API Documentation.
                    	For more enquiries, contact our customer support using the phone number above.
                    </p>
                </div>
            </div>
            <!-- Single Feature Start -->
            <div class="col-12 col-sm-6 col-lg-4">
                <div class="single-feature">
<!--                     <i class="ti-crown" aria-hidden="true"></i> -->
                    <img src="{{assets('img/icons/wordpress-o.png')}}" alt="WordPress Integration" class="img-icon" />
                    <h5>Wordpress Integration</h5>
                    <p>
                    	Wordpress/Woocommerce websites can include <b class="sit-title">{{SITE_TITLE}}</b>
                    	as a payment option. <br />
                    	To achieve this, generate your API credentials in the developers section
                    	of your dashboard. Then, download our 'woocommerce cheetahpay plugin' from
                    	wordpress store, setup your your plugin with the API credentials 
                    	and you are good to go.
                    	For more enquiries, contact our customer support using the phone number above.
                    </p>
                </div>
            </div>
            <!-- Single Feature Start -->
            <div class="col-12 col-sm-6 col-lg-4">
                <div class="single-feature">
<!--                     <i class="ti-headphone" aria-hidden="true"></i> -->
                    <img src="{{assets('img/icons/user.png')}}" alt="Personal Page" class="img-icon" />
                    <h5>Personal Page</h5>
                    <p>
                    	if you do not have a website but wish to receive airtime payment 
                    	and donations from users. <b class="site-title">{{SITE_TITLE}}</b>
                    	got you covered. A page is generated for you where the donors can enter
                    	the airitme details and you account will be credited.  
                    </p>
                </div>
            </div>
        </div>

    </div>
<style>

</style>
</section>
<!-- ***** Awesome Features End ***** -->
