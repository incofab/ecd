<?php 
$banks = \App\Core\Settings::BANKS; 
?>


<div class="form-group px-5">
	<label for="">Account Name</label>
	<input type="text" name="{{ACCOUNT_NAME}}" class="form-control" value="<?= array_get($post, ACCOUNT_NAME)?>" placeholder="Enter Name" required="required" />
</div>
<div class="form-group px-5">
	<label for="">Account Number</label>
	<input type="text" name="{{ACCOUNT_NUMBER}}" class="form-control" value="<?= array_get($post, ACCOUNT_NUMBER)?>" placeholder="Enter Account No" required="required" />
</div>
<div class="form-group px-5" >
	<label >Bank</label>
	<select name="<?= BANK_NAME ?>" id="" class="form-control" required="required">
		<option value="">select bank</option>
		@foreach($banks as $bank)
		<option <?= markSelected(array_get($post, BANK_NAME), $bank)?>>{{$bank}}</option>
		@endforeach
	</select> 
</div>
