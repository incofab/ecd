<?php 
$title = 'Activation | ' . SITE_TITLE;
$error = isset($error) ? $error : \Session::getFlash('error');
?>

@extends('home.layout')

@section('content')

<style>
    #activation_page{
        background-color: white;
        min-height: 380px;
    }
</style>

<div id="activation_page" class="row">
<br /><br />
	<div class="col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3" >
		<h3 class="text-center">Account Activation</h3>
		<form method="POST" action="" name="activation">
    		@if($error)
    		<div class="row">
        		<div class="alert col-sm-offset-2">
        			<p><i class="fa fa-star" style="color: #cc4141;"></i> {{$error}}</p>
        		</div>
    		</div>
    		<br />
    		@endif
			<br />
			<div class="form-group" >
				<label for="">Activation Code</label>
				<input type="number" name="<?= ACTIVATION_CODE ?>" placeholder="A 6 digit code" 
					required class="form-control" >
			</div>
			
			<div class="form-group">
				If you do not receive the message in 10 mins, 
				<a href="{{getAddr('home_resend_activation') . '?activation_token=' . $activationToken }}"
					>click here to resend</a>
			</div>
						
			<input type="hidden" name="activate_account" value="true" />
			<input type="submit"  name="activate" class="btn btn-primary pull-right" value="Activate" >
	
		</form>
		<br />
		<br />
	</div>
</div>
		

@endsection