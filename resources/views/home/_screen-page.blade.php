
<!-- ***** Wellcome Area Start ***** -->
<section class="wellcome_area clearfix bg-white" id="screen-page" 
	style="background-position: center; background-size: cover; background-image: url({{assets('img/images/welcome-bg.png')}})">
    <div class="container">
        <div class="row mx-0 text-center center-message mt-4">
        	<div class="col-12 col-md-5">
        		<div class="d-block d-md-none">
	            	<h3 class="slogan">Airtime is Money</h3>
        			<div class="row mx-0 mb-2">
	            		<div class="col-7 p-0">
        	            	<div class="sing-up-button text-center my-2">
                                <a href="{{getAddr('register_user')}}" class="btn submit-btn no-box-shadow">Sign up</a>
                            </div>
	            		</div>
	            		<div class="col-5 p-0">
        	            	<div class="sing-up-button text-center my-2">
                                <a href="{{getAddr('user_login')}}" class="btn submit-btn no-box-shadow">Sign in</a>
                            </div>
	            		</div>
	            	</div>
        		</div>
            	<div class="d-none d-md-block align-center">
	            	<h3 class="slogan">Airtime is Money</h3>
	            	<div class="sing-up-button d-block text-center my-2">
                        <a href="{{getAddr('user_login')}}" class="btn submit-btn no-box-shadow">Sign in</a>
                    </div>
        			<div class="sing-up-button d-block text-center my-2">
                        <a href="{{getAddr('register_user')}}" class="btn submit-btn no-box-shadow">Sign up</a>
                    </div>
	            	<div class="slogan-message">Easily convert your airtime to cash at just <b>{{PERCENT_CHARGE}}%</b>.</div>
            	</div>
            </div>
        	<div class="col-12 col-md-7">
        		<div  id="anim-box" class="position-relative"> 
                	<div id="all-networks-box" class="mt-2">
                		<img src="{{assets('img/images/all-networks.png')}}" alt="" id="all-networks" class="skew img-fluid w-100" />
                	</div>
                	<div id="features-list">
                		<div class="item">Convert airtime to cash at just <span class="h3">{{PERCENT_CHARGE}}%</span></div>
                		<div class="item">Buy and sell goods online with airtime</div>
                		<div class="item">Integrate your website with our API</div>
                		<div class="item">Bank Payout is instant and free</div>
                		<div class="item">It is fast, easy and <b>Cheetah!!!</b></div>
                    	<div id="feature-box-signup">
			            	<a href="{{getAddr('register_user')}}" 
			            		class="btn submit-btn no-box-shadow animated flash">Sign Up Now</a>
                    	</div>
                	</div>
                </div>
        	<?php /*
                <div class="airtime-cash wow fadeInDown" data-wow-delay="0.5s">
                    <img src="{{assets('img/images/airtime-to-cash.png')}}" alt="" class="img-fluid w-100">
                </div>
               */ ?>
        	</div>
        	<div class="col-12 col-md-8 offset-md-2 col-lg-6 offset-lg-0 mt-3">
        		<div class="d-block d-md-none slogan-message" >Easily convert your airtime to cash at just <b>{{PERCENT_CHARGE}}%</b>.</div>
        	</div>
        </div>
    </div>
    <!-- Welcome thumb -->
    <div class="welcome-thumb wow fadeInDown" data-wow-delay="0.5s">
        <img src="img/bg-img/welcome-img.png" alt="">
    </div>
</section>
<!-- ***** Wellcome Area End ***** -->
<style>
#screen-page{overflow: hidden;}
#screen-page .submit-btn{
    height: auto;
    line-height: normal;
    min-width: auto;
    padding: 8px 25px;
}
.center-message, .align-center{
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    width: 100%;
}
.slogan{
    font-size: 2rem;
    color: #fff;
    text-shadow:0px 2px 3px #f8055a; 
}
.slogan-message{ font-weight: 500; color: #fff; }
.airtime-cash{ }

#anim-box{
    overflow: hidden;
    text-align: center;
}
#features-list{
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    overflow: hidden;
}
#features-list #feature-box-signup > .btn{
    animation-iteration-count: infinite;
}
#features-list #feature-box-signup {
    border-radius: 22px 25px 22px 0;
    padding: 3px !important;
    background:-webkit-linear-gradient(0deg, rgb(250, 31, 107) 0%, rgb(252, 103, 155) 80%);
    background:-o-linear-gradient(0deg, rgb(250, 31, 107) 0%, rgb(252, 103, 155) 80%);
    background:-moz-linear-gradient(0deg, rgb(250, 31, 107) 0%, rgb(252, 103, 155) 80%);
    background:linear-gradient(0deg, rgb(250, 31, 107) 0%, rgb(252, 103, 155) 80%);
}
#features-list > .item, #features-list #feature-box-signup {
    display: none;
//    background-color: #f8055a;
    color: #fff;
    font-weight: bold;
    padding: 15px 10px;
    font-size: 1.4rem;
    text-shadow:0px 2px 3px #f8055a;
}
#features-list > .item.show{
    display: block;
    -webkit-animation:featuresAnim 5s ease 0s 1 forwards;
    -moz-animation:featuresAnim 5s ease 0s 1 forwards;
    -ms-animation:featuresAnim 5s ease 0s 1 forwards;
    animation:featuresAnim 5s ease 0s 1 forwards;
    
}
@-webkit-keyframes featuresAnim{
    0%{ -webkit-transform: scale(0); transform: scale(0); }
    30%{ -webkit-transform: scale(1); transform: scale(1); }
    80%{ -webkit-transform: scale(1); transform: scale(1); }
    100%{ -webkit-transform: translateX(-100%); transform: translateX(-100%); }    
}
@keyframes featuresAnim{
    0%{ -ms-transform: scale(0); transform: scale(0); }
    30%{ -ms-transform: scale(1); transform: scale(1); }
    80%{ -ms-transform: scale(1); transform: scale(1); }
    100%{ -ms-transform: translateX(-100%); transform: translateX(-100%); }
}
.skew{
    -webkit-animation:skew 20s linear 0s 5 normal;
    -moz-animation:skew 20s linear 0s 5 normal;
    -ms-animation:skew 20s linear 0s 5 normal;
    animation:skew 20s linear 0s 5 normal;
}
@-webkit-keyframes skew {
    0%{ -webkit-transform: skew(0deg, 0deg); transform: skew(0deg, 0deg); }
    25%{ -webkit-transform: skew(-30deg, 0deg); transform: skew(-30deg, 0deg); }
    50%{ -webkit-transform: skew(0deg, 0deg); transform: skew(0deg, 0deg); }
    75%{ -webkit-transform: skew(30deg, 0deg); transform: skew(30deg, 0deg); }
    100%{ -webkit-transform: skew(0deg, 0deg); transform: skew(0deg, 0deg); }
}
@keyframes skew {
    0%{ -ms-transform: skew(0deg, 0deg); transform: skew(0deg, 0deg); }
    25%{ -ms-transform: skew(-30deg, 0deg); transform: skew(-30deg, 0deg); }
    50%{ -ms-transform: skew(0deg, 0deg); transform: skew(0deg, 0deg); }
    75%{ -ms-transform: skew(30deg, 0deg); transform: skew(30deg, 0deg); }
    100%{ -ms-transform: skew(0deg, 0deg); transform: skew(0deg, 0deg); }
}

@media (max-width:480px)  {
    .slogan{
        font-size: 1.5rem;
    }
    #screen-page .submit-btn{
        padding: 8px 12px;
    }
    #features-list > .item{
        font-weight: 500;
        padding: 10px 5px;
        font-size: 1.1rem;
    }  
}
</style>

<script type="text/javascript">
var height = $(window).height();
$(document).ready(function() {
	setScreenPageHeight();
	animateFeatures();
});
$(window).on('resize', function(e) {
	height = $(window).height();
	setScreenPageHeight();		
});
function setScreenPageHeight() {
	$('.wellcome_area').css('height', height);		
}
/* ********* Animate Features ********* */
var $featuresItems = $('#features-list .item') 
var iterationCount = 0;
var iterationLimit = 3;
var animItemIndex = 0;
var animItemLen = $featuresItems.length;
function animateFeatures() {
	$featuresItems.removeClass('show');
	if(animItemIndex >= animItemLen){
		if(iterationCount >= iterationLimit){
			$('#feature-box-signup').show();
			 return;
		}
		iterationCount++;
	    animItemIndex = 0;
	}
	$($featuresItems[animItemIndex]).addClass('show');
	animItemIndex++;
}
$featuresItems.on('animationend webkitAnimationEnd MSAnimationEnd oAnimationEnd', function(e) {
	animateFeatures();
});
/* ********* End Animate Features ********* */
</script>	


