@extends('dummy.layout')

@section('dashboard_content')
<style>
#questions-content{
    line-height: 1.8rem;
    font-size: 1.15rem;
}
#questions-content .question-no{
    font-size: 1.05rem;
}
#questions-content .option{
    padding: 5px 0;
}
#questions-content .option-letter{
    font-weight: 500;
    padding-right: 4px;
}
#questions-content .option-text{
    padding-left: 4px;
}
#questions-content .instruction{
    font-weight: bold;
    margin: 10px 0;
}
#questions-content .passage{
    margin: 10px 0;
    padding: 7px 0;
    border-top: 1px solid #e0e0e0;
    border-bottom: 1px solid #e0e0e0;
}
#questions-content .passage:empty, 
#questions-content .passage:blank,
#questions-content .passage:-moz-only-whitespace,
#questions-content .instruction:empty,
#questions-content .instruction:blank,
#questions-content .instruction:-moz-only-whitespace{
    display: none;
}
#questions-content .question-numbers-tab{
    margin-top: 5px;
    padding-top: 10px;
    border-top: 1px solid #e0e0e0;
}
#questions-content .question-numbers-tab > li{
    float: left;
    width: 50px;
    height: 50px;
    border: 1px solid #2f2f2f;
    border-radius: 3px;
    margin: 0 4px 4px 0;
    line-height: 45px;
}
#questions-content .question-numbers-tab > li.current{
    font-weight: bold;
    border-width: 2px;
}
#questions-content .question-numbers-tab > li.attempted{
    background-color: #009688;
    color: #efefef;
    font-weight: 500;
    border-color: #007569;
    border-width: 2px;
}
#questions-content .question-nav{
    position: fixed;
    bottom: 0;
    left: 0;
    right: 0;
/*     background: #0d1214; */
}
#questions-content .next-question,
#questions-content .previous-question{
    border: 1px solid #0d1214;
    border-radius: 0;
}
</style>
<nav>
	<div class="nav nav-tabs" id="nav-tab" role="tablist">
		<a class="nav-item nav-link active"
			data-toggle="tab" href="#nav-home" role="tab"
			>Home</a> 
		<a
			class="nav-item nav-link" data-toggle="tab"
			href="#nav-profile" role="tab" >Profile</a> 
		<a class="nav-item nav-link"
			data-toggle="tab" href="#nav-contact" role="tab"
			>Contact</a>
	</div>
</nav>
<div class="tab-content mt-4" id="questions-content" >
	<div class="tab-pane fade show active" id="nav-home" role="tabpanel" >
		<div class="tile text-center p-1 mb-3">
			<div class="tile-title question-no mb-0">Question 3 of 50</div>
		</div>
		<div class="instruction">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nihil quis.</div>
		<div class="passage">
			Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vero blanditiis natus recusandae eius.
			Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vero blanditiis natus recusandae eius.
			Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vero blanditiis natus recusandae eius.
		</div>
		<div class="question-main">
    		<div>
    			Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nam repudiandae rerum error molestias similique eius fugiat impedit alias illum nemo! Incidunt explicabo sit numquam doloremque. Atque explicabo assumenda officiis accusantium.
    		</div>
    		<div class="options">
    			<div class="animated-radio-button option">
                  <label>
                    <span class="option-letter">A)</span> 
                    <input type="radio" name="option" checked="checked">
                    <span class="label-text"><span class="option-text">Lorem ipsum dolor sit amet.</span></span>
                  </label>
                </div>
    			<div class="animated-radio-button option">
                  <label>
                    <span class="option-letter">B)</span> 
                    <input type="radio" name="option">
                    <span class="label-text"><span class="option-text">Lorem ipsum dolor sit amet.</span></span>
                  </label>
                </div>
    			<div class="animated-radio-button option">
                  <label>
                    <span class="option-letter">C)</span> 
                    <input type="radio" name="option">
                    <span class="label-text"><span class="option-text">Lorem ipsum dolor sit amet.</span></span>
                  </label>
                </div>
    		</div>
    	</div>
	</div>
	<div class="tab-pane fade" id="nav-profile" role="tabpanel" >
		Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eveniet dolores accusantium itaque aliquid porro ab nesciunt ipsa voluptas vel nam magni quae amet quis repellat pariatur recusandae distinctio alias eaque.
	</div>
	<div class="tab-pane fade" id="nav-contact" role="tabpanel" >
		Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolores unde eum nostrum ullam ut a modi illum minus? Explicabo temporibus quasi nobis alias quae deserunt modi nostrum iusto officia repudiandae?
	</div>
	<ul class="question-numbers-tab list-unstyled clearfix text-center">
		<?php for ($i = 1; $i < 50; $i++): ?>
		<li>{{$i}}</li>
		<?php endfor; ?>
	</ul>
	<div class="question-nav text-center clearfix">
		<button class="previous-question btn btn-primary pull-left">&laquo; Previous</button>
		<button class="next-question btn btn-primary pull-right">Next &raquo;</button>
	</div>
</div>
<script type="text/javascript">
$('nav .nav-tabs').on('shown.bs.tab', 'a[data-toggle="tab"]', function (e) {
    console.log('dmdcsklvfnm,frerml,sodfp,psomf');
});
</script>

@endsection