<style>
.app-sidebar {
    padding-top: 50px;
}
.app-sidebar__user{
    margin-bottom: 0;
    padding-top: 20px;
    padding-bottom: 20px;
    background-color: rgba(51, 51, 51, 0.4);
}
.app-sidebar__user_box{
    background-image: url("{{assets('img/images/material-bg.jpg')}}");
    background-size: cover;
}
.treeview-item {
    padding: 15px 5px 15px 20px;
}
</style>
<!-- Sidebar menu-->
<div class="app-sidebar__overlay" data-toggle="sidebar"></div>
<aside class="app-sidebar">
	<div class="app-sidebar__user_box">
    	<div class="app-sidebar__user">
    		<img class="app-sidebar__user-avatar" style="width: 48px; height: 48px; background-color: #afb7c4;"
    			src="{{assets('img/default.png')}}"
    			alt="User Image">
    		<div>
    			<p class="app-sidebar__user-name">John Doe</p>
    			<p class="app-sidebar__user-designation">Frontend Developer</p>
    		</div>
    	</div>
	</div>
	<ul class="app-menu">
		<li><a class="app-menu__item active" href="index.html"><i
				class="app-menu__icon fa fa-dashboard"></i><span
				class="app-menu__label">Dashboard</span></a>
		</li>
		<li class="treeview"><a class="app-menu__item" href="#"
			data-toggle="treeview"><i class="app-menu__icon fa fa-users"></i><span
				class="app-menu__label">Students</span><i
				class="treeview-indicator fa fa-angle-right"></i></a>
			<ul class="treeview-menu">
				<li><a class="treeview-item" href="bootstrap-components.html"><i
						class="icon fa fa-graduation-cap"></i> View Students</a>
				</li>
				<li><a class="treeview-item"
					href="https://fontawesome.com/v4.7.0/icons/" target="_blank"
					rel="noopener"><i class="icon fa fa-plus"></i> Add Student</a>
				</li>
			</ul>
		</li>
		<li><a class="app-menu__item" href="charts.html"><i
				class="app-menu__icon fa fa-pie-chart"></i><span
				class="app-menu__label">Charts</span></a>
		</li>
		<li class="treeview"><a class="app-menu__item" href="#"
			data-toggle="treeview"><i class="app-menu__icon fa fa-edit"></i><span
				class="app-menu__label">Exams</span><i
				class="treeview-indicator fa fa-angle-right"></i></a>
			<ul class="treeview-menu">
				<li><a class="treeview-item" href="form-components.html"><i
						class="icon fa fa-circle-o"></i> View Exams</a>
				</li>
				<li><a class="treeview-item" href="form-custom.html"><i
						class="icon fa fa-circle-o"></i> Register Exam</a>
				</li>
			</ul>
		</li>
	</ul>
</aside>