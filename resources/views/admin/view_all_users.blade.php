<?php 
$title = "Admin - View Users | " . SITE_TITLE;
$page = 'manage';
$subCat = 'users';
?>
@extends('admin.layout')

@section('dashboard_content')
<style>
    #user_summary span{
        min-width: 150px; 
        display: inline-block;
    }
</style>
	<div>
		<ol class="breadcrumb">
			<li><a href="{{getAddr('admin_dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
			<li class="active"><i class="fa fa-users"></i> Users</li>
		</ol>
	</div>
	<form action="<?= getAddr('admin_search_user') ?>" method="get">
		<div class="form-group">
			<div class="input-group">
				<input type="text" name="search_user" class="form-control" placeholder="Search by Username, Name, Email, Phone..." >
				<span class="input-group-btn" style="padding: 0;">
					<button type="submit" class="btn btn-success" >
						<i class="fa fa-search fa-fw"></i> search
					</button>
				</span>
				
			</div>
		</div>
	</form>    
	<div id="user_summary">
		<p><span>Total No Users: </span> <strong>{{$numOfRealUsers}}</strong></p>
		<div class="row">
			<div class="col-sm-6">
				<p><span>Main Balance: </span> <strong>{{CURRENCY_SIGN . $totalMainBalance}}</strong></p>
			</div>
		</div>
	</div>
	@include('common.view_all_users')
	

@stop
