<?php
$title = "Admin - Send SMS | " . SITE_TITLE;
$page = 'manage';
$subCat = 'sms';
$isBroadcast = isset($isBroadcast) ? $isBroadcast : false;
?>
@extends('admin.layout')

@section('dashboard_content')

	<div class="row">
		<ol class="breadcrumb">
			<li><a href="{{getAddr('admin_dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
			<li class="active"><i class="fa fa-users"></i> SMS</li>
		</ol>
	</div>
	
	<h3>Send {{($isBroadcast) ? 'Broadcast' : 'SMS'}}</h3>
          
	<form action="" method="post">
	   @if($isBroadcast)
	   <p>Send SMS to all users</p>
	   @else
	   <div class="form-group" >
			<label for="">Phone number</label>
			<input type="tel" name="<?= PHONE_NO ?>" value="<?= getValue($post, PHONE_NO, $phone) ?>" 
				 required class="form-control">
	   </div>
	   @endif
	   <div class="form-group" >
			<label for="">Message</label>
			<textarea name="{{MESSAGE}}" class="form-control" cols="30" rows="5">{{getValue($post, MESSAGE)}}</textarea>
		</div>
		
		<input type="hidden" name="send_sms" value="true" />
		<input type="hidden" name="<?= CSRF_TOKEN ?>" value="<?= \Session::getCsrfValue() ?>" />
		<input type="submit"  name="send" class="btn btn-success" value="Send">
	</form>    
	
	

@stop
