<?php

?>

<fieldset>
	<legend>Games</legend>
	<div class="row dashboard-items">
		<div class="col-lg-3 col-md-6">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<div class="row">
						<div class="col-xs-9">
							<div><h4>Head to Head</h4></div>
						</div>
						<div class="col-xs-3 text-right">
							<h4 class="badge">{{$numOfSG}}</h4>
						</div>
					</div>
				</div>
				<a href="{{getAddr('admin_sg')}}">
					<div class="panel-footer">
						<span class="pull-left">Click here...</span>
						<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
						<div class="clearfix"></div>
					</div>
				</a>
			</div>
		</div>
		<div class="col-lg-3 col-md-6">
			<div class="panel panel-green">
				<div class="panel-heading">
					<div class="row">
						<div class="col-xs-9">
							<div><h4>Tournaments</h4></div>
						</div>
						<div class="col-xs-3 text-right">
							<h4 class="badge">{{$numOfTournaments}}</h4>
						</div>
					</div>
				</div>
				<a href="{{getAddr('admin_tournaments')}}">
					<div class="panel-footer">
						<span class="pull-left">Click here...</span>
						<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
						<div class="clearfix"></div>
					</div>
				</a>
			</div>
		</div>
		<div class="col-lg-3 col-md-6">
			<div class="panel panel-orange">
				<div class="panel-heading">
					<div class="row">
						<div class="col-xs-9">
							<div><h4>Hourly 6</h4></div>
						</div>
						<div class="col-xs-3 text-right">
							<h4 class="badge">{{$numOfIT}}</h4>
						</div>
					</div>
				</div>
				<a href="{{getAddr('admin_IT')}}">
					<div class="panel-footer">
						<span class="pull-left">Click here...</span>
						<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
						<div class="clearfix"></div>
					</div>
				</a>
			</div>
		</div>
		<div class="col-lg-3 col-md-6">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<div class="row">
						<div class="col-xs-9">
							<div><h4>Location Tournaments</h4></div>
						</div>
						<div class="col-xs-3 text-right">
							<h4 class="badge">{{$numOfGT}}</h4>
						</div>
					</div>
				</div>
				<a href="{{getAddr('admin_GT')}}">
					<div class="panel-footer">
						<span class="pull-left">Click here...</span>
						<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
						<div class="clearfix"></div>
					</div>
				</a>
			</div>
		</div>
	</div>
</fieldset>