<?php 
$title = isset($title) ? $title : 'Admin | ' . SITE_TITLE;

$category = isset($category) ? $category : '';
$subCat = isset($subCat) ? $subCat : '';
$dashboard = ''; $manage = ''; $users = ''; $founders = ''; $agents = '';

$active = 'active';  

if($category == 'dashboard') $dashboard = $active; 
if($category == 'manage') $manage = $active; 

if($subCat == 'users') $users = $active;  

function getActiveCat($page)
{
	global $category;
	
	if($category == $page) return 'active';
	
	return '';
}
function getActiveSubCat($pageSubCat)
{
	global $subCat;
	
	if($subCat == $pageSubCat) return 'active';
	
	return '';
}

?>

@extends('admin.vali_layout')

@section('body')
	
	@include('admin.header')
	
	@include('admin._sidebar')
	
	<main class="app-content">
		@include('common.message')
		@yield('dashboard_content')
	</main>
@endsection


