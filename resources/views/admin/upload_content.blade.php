<?php
$donFlashMessages = true;
$title = "Admin - Install Content for {$course[COURSE_CODE]} | " . SITE_TITLE;
?>

@extends('admin.layout')

@section('dashboard_content')

<div class="tile w-75 mx-auto">
	 <header class="tile-header text-center">
		<h2>Upload Content</h2><hr />
	 </header>
	<div class="tile-body">
	 <div class="">
		<form action="" method="post" enctype="multipart/form-data">	
			@include('common.form_message')		
			<div class="alert alert-info">
				Upload content for this subject
				<br /><br />
				<strong>
					<strong>NOTE:</strong> Installing {{$course[COURSE_CODE]}} will take a while and should not be interrupted
				</strong>
			</div>
			<dl class="row">
				<dt class="col-md-3">Subject</dt>
				<dd class="col-md-9">{{$course[COURSE_CODE]}}</dd>
				<dt class="col-md-3">Title</dt>
				<dd class="col-md-9">{{$course[COURSE_TITLE]}}</dd>
			</dl>
			<div class="form-group">
				<label for="" >Content</label><br />
				<input type="file" class="form-control" name="content" value="" />
			</div>
			<br />
			<div class="form-group mt-2">
    			<input type="submit"  name="add" style="width: 60%; margin: auto;" 
    					class="btn btn-primary btn-block" value="Install Now">
			</div>
		</form>
	</div>
	</div>
</div>
			
@endsection