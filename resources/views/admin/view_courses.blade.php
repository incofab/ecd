<?php
$title = "Admin - All Courses | " . SITE_TITLE;
$page = 'manage';
$subCat = 'admin';

?>
@extends('admin.layout')

@section('dashboard_content')

	<div >
		<ol class="breadcrumb">
			<li><a href="{{getAddr('admin_dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
			<li class="active ml-2"><i class="fa fa-users"></i> Courses</li>
		</ol>
		<h4 class="">Admin Available Courses</h4>
	</div>
	<div>
		<table class="table table-striped">
			<tr>
				<th>Course Title</th>
				<th>Course Code</th>
				<th>Description</th>
				<th>State</th>
			</tr>
			@foreach($courses as $course)
				<tr>
					<td>{{$course[COURSE_TITLE]}}</td>
					<td>{{$course[COURSE_CODE]}}</td>
					<td>{{$course[DESCRIPTION]}}</td>
					<td>
						<a href="{{getAddr('admin_export_content', [$course[COURSE_CODE], '?next='.getAddr('')])}}" 
							onclick="return confirm('Download {{$course[COURSE_CODE]}} now? \nThis is take a few minutes and should not be interrupted')"
							class="btn btn-sm btn-warning mr-2"> <i class="fa fa-download"></i> Download </a>
    					@if($courseInstaller->isCourseInstalled($course[COURSE_CODE]))
    						<a href="{{getAddr('admin_uninstall_course', [$course[COURSE_CODE], '?next='.getAddr('')])}}" 
    							class="btn btn-sm btn-danger"> <i class="fa fa-trash"></i> Uninstall </a>
						@else
    						<a href="{{getAddr('admin_install_courses', [$course[COURSE_CODE], '?next='.getAddr('')])}}" 
    							class="btn btn-sm btn-success"> <i class="fa fa-upload"></i> Install </a>
    					@endif
					</td>
				</tr>
			@endforeach
		</table>
	</div>
	

@stop
