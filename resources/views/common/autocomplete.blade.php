<?php

$remoteSearchURL = isset($remoteSearchURL) ? $remoteSearchURL : '';

?>

<link type="text/css" rel="stylesheet" href="{{assets('lib/autocomplete/easy-autocomplete.min.css')}}"/>
<script src="{{assets('lib/autocomplete/jquery.easy-autocomplete.min.js')}}"></script>

<script type="text/javascript" >
	var options = {
		url: function(phrase) {
			// $remoteSearchURL return result in the form {id: 2,  title: 'the title'}
			return "<?= $remoteSearchURL ?>?search_query=" + encodeURIComponent(phrase);
		},
		
		getValue: function(element) {
			return element.title;
		},
		
		list: {
			
			onSelectItemEvent: function() {
				// Get the ID
				var value = $("#remote_input").getSelectedItemData().id;

				$("#data-holder").val(value).trigger("change");
			}
		}
	};

	$("#remote_input").easyAutocomplete(options);
</script>