<?php
namespace App\Migrations;

use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Database\Schema\Blueprint;

class Courses{
	
	public $connection = 'default'; 
	
	function __construct() {  
		$this->create_courses_Table();   
	} 
	
	function create_courses_Table() {

		$schema = Capsule::schema($this->connection); 
		
		if ($schema->hasTable(COURSES_TABLE)){ 
			echo 'Courses Table already exists';
			return;
		}
		
		$schema->create(COURSES_TABLE, function(Blueprint $table) {
			
			$table->increments(TABLE_ID);
			$table->string(COURSE_CODE, 80)->unique();
			$table->string(CATEGORY, 20)->nullable(true);
			$table->string(COURSE_TITLE)->nullable(true);
			$table->text(DESCRIPTION)->nullable(true);

// 			$table->timestamps();
			$table->timestamp(CREATED_AT)->nullable(true);
			$table->timestamp(UPDATED_AT)->nullable(true);
			
			$table->engine = 'InnoDB';
			
			echo 'Courses table created';
		});
		
		$this->installCourses();
	}
	
	const COURSES = [
	    /*
	     'Sweet16' => 'Sweet Sixteen Novel',
	     */
	     'English' => [
	         COURSE_TITLE => 'English Language',
	         CATEGORY => 'JAMB',
         ],
	     'Account' => [
	         COURSE_TITLE => 'Accounting',
	         CATEGORY => 'JAMB',
	     ],
	     'Biology' => [
	         COURSE_TITLE => 'Biology',
	         CATEGORY => 'JAMB',
	     ],
	     'Chemistry' => [
	         COURSE_TITLE => 'Chemistry',
	         CATEGORY => 'JAMB',
         ],
	     'Commerce' => [
	         COURSE_TITLE => 'Commerce',
	         CATEGORY => 'JAMB',
         ],
	     'CRK' => [
	         COURSE_TITLE => 'Christian Religious Knowledge',
	         CATEGORY => 'JAMB',
         ],
	    'Economics' => [
	        COURSE_TITLE => 'Economics',
	        CATEGORY => 'JAMB',
	    ],
	    'Geography' => [
	        COURSE_TITLE => 'Geography',
	        CATEGORY => 'JAMB',
        ],
	    'Government' => [
	        COURSE_TITLE => 'Government',
	        CATEGORY => 'JAMB',
        ],
	    'Literature' => [
	        COURSE_TITLE => 'Literature in English',
	        CATEGORY => 'JAMB',
	    ],
	    'Mathematics' => [
	        COURSE_TITLE => 'Mathematics',
	        CATEGORY => 'JAMB',
        ],
	    'Physics' => [
	        COURSE_TITLE => 'Physics',
	        CATEGORY => 'JAMB',
        ],
	    'Sweet16' => [
	        COURSE_TITLE => 'Sweet Sixteen Novel',
	        CATEGORY => 'JAMB',
        ],
	];
	
	function installCourses() 
	{
	    foreach (self::COURSES as $courseCode => $courseData) 
	    {
	        $courseData[COURSE_CODE] = $courseCode;
	        
	        $obj = new \App\Models\Courses();
	        
	        $ret = $obj->insert($courseData);
	    }
	    
	    echo '<br />Courses table seeded';
	}
	
}