<?php
namespace App\Migrations;

use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Database\Schema\Blueprint;

class Sessions{

	public $connection = 'default';

	function __construct() {
		$this->create__Table();
	}

	function create__Table() {

		$schema = Capsule::schema($this->connection);

		if ($schema->hasTable(SESSIONS_TABLE)){
			echo 'Sesssions Table already exists';
			return;
		}

		$schema->create(SESSIONS_TABLE, function(Blueprint $table) {
				
			$table->increments(TABLE_ID); 
			$table->string(COURSE_CODE, 80);
			$table->string(SESSION, 20);
			
			$table->string(CATEGORY, 20)->nullable(true);

			$table->text(GENERAL_INSTRUCTIONS)->nullable(true);
			

			// 			$table->timestamps();
			$table->timestamp(CREATED_AT)->nullable(true);
			$table->timestamp(UPDATED_AT)->nullable(true);
			$table->engine = 'InnoDB';

			$table->foreign(COURSE_CODE)->references(COURSE_CODE)->on(COURSES_TABLE)
				->onDelete('cascade')->onUpdate('cascade');
				

			echo 'Sesssions table created';

		});


	}

}