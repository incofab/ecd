<?php
namespace App\Migrations;

use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Database\Schema\Blueprint;

class Topics
{
	public $connection = 'default';

	function __construct() 
	{
		$this->create__Table();
	}

	function create__Table() 
	{
		$schema = Capsule::schema($this->connection);

		if ($schema->hasTable(TOPICS_TABLE))
		{
			echo 'Topics Table already exists';
		
			return;
		}

		$schema->create(TOPICS_TABLE, function(Blueprint $table) 
		{
			$table->increments(TABLE_ID); 
			$table->string(COURSE_CODE, 80);
			
			$table->string(TITLE)->unique();
			$table->text(DESCRIPTION)->nullable(true);
			
			$table->timestamp(CREATED_AT)->nullable(true);
			$table->timestamp(UPDATED_AT)->nullable(true);
			$table->engine = 'InnoDB';

			$table->foreign(COURSE_CODE)->references(COURSE_CODE)->on(COURSES_TABLE)
				->onDelete('cascade')->onUpdate('cascade');

			echo 'Topics table created';
		});

	}

}