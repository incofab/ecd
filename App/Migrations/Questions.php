<?php
namespace App\Migrations;

use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Database\Schema\Blueprint;

class Questions{

	public $connection = 'default';

	function __construct() {
		$this->create__Table();
	}

	function create__Table() { 

		$schema = Capsule::schema($this->connection);

		if ($schema->hasTable(QUESTIONS_TABLE))
		{
			echo 'Questions Table already exists';
		
			return;
		}

		$schema->create(QUESTIONS_TABLE, function(Blueprint $table) {

			$table->increments(TABLE_ID);
			
			$table->string(COURSE_CODE, 80);
			$table->integer(SESSION_ID, false, true);
			$table->integer(TOPIC_ID, false, true)->nullable(true);
			
			$table->integer(QUESTION_NO, false, true);
			$table->text(QUESTION);
			$table->text(OPTION_A);
			$table->text(OPTION_B);
			$table->text(OPTION_C);
			$table->text(OPTION_D)->nullable(true);
			$table->text(OPTION_E)->nullable(true);
			$table->string(ANSWER, 2);
			$table->longText(ANSWER_META)->nullable(true);
			
			// 			$table->timestamps();
			$table->timestamp(CREATED_AT)->nullable(true);
			$table->timestamp(UPDATED_AT)->nullable(true);
			$table->engine = 'InnoDB';

			$table->foreign(COURSE_CODE)->references(COURSE_CODE)->on(COURSES_TABLE)
				->onDelete('cascade')->onUpdate('cascade');
			
			$table->foreign(SESSION_ID)->references(TABLE_ID)->on(SESSIONS_TABLE)
				->onDelete('cascade')->onUpdate('cascade');
			
			$table->foreign(TOPIC_ID)->references(TABLE_ID)->on(TOPICS_TABLE)
				->onDelete('cascade')->onUpdate('cascade');

			echo 'Questions table created';

		});


	}

}