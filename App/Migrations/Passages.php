<?php
namespace App\Migrations;

use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Database\Schema\Blueprint;

class Passages{

	public $connection = 'default';

	function __construct() {
		$this->create__Table();
	}

	function create__Table() {

		$schema = Capsule::schema($this->connection);
 
		if ($schema->hasTable(PASSAGES_TABLE)){
			echo 'Passages Table already exists';
			return;
		}

		$schema->create(PASSAGES_TABLE, function(Blueprint $table) {

			$table->increments(TABLE_ID);
			$table->string(COURSE_CODE, 80);
			$table->integer(SESSION_ID, false, true);
				
			$table->text(PASSAGE);
			$table->integer(FROM_, false, true);
			$table->integer(TO_, false, true);

			// 			$table->timestamps();
			$table->timestamp(CREATED_AT)->nullable(true);
			$table->timestamp(UPDATED_AT)->nullable(true);
			$table->engine = 'InnoDB';

			$table->foreign(COURSE_CODE)->references(COURSE_CODE)->on(COURSES_TABLE)
				->onDelete('cascade')->onUpdate('cascade');
			$table->foreign(SESSION_ID)->references(TABLE_ID)->on(SESSIONS_TABLE)
				->onDelete('cascade')->onUpdate('cascade');

			echo 'Passages table created';

		});


	}

}