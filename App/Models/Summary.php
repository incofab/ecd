<?php
namespace App\Models;


class Summary extends BaseModel {

	public $table = SUMMARY_TABLE;
	
	public $rules_insert = [
	    'required' =>  [ [COURSE_CODE], [CHAPTER_NO], [TITLE], [SUMMARY] ],
	];
	
	function insert($post) {

		$val = new \Valitron\Validator($post);

		$val->rules($this->rules_insert);
		$val->labels($this->labels);
		if (!$val->validate()){
			\Session::flash('errors', $val->errors());
			\Session::flash('post', $post);
			return false;
		}

		//Check if chapter_no already exists under the same courseName (chapter numbers must be unique
		if ($this->where(CHAPTER_NO, '=', $post[CHAPTER_NO])
				->where(COURSE_CODE, '=', $post[COURSE_CODE])->first()){
					\Session::flash('errorMsg', 'This Chapter No already
					exists, Chapter Numbers must be unique');
			\Session::flash('post', $post);
			return false;
		} 

		$this[COURSE_CODE] = $post[COURSE_CODE];
		$this[CHAPTER_NO] = $post[CHAPTER_NO];
		$this[TITLE] = $post[TITLE];
		$this[DESCRIPTION] = array_get($post, DESCRIPTION);
		$this[SUMMARY] = $post[SUMMARY];

		if ($this->save()){
			\Session::flash('success', 'Data recorded successfully');
			return true;
		}else {
			\Session::flash('errorMsg', 'Error: Data entry failed');
			\Session::flash('post', $post);
			return false;
		}
	}

	function updateRecord($post) {

		$val = new \Valitron\Validator($post);

		$val->rules($this->rules_update);
		//Assign labels to the validator... Use to replace form name
		$val->labels($this->labels);

		if (!$val->validate()){
			\Session::flash('errors', $val->errors());
			\Session::flash('post', $post);
			return false;
		}
		
		$old = $this->where(TABLE_ID, '=', $post[TABLE_ID])->first();
		
		//Check if row exists
		if (!$old){
			\Session::flash('errorMsg', 'There is no existing record, create new one');
			\Session::flash('post', $post);
			return false;
		}
		
		$old[COURSE_CODE] = $post[COURSE_CODE];
		$old[CHAPTER_NO] = $post[CHAPTER_NO];
		$old[TITLE] = $post[TITLE];
		$old[DESCRIPTION] = array_get($post, DESCRIPTION);
		$old[SUMMARY] = $post[SUMMARY];

		if ($old->save()){
			\Session::flash('success', 'Record updated successfully');
			return true;
		}else {
			\Session::flash('errorMsg', 'Error: Update failed');
			\Session::flash('post', $post);
			return false;
		}

	}

	function course() {
		return $this->belongsTo(\App\Models\Courses::class, COURSE_CODE, COURSE_CODE);
	}
	


}