<?php

namespace App\Models;

class Courses extends BaseModel 
{
	public $table = COURSES_TABLE;
	
// 	function validate($param) 
// 	{
	    
// 	    $val = new \Valitron\Validator($post);
	    
// 	    $val->rules($this->rules_insert);
	    
// 	    $val->labels($this->labels);
	    
// 	    if (!$val->validate())
// 	    {
// 	        return [SUCCESS => false, MESSAGE => 'Validation failed', 'val_errors' => $val->errors() ];
// 	    }
	    
//         return [SUCCESS => true, MESSAGE => 'Record validated'];
// 	}

	function insert($post) {
		
		$val = new \Valitron\Validator($post);  
		$val->rules($this->rules_insert);
		$val->labels($this->labels);
		
		if (!$val->validate()){
			\Session::flash('errors', $val->errors());
			\Session::flash('post', $post);
			return false;
		} 
		
		if($this->where(COURSE_CODE, '=', $post[COURSE_CODE])->first()){
			\Session::flash('errorMsg', 'Error: Course code already exist');
			\Session::flash('post', $post);
			return false;
		}
		
		$this[COURSE_CODE] = $post[COURSE_CODE];
		$this[CATEGORY] = array_get($post, CATEGORY);
		$this[COURSE_TITLE] = $post[COURSE_TITLE];
		$this[DESCRIPTION] = array_get($post, DESCRIPTION);
		
		if ($this->save()){
			\Session::flash('success', 'Data recorded successfully');
			return true;
		}else {
			\Session::flash('errorMsg', 'Error: Data entry failed');
			\Session::flash('post', $post);
			return false;
		}
	}
	
	function updateRecord($post) {
	 
		$val = new \Valitron\Validator($post);
	
		$val->rules($this->rules_update);
		//Assign labels to the validator... Use to replace form name
		$val->labels($this->labels);
		if (!$val->validate()){
			\Session::flash('errors', $val->errors());
			\Session::flash('post', $post);
			return false;
		}
		
		$old = $this->where(TABLE_ID, '=', $post[TABLE_ID])->first();
		
		//Check if row exists
		if (!$old){
			\Session::flash('errorMsg', 'There is no existing record, create new one');
			\Session::flash('post', $post);
			return false;
		}
		
		if($old[COURSE_CODE] != $post[COURSE_CODE] &&
				$this->where(COURSE_CODE, '=', $post[COURSE_CODE])->first()){
			\Session::flash('errorMsg', 'Error: Course code already exist');
			\Session::flash('post', $post);
			return false;
		}
		
		$arr = [];
		
		$arr[COURSE_CODE] = $post[COURSE_CODE];
		$arr[CATEGORY] = array_get($post, CATEGORY, $old[CATEGORY]);
		$arr[COURSE_TITLE] = $post[COURSE_TITLE];
		$arr[DESCRIPTION] = array_get($post, DESCRIPTION, $old[DESCRIPTION]);
		
		$success = $this->where(TABLE_ID, '=', $post[TABLE_ID])->update($arr);
	
		if ($success){
			\Session::flash('success', 'Record updated successfully');
			return true;
		}else {
			\Session::flash('errorMsg', 'Error: Update failed');
			\Session::flash('post', $post);
			return false;
		}
	
	}
	
	function sessions() {
		return $this->hasMany(\App\Models\Sessions::class, COURSE_CODE, COURSE_CODE);
	}
	
	function topics() {
		return $this->hasMany(\App\Models\Topics::class, COURSE_CODE, COURSE_CODE);
	}
	
	function summaryChapters() {
		return $this->hasMany(\App\Models\Summary::class, COURSE_CODE, COURSE_CODE);
	}
	
}