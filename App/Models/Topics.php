<?php

namespace App\Models;


class Topics extends BaseModel 
{
	public $table = TOPICS_TABLE;
	
	public $fillable = [TABLE_ID, COURSE_CODE, TITLE, DESCRIPTION];
	
	public $rules_insert = [
	    'required' => [
	        [COURSE_CODE], [TITLE]
	    ],
	];
	
	public $rules_update = [
	    'required' => [
	        [TABLE_ID], [TITLE]
	    ],
	];
	
	function insert($post) 
	{ 
		$val = new \Valitron\Validator($post);  
		$val->rules($this->rules_insert);
		$val->labels($this->labels);

		if (!$val->validate())
		{
			return [SUCCESSFUL => false, MESSAGE => 'validation failed', 'val_errors' => $val->errors()];
		}
		
		if ($this->where(COURSE_CODE, '=', $post[COURSE_CODE])->where(TITLE, '=', $post[TITLE])->first())
		{
			return [SUCCESSFUL => false, MESSAGE => "'{$post[TITLE]}' already exist for this course, {$post[COURSE_CODE]}"];
		}
		
		$arr = [];
		
		$arr[COURSE_CODE] = $post[COURSE_CODE];  
		$arr[TITLE] = $post[TITLE];  
		$arr[DESCRIPTION] = getValue($post, DESCRIPTION);  
		
		$ret = $this->create($arr);
		
		if ($ret)
		{
			return [SUCCESSFUL => true, MESSAGE => 'Data recorded successfully', 'data' => $ret->toArray()];
		}

		return [SUCCESSFUL => false, MESSAGE => 'Data entry failed'];
	}
	
	function updateRecord($post) {
	 
		$val = new \Valitron\Validator($post);
	
		$val->rules($this->rules_update);
		$val->labels($this->labels);

		if (!$val->validate())
		{
		    return [SUCCESSFUL => false, MESSAGE => 'validation failed', 'val_errors' => $val->errors()];
		}
		
		$old = $this->where(TABLE_ID, '=', $post[TABLE_ID])->first();
		
		if (!$old)
		{
		    return [SUCCESSFUL => false, MESSAGE => 'Record not found'];
		}
		
		if ($old[TITLE] !== $post[TITLE])
		{
		    if ($this->where(COURSE_CODE, '=', $old[COURSE_CODE])->where(TITLE, '=', $post[TITLE])->first())
		    {
		        return [SUCCESSFUL => false, MESSAGE => "'{$post[TITLE]}' already exist for this course, {$old[COURSE_CODE]}"];
		    }
		}
		
		$old[TITLE] = $post[TITLE];
		$old[DESCRIPTION] = array_get($post, DESCRIPTION, $old[DESCRIPTION]);
		
		if ($old->save())
		{
		    return [SUCCESSFUL => true, MESSAGE => 'Record updated successfully', 'data' => $old->toArray()];
		}
		
		return [SUCCESSFUL => false, MESSAGE => 'Error: Update failed'];
	}
	
	function course() 
	{
		return $this->belongsTo(\App\Models\Courses::class, COURSE_CODE, COURSE_CODE);
	}
	
}