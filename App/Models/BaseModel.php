<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model {

	public $connection = 'default';
	public $labels = [];
	public $table = '';
	public $rules_insert = [];
	public $rules_update = [];
	public $timestamps = TRUE;
	public $fillable = [];
	public $primaryKey = TABLE_ID;
	
	
	protected $container;
	
	/**
	 * @var \Carbon\Carbon
	 */
	protected $carbon;
	
	/**
	 * @var \App\Core\ValidatorWrapper
	 */
	protected $validatorWrapper;
	
	function __construct($param = null)
	{ 
	    parent::__construct();
	    
// 	    if($param != INJECTED) return;
	    
	    $this->container = (new \Bootstrap\Container\MyContainer())->getContainerInstance();
	    
	    $this->carbon = $this->container->get(\Carbon\Carbon::class);
	    $this->validatorWrapper = $this->container->get(\App\Core\ValidatorWrapper::class); 
	}
	
	protected function DIget($name) 
	{
	    return $this->container->get($name);
	}
	
	/**
	 * Creates a new row and return the instance.
	 * This is meant to override the libraries create methods which for some 
	 * strange reason aren't working properly.
	 * NOTE: The fillable property must be present in the model, else, this will throw an exception
	 * @param array $attributes
	 */
	static function create(array $attributes) {
		$model = new static();
		$model->fill($attributes);
		$model->save();
		return $model;
	}
	
	/** @return \App\Core\Validator */
	function initValidator($data, $fields = [])
	{
	    return $this->validatorWrapper->initValidator($data);
	}
	
	function validateInsert($post, $forUpdate = false)
	{
	    $val = $this->initValidator($post);
	    
	    if($forUpdate)
	       $val->rules($this->rules_update);
	    else
	       $val->rules($this->rules_insert);
	    
	    $val->labels($this->labels);
	    
	    if (!$val->validate())
	    {
	        return [SUCCESS => false, MESSAGE => 'Validation failed', 'val_errors' => $val->errors() ];
	    }
	    
	    return [SUCCESSFUL => true, MESSAGE => 'Data is valid'];
	}
	function validateUpdate($post)
	{
	    return $this->validateInsert($post, true);
	}
	
	function getCount()
	{
	    $sql = 'SELECT COUNT('.$this->primaryKey.") AS count_users FROM {$this->getTable()}";
	    
	    $arr = [];
	    
	    $superArray = $this->pdoQuery($sql, $arr);
	    
	    return array_get($superArray, 'count_users', 0);
	}
	
	function getSum($columnName)
	{
	    $sql = 'SELECT SUM('.$columnName.") AS sum_column FROM {$this->getTable()}";
	    
	    $arr = [];
	    
	    $superArray = $this->pdoQuery($sql, $arr);
	    
	    return array_get($superArray, 'sum_column', 0);
	}
	
	function pdoQuery($query, $binders) 
	{
	    $superArray = null;
	    $pdo = $this->getConnection()->getPdo();
	    try 
	    {	        
           $stmt = $pdo->prepare($query);
           
           foreach ($binders as $key => $value) 
           {
//                echo "Key = $key, value = $value <br />";
               $stmt->bindValue($key, $value);
	       }
// 	       die($stmt->queryString);
	       $stmt->execute();
	        
	       $superArray = $stmt->fetch(\PDO::FETCH_ASSOC);
	       
           return $superArray;
	       
	    } catch (\Exception $e) { dlog($e->getMessage()); }
	    
       return $superArray;
	}
	
}