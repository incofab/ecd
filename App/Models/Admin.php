<?php
namespace App\Models;

class Admin extends BaseModel
{

	public $table = ADMIN_TABLE;
	public $fillable = [EMAIL, PASSWORD, USERNAME, LEVEL];
	
	public $rules_insert = [
		'required' => [
		    [USERNAME], [EMAIL], [LEVEL]
		],
       'integer' => [ [LEVEL] ]
	];
	
	public $rules_update = [
		'required' => [
		    [TABLE_ID], [USERNAME], [EMAIL]
		],
	];
	
	const ACCESS_LEVEL_NON_GRATA = 0;
	const ACCESS_LEVEL_FULL_ACCESS = 1;
	const ACCESS_LEVEL_GOLD = 2;
	const ACCESS_LEVEL_SILVER = 3;
	const ACCESS_LEVEL_BRONZE = 4;
	const ACCESS_LEVEL_OTHERS = 5;

	/**
	 * @var \Session
	 */
	private $session; 
	
	function __construct($param = null)
	{
	    parent::__construct($param);
	    
// 	    if($param != INJECTED) return;
	    
	    $this->session = $this->container->get(\Session::class);
	}
	
	function save(array $options = array())
	{
	    $isSaved = parent::save($options);
	    
	    if(!$isSaved) return $isSaved;
	    
	    // Update session
	    $sessionData = $this->session->get(ADMIN_SESSION_DATA, false);
	    
	    // Update session if it is an action taken by the user
	    if ($sessionData && $sessionData[TABLE_ID] == $this[TABLE_ID])
	    {
	        $this->session->put(ADMIN_SESSION_DATA, $this->toArray()); 
	    }
	    
	    return $isSaved;
	}
	
	function insert($post) {

	    $val = $this->validatorWrapper->initValidator($post);

		$val->rules($this->rules_insert);
		$val->labels($this->labels);

		if (!$val->validate())
		{
		    return [SUCCESS => false, MESSAGE => 'Validation failed', 'val_errors' => $val->errors() ];
		}
		
		// Check if username, email, or phone number already exists
		if ($this->where(USERNAME, '=', $post[USERNAME])->first())
		{
		    return [SUCCESS => false, MESSAGE => 'Username already exists'];
		}
		
// 		if ($this->where(EMAIL, '=', $post[EMAIL])->first())
// 		{
// 		    return [SUCCESS => false, MESSAGE => 'Email already exists'];
// 		}
        
		$arr = [];
		$arr[USERNAME] = htmlentities($post[USERNAME]);
		$arr[PASSWORD] = cryptPassword('admin');
		$arr[EMAIL]    = htmlentities($post[EMAIL]);
		$arr[LEVEL]    = htmlentities($post[LEVEL]);

		if ($this->create($arr))
		{
		    return [SUCCESS => true, MESSAGE => 'registration successful'];
		}
		
		return [SUCCESS => false, MESSAGE => 'Error: Data entry failed'];
	}

	function edit($post) {

	    $val = $this->validatorWrapper->initValidator($post);

		$val->rules($this->rules_update);
		$val->labels($this->labels);

		if (!$val->validate())
		{
		    return [SUCCESS => false, MESSAGE => 'Validation failed', 'val_errors' => $val->errors() ];
		}

		$oldData = $this->where(TABLE_ID, '=', $post[TABLE_ID])->first();
		// Check if table ID is valid
		if (!$oldData)
		{
		    return [SUCCESS => false, MESSAGE => 'Invalid table ID'];
		}
		
		// Check if username, email, or phone number already exists
		if ($oldData[USERNAME] != $post[USERNAME] 
		    && $this->where(USERNAME, '=', $post[USERNAME])->first())
		{
		    return [SUCCESS => false, MESSAGE => 'Username already exists'];
		}
		
// 		if ($oldData[EMAIL] != $post[EMAIL]
// 		    && $this->where(EMAIL, '=', $post[EMAIL])->first())
// 		{
// 		    return [SUCCESS => false, MESSAGE => 'Email already exists'];
// 		}

		$oldData[USERNAME] = htmlentities($post[USERNAME]);
		$oldData[EMAIL] = htmlentities($post[EMAIL]);

		$success = $oldData->save();

		if ($success)
		{
		    return [SUCCESS => true, MESSAGE => 'Record updated successfully'];
		}
		
		return [SUCCESS => false, MESSAGE => 'Error: Update failed'];
	}
	
	function getLevel() 
	{
	    $str = 'Limited Admin';
	    switch ($this[LEVEL]) 
	    {
	        case self::ACCESS_LEVEL_NON_GRATA: $str = 'Non Grata'; break;
	        case self::ACCESS_LEVEL_FULL_ACCESS: $str = 'Full Access'; break;
	        case self::ACCESS_LEVEL_GOLD: $str = 'Gold'; break;
	        case self::ACCESS_LEVEL_SILVER: $str = 'Silver'; break;
	        case self::ACCESS_LEVEL_BRONZE: $str = 'Bronze'; break;
	    }
	    return $str;
	}



}


?>