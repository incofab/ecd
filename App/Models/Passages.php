<?php
namespace App\Models;


class Passages extends BaseModel {

	public $table = PASSAGES_TABLE;


	public $fillable = [TABLE_ID, COURSE_CODE, SESSION_ID, PASSAGE, FROM_, TO_];
	
	
	function session() {
		return $this->belongsTo(\App\Models\Sessions::class, SESSION_ID, TABLE_ID);
	}


}