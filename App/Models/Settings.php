<?php
namespace App\Models;


class Settings extends BaseModel
{
    public $table = SETTINGS_TABLE;
	
    public $fillable = [
        TABLE_ID, KEY, VALUE, DESCRIPTION
	];
	
	public $rules_insert = [
	    'required'   => [ [KEY] ],
	    'lengthMax'  => [ [KEY, 200] ],
	];
	
	public $rules_update = [
	    'required'   => [ [TABLE_ID], [KEY]  ],
	    'lengthMax'  => [ [KEY, 200] ],
	];
	
	function __construct($param = null)
	{
	    parent::__construct($param);
	}
	
	function insert($post){	
        $val = $this->validatorWrapper->initValidator($post);
        
        $val->rules($this->rules_insert);
        $val->labels($this->labels);
        
        if (!$val->validate())
        {
            return [SUCCESS => false, MESSAGE => 'Validation failed', 'val_errors' => $val->errors() ];
        }
	    
		$arr = [];
		$arr[KEY] = htmlentities($post[KEY]);
		$arr[VALUE] = array_get($post, VALUE);
		$arr[DESCRIPTION] = htmlentities($post[DESCRIPTION]);

        $data = $this->create($arr);
        
        if ($data)
		{
			return [SUCCESS => true, MESSAGE => 'Data recorded', 'data' => $data];
		}
		
		return [SUCCESS => false, MESSAGE => 'Error: Data entry failed'];
	}
	
	function edit($post){
        $val = $this->validatorWrapper->initValidator($post);
        
        $val->rules($this->rules_update);
        $val->labels($this->labels);
        
        if (!$val->validate())
        {
            return [SUCCESS => false, MESSAGE => 'Validation failed', 'val_errors' => $val->errors() ];
        }
        
        $oldData = $this->where(TABLE_ID, '=', $post[TABLE_ID])->first();
        
        if (!$oldData)
        {
            return [SUCCESS => false, MESSAGE => 'Parent record not found'];
        }
        
        $arr = [];
        $oldData[KEY] = htmlentities(array_get($post, KEY, $oldData[KEY]));
        $oldData[VALUE] = array_get($post, VALUE, $oldData[VALUE]);
        $oldData[DESCRIPTION] = htmlentities(array_get($post, DESCRIPTION, $oldData[DESCRIPTION]));
        
        $success = $oldData->save();
        
        if ($success)
        {
            return [SUCCESS => true, MESSAGE => 'Data recorded', 'data' => $oldData];
        }
        
        return [SUCCESS => false, MESSAGE => 'Error: Data entry failed'];
	}
	
	
}


?>