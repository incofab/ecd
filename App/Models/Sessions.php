<?php

namespace App\Models;


class Sessions extends BaseModel {
	
	public $table = SESSIONS_TABLE;
	public $fillable = [TABLE_ID, COURSE_CODE, SESSION, GENERAL_INSTRUCTIONS];
	
	function insert($post, $fromAnotherTable = false) { 
		
		$val = new \Valitron\Validator($post);  

		$val->rules($this->rules_insert);
		$val->labels($this->labels);
		if (!$val->validate()){
			\Session::flash('errors', $val->errors());
			\Session::flash('post', $post);
			return false;
		}
		
		if ($this->where(COURSE_CODE, '=', $post[COURSE_CODE])
		      ->where(SESSION, '=', $post[SESSION])->first()){
            \Session::flash('errors', "{$post[SESSION]} session already exist for this course, {$post[COURSE_CODE]}");
			\Session::flash('post', $post);
			return false;
		}
		
		$arr = [];
		if($fromAnotherTable){
		    $arr[TABLE_ID] = $post[TABLE_ID];
		}
		
		$arr[COURSE_CODE] = $post[COURSE_CODE];  
		$arr[SESSION] = $post[SESSION];  
		$arr[CATEGORY] = getValue($post, CATEGORY);  
		$arr[GENERAL_INSTRUCTIONS] = $post[GENERAL_INSTRUCTIONS];
		
		$ret = $this->create($arr);
		
		if ($ret){
			\Session::flash('success', 'Data recorded successfully');
			$this->savePerQuestionsInstructions($post[COURSE_CODE], $ret[TABLE_ID], static::joinUpInstruction($post));  
			$this->savePassages($post[COURSE_CODE], $ret[TABLE_ID], static::joinUpPassage($post)); 
			// Create Questions table
			return $ret;
		}else {
			\Session::flash('errorMsg', 'Error: Data entry failed');
			\Session::flash('post', $post);
			return false;
		}
		
	}
	
	function updateRecord($post) {
	 
		$val = new \Valitron\Validator($post);
	
		$val->rules($this->rules_update);
		//Assign labels to the validator... Use to replace form name
		$val->labels($this->labels);
		if (!$val->validate()){
			\Session::flash('errors', $val->errors());
			\Session::flash('post', $post);
			return false;
		}
		
		$old = $this->where(TABLE_ID, '=', $post[TABLE_ID])->first();
		
		//Check if row exists
		if (!$old){
			\Session::flash('errorMsg', 'There is no existing record, create new one');
			\Session::flash('post', $post);
			return false;
		}
		
		$old[COURSE_CODE] = $post[COURSE_CODE];
		$old[SESSION] = $post[SESSION];
		$old[CATEGORY] = $post[CATEGORY];
		$old[GENERAL_INSTRUCTIONS] = $post[GENERAL_INSTRUCTIONS];
		
		if ($old->save()){
			\Session::flash('success', 'Record updated successfully');
			$this->savePerQuestionsInstructions($post[COURSE_CODE], $old[TABLE_ID], static::joinUpInstruction($post));  
			$this->savePassages($post[COURSE_CODE], $old[TABLE_ID], static::joinUpPassage($post));
			return true;
		}else {
			\Session::flash('errorMsg', 'Error: Update failed');
			\Session::flash('post', $post);
			return false;
		}
	
	}
	
	function savePerQuestionsInstructions($courseCode, $sessionID, $post) {

		// First delete all the existing ones
		\App\Models\Instructions::where(SESSION_ID, '=', $sessionID)->delete();
		
		foreach ($post as $data) {
			
			if(empty($data[INSTRUCTION]) || empty($data[FROM_]) || empty($data[TO_])) continue;
			if(!is_numeric($data[FROM_]) || !is_numeric($data[TO_])) continue;
			
			\App\Models\Instructions::create([
					COURSE_CODE => $courseCode,
					SESSION_ID  => $sessionID,
					INSTRUCTION => $data[INSTRUCTION],
					FROM_ => $data[FROM_],
					TO_   => $data[TO_],
			]);
			
		}
				
	}
	
	function savePassages($courseCode, $sessionID, $post) {

		// First delete all the existing ones
		\App\Models\Passages::where(SESSION_ID, '=', $sessionID)->delete();
		
		foreach ($post as $data) {
			
			if(empty($data[PASSAGE]) || empty($data[FROM_]) || empty($data[TO_])) continue;
			if(!is_numeric($data[FROM_]) || !is_numeric($data[TO_])) continue;
			
			\App\Models\Passages::create([
					COURSE_CODE => $courseCode,
					SESSION_ID  => $sessionID,
					PASSAGE => $data[PASSAGE],
					FROM_ => $data[FROM_],
					TO_   => $data[TO_],
			]);
			
		}
				
	}

	static function joinUpInstruction($post) {

		$arr = [];
		if(!isset($post[ALL_INSTRUCTION])) return $arr;
		
		$len = count(getValue($post[ALL_INSTRUCTION], INSTRUCTION, []));
		
		for ($i = 0; $i < $len; $i++) {
			$arr[] = [
				INSTRUCTION => $post[ALL_INSTRUCTION][INSTRUCTION][$i],
				FROM_ => $post[ALL_INSTRUCTION][FROM_][$i],
				TO_ => $post[ALL_INSTRUCTION][TO_][$i],
				TABLE_ID => getValue(getValue($post[ALL_INSTRUCTION], TABLE_ID), $i),
			];
		}
		return $arr;
	}
	
	static function joinUpPassage($post) {
		
		$arr = [];
		if(!isset($post[ALL_PASSAGES])) return $arr;
		
		$len = count(getValue($post[ALL_PASSAGES], PASSAGE, []));
		
		for ($i = 0; $i < $len; $i++) {
			
			$arr[] = [
				PASSAGE => $post[ALL_PASSAGES][PASSAGE][$i],
				FROM_ => $post[ALL_PASSAGES][FROM_][$i],
				TO_ => $post[ALL_PASSAGES][TO_][$i],
				TABLE_ID => getValue(getValue($post[ALL_PASSAGES], TABLE_ID), $i),
			];
			
		}
		
		return $arr;
	}
	
	
	
	
	
	
	
	function course() {
		return $this->belongsTo(\App\Models\Courses::class, COURSE_CODE, COURSE_CODE);
	}
	
	function questions() {
		return $this->hasMany(\App\Models\Questions::class, SESSION_ID, TABLE_ID);
	}
	
	function instructions() {
		return $this->hasMany(\App\Models\Instructions::class, SESSION_ID, TABLE_ID);
	}
	
	function passages() {
		return $this->hasMany(\App\Models\Passages::class, SESSION_ID, TABLE_ID);
	}

	
	
}