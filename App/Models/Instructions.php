<?php
namespace App\Models;


class Instructions extends BaseModel {

	public $table = INSTRUCTIONS_TABLE;
	
	public $fillable = [TABLE_ID, COURSE_CODE, SESSION_ID, INSTRUCTION, FROM_, TO_];

	
	
	function session() {
		return $this->belongsTo(\App\Models\Sessions::class, SESSION_ID, TABLE_ID);
	}

}