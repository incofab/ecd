<?php

namespace App\Models;

class Questions extends BaseModel {

	public $table = QUESTIONS_TABLE;
	
	public $rules_insert = [
	    'required' =>  [ [QUESTION_NO], [QUESTION], [OPTION_A], [OPTION_B], [ANSWER] ],
	    'integer' =>  [ [QUESTION_NO] ],
	];

	function insert($post) 
	{
		$val = new \Valitron\Validator($post);

		$val->rules($this->rules_insert);
		$val->labels($this->labels);
		if (!$val->validate())
		{
			return [SUCCESSFUL => false, MESSAGE => 'Validation failed', 'error' => $val->errors()];
		}
		
		$this[COURSE_CODE] = $post[COURSE_CODE];
		$this[SESSION_ID] = $post[SESSION_ID];
		
		$this[QUESTION_NO] = $post[QUESTION_NO];
		$this[QUESTION] = $post[QUESTION];
		$this[OPTION_A] = $post[OPTION_A];
		$this[OPTION_B] = $post[OPTION_B];
		$this[OPTION_C] = array_get($post, OPTION_C);
		$this[OPTION_D] = array_get($post, OPTION_D);
		$this[OPTION_E] = array_get($post, OPTION_E);
		
		if(!empty($post[TOPIC_ID]))
		{
		    $this[TOPIC_ID] = array_get($post, TOPIC_ID);
		}
		
		$this[ANSWER] = $post[ANSWER];
		$this[ANSWER_META] = array_get($post, ANSWER_META);

		if ($this->save())
		{
		    return [SUCCESSFUL => true, MESSAGE => 'Data recorded successfully'];
		}
	    return [SUCCESSFUL => false, MESSAGE => 'Error: Data entry failed'];
	}
	
	function validateData($post)
	{
	    $val = new \Valitron\Validator($post);
	    
	    $val->rules($this->rules_insert);
	    $val->labels($this->labels);
	    
	    if (!$val->validate())
	    {
	        return [SUCCESSFUL => false, MESSAGE => 'Validation failed', 'error' => $val->errors()];
	    }
	    
	    return [SUCCESSFUL => true, MESSAGE => "Validated"];
	}

	function updateRecord($post) {

		$val = new \Valitron\Validator($post);

		$val->rules($this->rules_update);
		//Assign labels to the validator... Use to replace form name
		$val->labels($this->labels);
		
		if (!$val->validate()){
			\Session::flash('errors', $val->errors());
			\Session::flash('post', $post);
			return false;
		}
		
		//Check if row exists
		if (!$this->where(TABLE_ID, '=', $post[TABLE_ID])->first()){
			\Session::flash('errorMsg', 'There is no existing record, create new one');
			\Session::flash('post', $post);
			return false;
		}

		$arr = [];

		$arr[SESSION_ID] = $post[SESSION_ID];
		$arr[COURSE_CODE] = $post[COURSE_CODE];
		$arr[QUESTION_NO] = $post[QUESTION_NO];
		$arr[QUESTION] = $post[QUESTION];
		$arr[OPTION_A] = $post[OPTION_A];
		$arr[OPTION_B] = $post[OPTION_B];
		$arr[OPTION_C] = array_get($post, OPTION_C);
		$arr[OPTION_D] = array_get($post, OPTION_D);
		$arr[OPTION_E] = array_get($post, OPTION_E);
		
		if(!empty($post[TOPIC_ID]))
		{
		    $arr[TOPIC_ID] = array_get($post, TOPIC_ID);
		}
		
		$arr[ANSWER] = $post[ANSWER];
		$arr[ANSWER_META] = array_get($post, ANSWER_META);


		$success = $this->where(TABLE_ID, '=', $post[TABLE_ID])->update($arr);

		if ($success){
			\Session::flash('success', 'Record updated successfully');
			return true;
		}else {
			\Session::flash('errorMsg', 'Error: Update failed');
			\Session::flash('post', $post);
			return false;
		}

	}
	
	function getNumOfQuestions($courseCode, $sessionId)
	{
	    $sql = 'SELECT COUNT('.TABLE_ID.") AS count_query FROM ".QUESTIONS_TABLE
	    .' WHERE '.COURSE_CODE.'=:course_code  AND '.SESSION_ID.' = :session_id';
        
        $arr = [
            ':course_code' => $courseCode,
            ':session_id' => $sessionId,            
        ];
        
        $superArray = $this->pdoQuery($sql, $arr);
        
        return array_get($superArray, 'count_query', 0);
	}

	function course() {
		return $this->belongsTo(\App\Models\Courses::class, COURSE_CODE, COURSE_CODE);
	}

	function topic() {
		return $this->belongsTo(\App\Models\Topics::class, TOPIC_ID, TABLE_ID);
	}

	function session() {
		return $this->belongsTo(\App\Models\Sessions::class, SESSION_ID, TABLE_ID);
	}
	



}