<?php
namespace App\Core;

class CourseInstaller
{
    const INSTALL_FROM_HTML = false;
    
    public $coursesFolder = APP_DIR.'../public/courses/'.(self::INSTALL_FROM_HTML ? 'html/' : 'json/');
    
    const IS_INSTALLED_SUFFIX = '.installed.zip';
    
    private $extractionFolder = APP_DIR."../public/files/content/extracted/installed-courses/";
    
    private $coursesModel;
    private $sessionModel;
    private $passagesModel;
    private $instructionsModel;
    private $questionsModel;
    private $summaryModel;
    
    function __construct(
        \App\Models\Courses $coursesModel,
        \App\Models\Sessions $sessionModel,
        \App\Models\Passages $passagesModel,
        \App\Models\Instructions $instructionsModel,
        \App\Models\Questions $questionsModel,
        \App\Models\Summary $summaryModel
    ){
        $this->coursesModel = $coursesModel;
        
        $this->sessionModel = $sessionModel;
        
        $this->passagesModel = $passagesModel;
        
        $this->instructionsModel = $instructionsModel;
        
        $this->questionsModel = $questionsModel;
        
        $this->summaryModel = $summaryModel;
    }
    
    function isCourseInstalled($courseCode) 
    {
        return file_exists($this->coursesFolder.$courseCode.self::IS_INSTALLED_SUFFIX);
    }
    
    function canInstallCourse($courseCode) 
    {
        return file_exists($this->coursesFolder.$courseCode.'.zip');
    }
    
    function installCourse($courseCode) 
    {
        $ret = $this->startInstallation($courseCode);
        
        if($ret[SUCCESSFUL])
        {
            rename($this->coursesFolder.$courseCode.'.zip', $this->coursesFolder.$courseCode.self::IS_INSTALLED_SUFFIX);
            
            // Delete the folder with the extracted files
            $extractedFilesDir = $this->extractionFolder.$courseCode;
            
            if(is_dir($extractedFilesDir)) \App\Core\Helper::deleteDir($extractedFilesDir);
        }
        
        return $ret;
    }
    
    private function startInstallation($courseCode)
    {
        $contentPath = $this->coursesFolder.$courseCode.'.zip';
        
        if(file_exists($this->coursesFolder.$courseCode.self::IS_INSTALLED_SUFFIX)) 
        {
            return [SUCCESSFUL => false, MESSAGE => 'Content already installed'];
        }
        
        if(!file_exists($contentPath)) return [SUCCESSFUL => false, MESSAGE => 'Error: File not found'];
            
        ini_set('max_execution_time', 1440);  
        
        if(self::INSTALL_FROM_HTML)
        {
            $ret = $this->extractAndFormatHTMLContent($contentPath, $courseCode);
        }
        else 
        {
            $ret = $this->extractAndFormatJSONContent($contentPath, $courseCode);
        }
            
        return $ret;
    }
	
    private function extractAndFormatHTMLContent($contentPath, $courseCode)
    {
        $course = \App\Models\Courses::where(COURSE_CODE, '=', $courseCode)->first();
        
        if(empty($course)) return [SUCCESSFUL => false, MESSAGE => 'Error: Course not found'];
        
	    $zip = new \ZipArchive();
	    
	    $res = $zip->open($contentPath);
	    
	    if($res !== TRUE) die('<h2>File could not open</h2>');
	    
	    $contentDir = $this->extractionFolder.$courseCode;
	    
	    $zip->extractTo($contentDir);
	    $zip->close();
	    
	    $formatedSessionsAndQuestions = $this->formatSessionsAndQuestions($contentDir, $course);
	    
	    if(empty($formatedSessionsAndQuestions)) 
	    {
	        return [SUCCESSFUL => false, MESSAGE => 'Error: No content to install or Content formatting failed'];
	    }
	    
	    // Validate content
	    foreach ($formatedSessionsAndQuestions as $sessionAndQuestions)
	    {
	        $sessionData = $sessionAndQuestions['session_data'];
	        $questionsData = $sessionAndQuestions['questions'];
	        
	        $sessionValidation = $this->sessionModel->validateInsert($sessionData);
	        
	        if(!$sessionValidation[SUCCESSFUL])
	        {
	            dlog($sessionData);
	            
	            dDie($sessionValidation);
	        }
	        
	        // Validate questions
	        foreach ($questionsData as $question) 
	        {
	            $questionValidation = $this->questionsModel->validateData($question);
	            
	            if(!$questionValidation[SUCCESSFUL])
	            {
    	            dlog($sessionData);
    	            
    	            dlog($question);
    	            
    	            dDie($questionValidation);
	            }
	        }	        
	    }
	    // // Validation Complete
	    
	    // Start inserting to DB
	    foreach ($formatedSessionsAndQuestions as $sessionAndQuestions)
	    {
	        $this->insertSessionAndQuestionsRecord(
	            $sessionAndQuestions['session_data'], $sessionAndQuestions['questions']);
	    }
	    
	    return [SUCCESSFUL => true, MESSAGE => 'Data recorded'];
	}
	
	private function formatSessionsAndQuestions($courseDir, $courseData)
	{
	    $allSessions = [];
	    
	    $sessionsList = scandir($courseDir);
	    
	    foreach ($sessionsList as $file)
	    {
	        if(in_array($file, ['.', '..'])) continue;

	        $subjectDir = $courseDir.'/'.$file;
	        
	        if(!is_dir($subjectDir)) continue;
	        
	        $arr = str_getcsv($file, '_');
	        
	        if(empty($arr[0]) || empty($arr[1])) continue;
	        
	        $courseCode = $arr[0];
	        
	        $year = $arr[1];
	        
	        $obj = new \App\Parser\GenericParse($year, $subjectDir);
	        
	        $ret = $obj->parse($courseData);
	        
	        if(!$ret[SUCCESSFUL])
	        {
	            dlog($ret[SUCCESSFUL]);
	            
	            continue;
	        }
	        
	        $allSessions[] = $ret;
	    }
	    
	    return $allSessions;
	}
	
	private function insertSessionAndQuestionsRecord($sessionData, $formattedQuestions)
	{
	    $session = new \App\Models\Sessions();
	    
	    $session = $session->insert($sessionData);
	    
	    if(empty($session[SESSION]))
	    {
	        return [SUCCESSFUL => FALSE, MESSAGE => "Failed to record Acad Session"];
	    }
	    
	    foreach ($formattedQuestions as $questionsArr)
	    {
	        $questionsArr[SESSION_ID] = $session[TABLE_ID];
	        
	        $questionRet = (new \App\Models\Questions())->insert($questionsArr);
	        
	        if(empty($questionRet[SUCCESSFUL]))
	        {
	            dlog(array_merge($questionsArr, $questionRet));
	            
	            dDie($questionRet);
	        }
	    }
	    
	    return [SUCCESSFUL => TRUE, MESSAGE => "Data recorded"];
	}
	
	
	
	
	
	
	
	
	private function extractAndFormatJSONContent($contentPath, $courseCode)
	{
	    $course = \App\Models\Courses::where(COURSE_CODE, '=', $courseCode)->first();
	    
	    if(empty($course)) return [SUCCESSFUL => false, MESSAGE => 'Error: Course not found'];
	    
	    $zip = new \ZipArchive();
	    
	    $res = $zip->open($contentPath);
	    
	    if($res !== TRUE) die('<h2>File could not open</h2>');
	    
	    $contentDir = $this->extractionFolder.$courseCode;
	    
	    $zip->extractTo($contentDir);
	    $zip->close();
	    
	    $courseFilename = "$contentDir/course.json";
	    $sessionsFilename = "$contentDir/sessions.json";
	    $summaryFilename = "$contentDir/summary.json";
	    $summaryData = null;
	    
	    if(!file_exists($courseFilename))
	    {
	        return [SUCCESSFUL => false, MESSAGE => 'Error: Course file not found'];	        
	    }
	    
	    if(!file_exists($sessionsFilename))
	    {
	        return [SUCCESSFUL => false, MESSAGE => 'Error: Sessions file not found'];	        
	    }
	    
	    if(file_exists($summaryFilename))
	    {
    	    $summaryData = json_decode(file_get_contents($summaryFilename), true);
    	    
    	    $summaryVal = $this->validateSummary($summaryData);
    
    	    if(!$summaryVal[SUCCESSFUL]) return $summaryVal;
	    }
	    
	    $courseData = json_decode(file_get_contents($courseFilename), true);
	    
	    $validateCourse = $this->coursesModel->validateInsert($courseData);

	    if(!$validateCourse[SUCCESSFUL]) return $validateCourse;
	    
	    $allSessionData = json_decode(file_get_contents($sessionsFilename), true);
	    
	    $sessionVal = $this->validateSessions($allSessionData, $contentDir);

	    if(!$sessionVal[SUCCESSFUL]) return $sessionVal;
	    
	    // Now start saving records to database
        foreach ($allSessionData as $sessionData) 
        {
            // $updatedSessionData updated with the new DB record
    	    $updatedSessionData = $this->sessionModel->create([
                COURSE_CODE => $sessionData[COURSE_CODE],
                SESSION => $sessionData[SESSION],
                CATEGORY => $sessionData[CATEGORY],
                GENERAL_INSTRUCTIONS => $sessionData[GENERAL_INSTRUCTIONS],
    	    ]);
    	    
    	    $passages = $sessionData['passages'];
    	    
    	    foreach ($passages as $passage) 
    	    {
    	        $this->passagesModel->create([
                    COURSE_CODE => $passage[COURSE_CODE],
    	            SESSION_ID => $updatedSessionData[TABLE_ID],
    	            PASSAGE => $passage[PASSAGE],
    	            FROM_ => $passage[FROM_],
    	            TO_ => $passage[TO_],
    	        ]);
    	    }
    	    
    	    $instructions = $sessionData['instructions'];
        
    	    foreach ($instructions as $instruction) 
    	    {
    	        $this->instructionsModel->create([
    	            COURSE_CODE => $instructions[COURSE_CODE],
    	            SESSION_ID => $updatedSessionData[TABLE_ID],
    	            INSTRUCTION => $instructions[INSTRUCTION],
    	            FROM_ => $instructions[FROM_],
    	            TO_ => $instructions[TO_],
    	        ]);
    	    }
    	    
    	    $questionsFilename = "$contentDir/questions_{$sessionData[SESSION]}_{$sessionData[TABLE_ID]}.json";
    	    
    	    if(!file_exists($questionsFilename)) continue;
    	    
    	    $questionsData = json_decode(file_get_contents($questionsFilename), true);
    	    
    	    foreach ($questionsData as $question) 
    	    {
    	        $question[SESSION_ID] = $updatedSessionData[TABLE_ID];
    	        
    	        $questionInstallRet = (new \App\Models\Questions())->insert($question);
    	        
    	        if(!$questionInstallRet[SUCCESSFUL])
    	        {
    	            dDie($questionInstallRet);
    	        }
    	    }
    	    
    	    // Copy images
    	    $imageLocation = "$contentDir/img";
            $imagesFolder = APP_DIR."../public/img/content/$courseCode";
            
            if(is_dir($imageLocation))
            {
                if(is_dir($imagesFolder))
                {
                    \App\Core\Helper::deleteDir($imagesFolder, false);
                }
                else
                {
                    mkdir($imagesFolder, 0777, true);
                }
                
                \App\Core\Helper::copy($imageLocation, $imagesFolder);
            }
        }
        
        // Insert Summary Data
        if($summaryData)
        {
            foreach ($summaryData as $summary) 
            {
                (new \App\Models\Summary())->insert($summary);
            }
        }
        
        return [SUCCESSFUL => true, MESSAGE => "$courseCode installed successfully"];
	}
	
	private function validateSessions($allSessionData, $contentDir)
	{
	    foreach ($allSessionData as $sessionData) 
	    {
            $val = $this->sessionModel->validateInsert($sessionData);
	        
            if(!$val[SUCCESSFUL]) return $val;
            
            $questionsFilename = "$contentDir/questions_{$sessionData[SESSION]}_{$sessionData[TABLE_ID]}.json";
            
            if(!file_exists($questionsFilename))
            {
//                 return [SUCCESSFUL => false, MESSAGE => "Error: Questions file for {$sessionData[SESSION]} not found"];
                continue;
            }
            
            $questionsData = json_decode(file_get_contents($questionsFilename), true);
            
            $questionsVal = $this->validateQuestions($questionsData, $contentDir);

            if(!$questionsVal[SUCCESSFUL]) return $questionsVal;
	    }
	    
	    return [SUCCESSFUL => true, MESSAGE => 'Validation successful'];
	}
	
	private function validateQuestions($questionsData, $contentDir)
	{
	    foreach ($questionsData as $question) 
	    {
	        $ret = $this->questionsModel->validateInsert($question);
	        
	        if(!$ret[SUCCESSFUL])
	        {
	            return $ret;
	        }
	    }
	    
	    return [SUCCESSFUL => true, MESSAGE => 'Validation successful'];
	}
	
	private function validateSummary($summayData)
	{
	    foreach ($summayData as $summary)
	    {
	        $ret = $this->summaryModel->validateInsert($summary);
	        
	        if(!$ret[SUCCESSFUL])
	        {
	            return $ret;
	        }
	    }
	    
	    return [SUCCESSFUL => true, MESSAGE => 'Validation successful'];
	}
		
	
	function unInstallCourse($courseCode)
	{
	    $course = $this->coursesModel->where(COURSE_CODE, '=', $courseCode)->first();
	    
	    if(!$course) return [SUCCESSFUL => false, MESSAGE => htmlentities($courseCode).' not found'];
	    
	    $allSessions = $this->sessionModel->where(COURSE_CODE, '=', $courseCode)->get();
	    
	    /** @var \App\Models\Sessions $session */
	    foreach ($allSessions as $session) 
	    {
// 	        $session->questions()->delete();
	        
	        $this->questionsModel->where(COURSE_CODE, '=', $courseCode)
	           ->where(SESSION_ID, '=', $session[TABLE_ID])->delete();

	        $this->instructionsModel->where(COURSE_CODE, '=', $courseCode)
	           ->where(SESSION_ID, '=', $session[TABLE_ID])->delete();

	        $this->passagesModel->where(COURSE_CODE, '=', $courseCode)
	           ->where(SESSION_ID, '=', $session[TABLE_ID])->delete();

	        $this->summaryModel->where(COURSE_CODE, '=', $courseCode)->delete();
	           
	        $session->delete();
	    }
	    
	    $imagesFolder = APP_DIR."../public/img/content/$courseCode";
	    
        if(is_dir($imagesFolder))
        {
            \App\Core\Helper::deleteDir($imagesFolder, true);
        }
        
        rename($this->coursesFolder.$courseCode.self::IS_INSTALLED_SUFFIX, $this->coursesFolder.$courseCode.'.zip');
        
        return [SUCCESSFUL => true, MESSAGE => $courseCode.' uninstalled successfully'];
	}
	
}
