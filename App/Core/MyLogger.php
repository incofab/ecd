<?php
namespace App\Core;

use Symfony\Component\HttpKernel\Tests\Logger;

class MyLogger extends Logger
{
    
    function __construct()
    {
        parent::__construct();
    }
    
    function dlog($msg) 
    {
        $str = '';

        if(is_array($msg))
        {
             $str = json_encode($msg, JSON_PRETTY_PRINT);
        }
        else
        {
            $str = $msg;
        }
        
        error_log(
            
            '*************************************' . K_NEWLINE .
            
            '     Date Time: ' . date('Y-m-d h:m:s') . K_NEWLINE .
            
            '------------------------------------' . K_NEWLINE .
            
            $str . K_NEWLINE . K_NEWLINE .
            
            '*************************************' . K_NEWLINE,
            
            3, APP_DIR . '../public/errorlog.txt');
    }
    
}