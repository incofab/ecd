<?php
namespace App\Core;

class ErrorCodes
{
    const UNATHORIZED_ACCESS = 1;
    const INSUFFICIENT_BALANCE = 2;
}