<?php

new Session();
/**
 * A wrapper for Gears session
 * @author Incofab
 *
 */
class Session {
	
	private static $session;
	
	function __construct(){
		
		if (!empty(self::$session)) return;
		
		// Create a new gears session.
		$session = new \Gears\Session();
		
		// Configure the session container
		$session->dbConfig = self::getConnectionConfig();
		
		$session->lifetime = 2*60*60;
		
		// Install the session api
		$session->install();
		
		// Next you will probably want to make the session object global.
// 		$session->globalise();
		
		self::$session = $session;
	}
	
	static function get($key, $alt = null) { 
		return self::$session->get($key, $alt);
	}
	static function put($key, $val) {
		self::$session->set($key, $val);
	}
	
	static function flash($key, $val) {
		self::$session->flash($key, $val);
	}
	static function appendFlash($key, $val, $joiner = '<br />') {
		
		$previousFlash = self::getFlash($key);
		
		if(!is_array($previousFlash) && !is_array($val)){
			self::flash($key, $previousFlash . $joiner . $val);
		}elseif (is_array($previousFlash) && is_array($val)){
			self::flash($key, array_merge($previousFlash, $val));
		}else {
			throw new Exception('You can only appen strings to strings or merge arrays to arrays');
		}
	}
	
	static function getFlash($key, $alt = null) {
		return self::$session->get($key, $alt);
	}

	static function keepFlash() {
	    return self::$session->reflash();
	}
	
	static function getCsrfValue() {
        return '';
	    $csrf_value = self::get('_token');
		return $csrf_value;
	}
	
	static function isCsrfValid($csrf_value) {
	    return true;
	    return (self::get('_token') == $csrf_value);
	}

	static function clearSessionData() {
		return self::$session->clear();
	}
	static function clearFlash() {
        self::$session->forget('flash.old');
        self::$session->forget('flash.new');
	}
	static function clearFlashNow() {
        self::$session->forget('flash.old');
        self::$session->forget('flash.new');
	}

	static function test() {
	    dDie(self::$session->all());
	}
	static function allSessions() {
	    return self::$session->all();
	}

	static function getConnectionConfig() {
	    return [
	        'driver'    => 'mysql',
	        'host'      => 'localhost',
	        'database'  => DB_NAME,
	        'username'  => DB_USERNAME,
	        'password'  => DB_PASSWORD,
	        'charset'   => 'utf8',
	        'collation' => 'utf8_unicode_ci',
	        'prefix'    => '',
	    ];
	}
	
}