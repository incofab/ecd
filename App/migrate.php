<?php

// createAllTables();

function createAllTables() {
	
	echo 'Installing... <br /><br />';
	new \App\Migrations\Admin();
	echo '<br />'; echo '<br />';
	new \App\Migrations\Courses();
	echo '<br />'; echo '<br />';
	new \App\Migrations\Topics();
	echo '<br />'; echo '<br />';
	new \App\Migrations\Sessions();
	echo '<br />'; echo '<br />';
	new \App\Migrations\Instructions();
	echo '<br />'; echo '<br />';
	new \App\Migrations\Questions();
	echo '<br />'; echo '<br />';
	new \App\Migrations\Passages();
	echo '<br />'; echo '<br />';
	new \App\Migrations\Summary();
	echo '<br />'; echo '<br />';
	new \App\Migrations\Settings();
	
}
?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="<?= ROOT_FOLDER.'favicon.ico' ?>">
    <title>Fresh Installation</title>
    <!-- Bootstrap core CSS -->
    <link href="<?= ROOT_FOLDER.'public/lib/bootstrap4/css/bootstrap.min.css'?>" rel="stylesheet">
	<script type="text/javascript" src="<?= ROOT_FOLDER.'public/lib/jquery.min.js' ?>"></script>
  </head>
  <body>
    <header>
      <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
        <a class="navbar-brand" href="#">Fresh Installation</a>
      </nav>
    </header>
	<br /><br />
	<br /><br />
    <div class="container-fluid">
    	<div class="row">
    		<div class="offset-md-2 col-md-8 offset-sm-3 col-sm-6">
    			<div id="installing">
    				<div class="card">
    					<div class="card-body">
    						<?php createAllTables(); ?>
    					</div>
					</div>    				
    			</div>
    			<br /><br /><br />
				<div class="card mt-5 hide" id="installation_successful" >
					<div class="card-header"><div class="h2"><b>Congratulations!!!</b></div></div>
					<div class="card-body">
						<p class="card-text h5">You have successully completed this Installation</p>
						<br />
						<a class="btn btn-lg btn-primary" href="<?= getAddr('admin_dashboard') ?>">Launch Now</a>
					</div>
				</div>
			</div>
    	</div>
    </div>
<script type="text/javascript" src="<?= ROOT_FOLDER.'public/lib/bootstrap4/js/bootstrap.bundle.min.js' ?>"></script>
<style>
.hide{display: none;}
</style>
<script type="text/javascript">
	$(function() {
		$('#installing').addClass('hide');
		$('#installation_successful').removeClass('hide');
	});
</script>
</body>
</html>





