<?php
namespace App\Controllers;

class BaseController{

    protected $session;
    protected $container;
    
    protected $page;
    protected $num = 1000;
    
    function __construct(\Bootstrap\Container\MyContainer $c, \Session $session) 
    {
	    $this->session = $session;
	    
	    $this->container = $c->getContainerInstance();
	    
	    $this->page = array_get($_REQUEST, 'page', 1);
	}
	 
	protected function isUserLoggedIn()
	{
	    $data = $this->session->get(USER_SESSION_DATA, null);
	    
	    if(!$data) return null;
	    
	    return $data;
	}
	
	protected function view($view, array $data = [])
	{
// 	    $data['data'] = isset($data['data']) ? $data['data'] : $this->isUserLoggedIn();

	    $data['next'] = isset($data['next']) ? $data['next'] : getAddr('');
		
	    $data['page'] = isset($data['page']) ? $data['page'] : $this->page;

	    $data['numPerPage'] = isset($data['numPerPage']) ? $data['numPerPage'] : $this->num;
	    
	    $data['sessionModel'] = $this->session;
	    
	    return view($view, $data);
	}
	
	protected function logout_($routeTo, $fromAPI = false) 
	{
		\Session::clearSessionData();
	
		\Session::flash('report', 'You are logged out');
		
		if($fromAPI) return [SUCCESSFUL => true, MESSAGE => 'User logged out'];
		
		redirect_(getAddr($routeTo));
	}
	
	protected function checkCSRFToken($token, $redirectOnFailure, $routeName = null) 
	{
	    //Check CSRF token match
	    if (!$this->session->isCsrfValid($token))
	    {
	        if($redirectOnFailure) 
	        {
    	        $this->session->flash('error', 'Token Mismatch');
    	        
	            redirect_(getAddr($routeName));
	        }
	        return false;
	    }
	    return true;
	}
		
	


}