<?php
namespace App\Controllers\Admin;


use App\Controllers\BaseController;

class BaseAdmin extends BaseController{

    protected $data = [];
    private $adminObj = null;
	
	function __construct(
	    \Bootstrap\Container\MyContainer $c,
	    \Session $session)  
	{
	    parent::__construct($c, $session);
	    
	    if (!$this->session->get(ADMIN_SESSION_DATA, false))
	    {
	        $this->session->flash('error', 'You are not logged in');
	        
	        redirect_(getAddr('admin_login', '?next=' . getAddr('')));
	    }
	    
	    $this->data = $this->session->get(ADMIN_SESSION_DATA);
	}
	
	protected function view($view, array $data = [])
	{
	    $data['data']    = isset($data['data'])    ? $data['data']    : $this->data;
	    
	    $data['isAdmin'] = isset($data['isAdmin']) ? $data['isAdmin'] : true;
	    
	    return parent::view($view, $data);
	} 
	
	protected function getAdminObj()
	{
	    if($this->adminObj) return $this->adminObj;
	    
	    $adminModel = $this->container->get(\App\Models\Admin::class);
	    
	    $this->adminObj = $adminModel->where(USERNAME, '=', $this->data[USERNAME])
	    ->where(PASSWORD, '=', $this->data[PASSWORD])->first();
	    
	    if(!$this->adminObj)
	    {
	        $this->session->clearSessionData();
	        
	        die('Invalid Login credentials');
	    }
	    
	    return $this->adminObj;
	}
	
	function checkAccessLevel($level)
	{
	    if($level >= $this->data[LEVEL]) return;
	    
	    $this->session->flash('error', 'Sorry, Access denied');
	    
	    redirect_(getAddr('admin_dashboard'));
	}
	
		
	
}