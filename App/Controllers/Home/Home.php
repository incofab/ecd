<?php
namespace App\Controllers\Home;

use App\Controllers\BaseController;

class Home extends BaseController
{
    private $carbon;
    
    function __construct(
        \Bootstrap\Container\MyContainer $c,
        \Session $session,
        \Carbon\Carbon $carbon
    ){
        parent::__construct($c, $session);
        
        $this->carbon = $carbon;  
    }
    
	function index() 
	{
	    redirect_(getAddr('admin_login'));
	    
// 	    return view('home/index', []); 
    }
	
	function developers() { return $this->view('home/developers', []); }
	
	function contactUs() { return $this->view('home/contact_us', []); }
	
	function aboutUs() { return $this->view('home/about_us', []); }
	
	function privacyPolicy() { return $this->view('home/privacy_policy', []); }
	
	function terms() { return $this->view('home/terms', []); }

	function howItWorks() { return $this->view('home/how_it_works', []); }
		
	function message($type = null) 
	{ 
	    $messageTitle = SITE_TITLE;
	    
	    $messageBody = $this->session->getFlash('message', 'No Message');
	    
	    if($type == 'exam_submitted')
	    {
    	    $messageTitle = 'Congratulations!';
	        
    	    $messageBody = 'You have successfully completed your test.';
	    }
	    else if($type == 'exam_paused')
	    {	        
    	    $messageTitle = 'Exam Paused!';
	        
    	    $messageBody = 'This exam has been paused.';
	    }
	    
        return $this->view('home/message', [
            'messageTitle' => $messageTitle,
            'messageBody' => $messageBody,
        ]); 
	}
}




