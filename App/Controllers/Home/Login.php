<?php
namespace App\Controllers\Home;

use App\Controllers\BaseController;

class Login extends BaseController{

    private $adminModel; 
    private $cookieManager;  
    private $loginHelper;  
    
	function __construct(
    	    \Bootstrap\Container\MyContainer $c, 
    	    \Session $session, 
    	    \App\Models\Admin $adminModel,
	        \App\Core\CookieManager $cookieManager,
	        \App\Core\Login $loginHelper
    ){
		parent::__construct($c, $session);
		
		$this->adminModel = $adminModel;
		$this->cookieManager  = $cookieManager;
		$this->loginHelper  = $loginHelper;
		
		// First check if user session is still active
		if($this->session->get(USER_SESSION_DATA))
		{
// 			redirect_(getAddr('user_dashboard'));
		}
		else if($this->session->get(CENTER_SESSION_DATA))
		{
// 			redirect_(getAddr('center_dashboard'));
		}
		else if($this->session->get(ADMIN_SESSION_DATA))
		{
// 			redirect_(getAddr('admin_dashboard'));
		}
		
		// Check if the remember login is still active
		if($remLogin = array_get($_COOKIE, USER_REMEBER_LOGIN))
		{
// 			$this->loginFromCookie($remLogin, 'user');
		}
		elseif($remLogin = array_get($_COOKIE, ADMIN_REMEBER_LOGIN))
		{
// 			$this->loginFromCookie($remLogin, 'admin'); 
		}
		
	}
	
	function adminLogin() 
	{
		if (!$_POST)
		{
			return view('admin/login', ['username' => array_get($_COOKIE, 'admin_username')]);
		}
		
		$this->checkCSRFToken(array_get($_POST, CSRF_TOKEN), true);
		
		$ret = $this->loginHelper->loginAdmin($this->adminModel, $_POST);
		
		if(!$ret[SUCCESSFUL])
		{
		    $this->session->flash('error', $ret[MESSAGE]);
		    
		    redirect_(getAddr(null));
		}
		
		setcookie('admin_username', $ret['data'][USERNAME], time() + (14 * 24 * 60 * 60)); // 2 weeks
		
		if($ret[REMEMBER_LOGIN])
		{
		    $this->cookieManager->setLoginCookie('admin', $ret[REMEMBER_LOGIN]);
		}
		
		$this->session->put(ADMIN_SESSION_DATA, $ret['data']->toArray());
		
		redirect_(array_get($_GET, 'next', getAddr('admin_dashboard')));
	}
	
}