<?php
namespace App\Controllers\CCD;

use Illuminate\Database\Capsule\Manager as Capsule;

class Courses extends BaseCCD
{	
    private $topicModel;
    private $courseModel;
    
    function __construct(
        \Bootstrap\Container\MyContainer $c, 
        \Session $session,
        \App\Models\Courses $courseModel,
        \App\Models\Topics $topicModel
    ){
        parent::__construct($c, $session);
        
        $this->topicModel = $topicModel;
        
        $this->courseModel = $courseModel;
    }
    
	function index() {
		die('index');
	}
	
	function viewAll() { 

		$obj = (new \App\Models\Courses());
		
		if (!Capsule::schema()->hasTable($obj->table)){
			return  $this->view('layout', [
					'error' => '<h3>Seems like a fresh installation, <a href="' .  
					getAddr('') . '">Click Here</a> to initialize database</h3>']);
		}
		
		$allRegdCourses = \App\Models\Courses::orderBy(TABLE_ID, 'DESC')->get(); 
		
		return  $this->view('ccd/courses/view_all', [
				'allRegdCourses' => $allRegdCourses, 
				'error' => \Session::getFlash('error') ,
				'success' => \Session::getFlash('success'), 
				'report' => \Session::getFlash('report')
		]);
		
	}
	
	function registerCourses() {
// 		die('nkhjjgjgj');
		if (!empty($_POST['register_course'])){
			(new \App\Models\Courses())->insert($_POST);
			redirect_($_SERVER['REQUEST_URI']);
		}
		
		if (\Session::getFlash('success')){
			\Session::keepFlash();
			redirect_(getAddr('ccd_all_courses'));
		}
		
		return $this->view('ccd/courses/register_course', [
				'errorMsg' => \Session::getFlash('errorMsg'), 
				'errors' => \Session::getFlash('errors'),
				'post' =>  \Session::getFlash('post', [])
		]);
		 
	}
	
	function editCourse($table_id) {
	
		if (!empty($_POST['update_course'])){
			(new \App\Models\Courses())->updateRecord($_POST);
			redirect_($_SERVER['REQUEST_URI']);
		}
		
		if (\Session::getFlash('success')){
			\Session::keepFlash();
			redirect_(getAddr('ccd_all_courses'));
		}
	
		$post = \Session::getFlash('post', \App\Models\Courses::where(TABLE_ID, '=', $table_id)->first());
		
		if (empty($post)){
			\Session::flash('error', 'Record not found or invalid or expired');
			redirect_(getAddr('ccd_all_courses'));
		}
		
		return $this->view('ccd/courses/register_course', [
				'errorMsg' => \Session::getFlash('errorMsg'), 
				'errors' => \Session::getFlash('errors'),
				'post' =>  $post, 'edit' => 'true',
				'table_id' => $table_id
		]);
		
	}
	
	function delete($table_id) {
		
		$success = \App\Models\Courses::where(TABLE_ID, '=', $table_id)->delete();
		
		if ($success){
			\Session::flash('success', 'Record deleted successfully');
		}else {
			\Session::flash('error', 'Delete failed');
		}
		
		redirect_(getAddr('ccd_all_courses'));
	}	
	
}
