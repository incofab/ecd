<?php
namespace App\Controllers\CCD;

class Topic extends BaseCCD
{	
    private $topicModel;
    private $courseModel;
    private $questionModel;
    
    function __construct(
        \Bootstrap\Container\MyContainer $c, 
        \Session $session,
        \App\Models\Courses $courseModel,
        \App\Models\Questions $questionModel,
        \App\Models\Topics $topicModel
    ){
        parent::__construct($c, $session);
        
        $this->topicModel = $topicModel;
        
        $this->courseModel = $courseModel;
        
        $this->questionModel = $questionModel;
    }
	
	function addTopic($courseCode) 
	{
	    $course = $this->courseModel->where(COURSE_CODE, '=', $courseCode)->first();
	    
	    if (!$course)
	    {
	        $this->outputResponse([SUCCESSFUL => false, MESSAGE => 'Course not found']);
	    }
	    
	    $_POST[COURSE_CODE] = $course[COURSE_CODE];
	    
	    $ret = $this->topicModel->insert($_POST);
	    
	    $this->outputResponse($ret);
	}
	
	function editTopic($tableId) 
	{
	    $_POST[TABLE_ID] = $tableId;
	    
	    $ret = $this->topicModel->updateRecord($_POST);
	    
        $this->outputResponse($ret);
	}
	
	function delete($table_id) 
	{
	    if($this->questionModel->where(TOPIC_ID, '=', $table_id)->first())
	    {
		    $this->outputResponse([SUCCESSFUL => false, MESSAGE => 'Failed: Cannot delete a topic already assigned to a question']);	        
	    }
	    
		$success = $this->topicModel->where(TABLE_ID, '=', $table_id)->delete();
		
		if ($success)
		{
		    $this->outputResponse([SUCCESSFUL => true, MESSAGE => 'Record deleted successfully']);
		}
		else 
		{
		    $this->outputResponse([SUCCESSFUL => false, MESSAGE => 'Delete failed']);
		}
	}

	private function outputResponse($data) 
	{
	    die(json_encode($data, JSON_PRETTY_PRINT));
	}
	
	
}
