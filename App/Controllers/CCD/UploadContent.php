<?php
namespace App\Controllers\CCD;

class UploadContent extends BaseCCD
{    
    private $sessionModel;
    private $multiContentUploader;
    private $courseInstaller;
    private $exportContent;
    
    function __construct(
        \Bootstrap\Container\MyContainer $c,
        \Session $session, 
        \App\Models\Sessions $sessionModel,
        \App\Core\UploadMultipleContent $multiContentUploader,
        \App\Controllers\Helpers\ExportContent $exportContent,
        \App\Core\CourseInstaller $courseInstaller
    ){
        parent::__construct($c, $session);
        
        $this->sessionModel = $sessionModel;

        $this->multiContentUploader = $multiContentUploader;
        
        $this->courseInstaller = $courseInstaller;
        
        $this->exportContent = $exportContent;
    }
	
	function uploadContent() 
	{  
        if(!$_POST)
        {
            $courses = \App\Models\Courses::orderBy(TABLE_ID, 'DESC')->get();
            
            if(!$courses->first())
            {
                $this->session->flash('error', 'No subject registered yet');
                
                redirect_(getAddr('ccd_register_course'));
            }
            
    		return  $this->view('ccd/upload_content', [
				'error' => \Session::getFlash('error') ,
				'success' => \Session::getFlash('success'),  
				'report' => \Session::getFlash('report'),
				'courses' => $courses,
    		]);
        }
        
        $courseCode = array_get($_REQUEST, COURSE_CODE);
        
        $course = \App\Models\Courses::where(COURSE_CODE, '=', $courseCode)->first();
        
        if(!$course)
        {
            $this->session->flash('error', 'Subject record not found');
            
            redirect_(getAddr('ccd_upload_content'));            
        }
        
        $subject = array_get($course, COURSE_CODE);
        $subjectFullname = array_get($course, COURSE_TITLE);
        $year = array_get($_REQUEST, SESSION);
        $extractionFolder = APP_DIR . "../public/files/content/extracted";
        $ret = $this->uploadFile($_FILES);
        
        if(!$ret[SUCCESSFUL])
        {
            $this->session->flash('error', $ret[MESSAGE]);
            
            redirect_(getAddr('ccd_upload_content'));            
        }
        
        $zip = new \ZipArchive();
        $res = $zip->open($ret['full_path']);
        $folderName = pathinfo($ret['full_path'], PATHINFO_FILENAME);
        
        if($res !== TRUE) die('<h2>File could not open</h2><h4>'.array_get($ret, MESSAGE).'</h4>');

        $subjectDir = "$extractionFolder/$folderName";
        
        $zip->extractTo($subjectDir);
        $zip->close();
//         die('We\'re done.');
        $obj = new \App\Parser\GenericParse($year, $subjectDir);
        
        $ret = $obj->parse($course);
        
        if(!$ret[SUCCESSFUL])
        {
            $this->session->flash('error', $ret[MESSAGE]);
            
            $this->session->flash('post', $_REQUEST);
        }
        
        $ret = $obj->insertSessionAndQuestionsRecord($ret['session_data'], $ret['questions']);
        
        if(!$ret[SUCCESSFUL])
        {
            $this->session->flash('error', $ret[MESSAGE]);
            
            $this->session->flash('post', $_REQUEST);
        }
        else 
        {
            $this->session->flash(SUCCESSFUL, $ret[MESSAGE]);            
        }
        redirect_(null);
	} 
	
	private function uploadFile($files, $destinationPath) 
	{
	    if(!isset($files['content'])) return [SUCCESSFUL => false, MESSAGE => 'Invalid File'];
	    
	    // First check if file type is image
	    $validExtensions = array("zip");
	    $maxFilesize = 10000000; // 10mb
	    
	    $name = $files['content']["name"];
	    
	    $ext = pathinfo($name, PATHINFO_EXTENSION);
	    $originalFilename = pathinfo($name, PATHINFO_FILENAME);
	    
	    if($files['content']["size"] > $maxFilesize) return [SUCCESSFUL => false, MESSAGE => 'File greater than 10mb'];
	    
	    if(!in_array($ext, $validExtensions)) return [SUCCESSFUL => false, MESSAGE => 'Invalid file Extension'];
	    
	    // Check if the file contains errors
	    if($files['content']["error"] > 0) return [SUCCESSFUL => false, MESSAGE => "Return Code: " . $files['content']["error"]];
	    	    
	    $tempPath = $files['content']['tmp_name'];
	    
	    @move_uploaded_file($tempPath, $destinationPath); // Moving Uploaded file
	    
	    return [SUCCESSFUL => true, MESSAGE => "File uploaded successfully", 'full_path' => $destinationPath];
	}
	
	function installBonusContent($which = 1)
	{
	    if($which == 2) 
	    {
	        $ret = $this->multiContentUploader->installBonusContent2();
	    }
        else 
        {
            $ret = $this->multiContentUploader->installBonusContent();
        }

        $this->session->flash($ret[SUCCESSFUL] ? SUCCESSFUL : 'error', $ret[MESSAGE]);

        redirect_(getAddr('ccd_home'));
	}
	
	function installCourse($courseCode = null)
	{
	    if(empty($courseCode))
	    {
	        $courses = \App\Models\Courses::all();
	        
	        return  $this->view('admin/view_courses', [
	            'courses' => $courses,
	            'courseInstaller' => $this->courseInstaller
	        ]);
	    }
	    
	    $course = \App\Models\Courses::where(COURSE_CODE, '=', $courseCode)->first();
	    
	    if(!$course)
	    {
	        $this->session->flash('error', htmlentities($courseCode).' not found');
	        
	        redirect_(array_get($_REQUEST, 'next', getAddr('ccd_home')));
	    }
	    
	    if(!$_POST)
	    {	        
	        return  $this->view('admin/upload_content', [
	            'course' => $course,
	        ]);
	    }
	    
	    $filename = "{$this->courseInstaller->coursesFolder}$courseCode.zip";
	    
	    if(file_exists($filename)) unlink($filename);
	    
	    $ret = $this->uploadFile($_FILES, $filename);
	    
	    if(!$ret[SUCCESSFUL])
	    {
	        $this->session->flash('error', $ret[MESSAGE]);
	        
	        redirect_(null);
	    }
	    
        $ret = $this->courseInstaller->installCourse($courseCode);

        $this->session->flash($ret[SUCCESSFUL] ? SUCCESSFUL : 'error', $ret[MESSAGE]);

        redirect_(array_get($_REQUEST, 'next', getAddr('admin_dashboard')));
	}
	
	function unInstallCourse($courseCode)
	{
	    if(!$_POST)
	    {
	        return  $this->view('admin/confirm_uninstall', [
	            'courseCode' => $courseCode,
	            'post' => $this->session->getFlash('post', [])
	        ]);
	    }
	    
	    if(!comparePasswords($this->getAdminObj()[PASSWORD], $_POST[PASSWORD]))
	    {
	        $this->session->flash('error', 'Invalid Password');
	        
	        redirect_(null);
	    }
	    
        $ret = $this->courseInstaller->unInstallCourse($courseCode);

        $this->session->flash($ret[SUCCESSFUL] ? SUCCESSFUL : 'error', $ret[MESSAGE]);

        redirect_(array_get($_REQUEST, 'next', getAddr('ccd_home')));
	}
	
	function exportCourse($courseCode = null) 
	{
	    if(empty($courseCode))
	    {
	        $courses = \App\Models\Courses::all();
	        
	        return  $this->view('admin/view_courses', [
	            'courses' => $courses,
	            'courseInstaller' => $this->courseInstaller
	        ]);
	    }
	    
        $course = \App\Models\Courses::where(COURSE_CODE, '=', $courseCode)->first();
	    
        if(!$course)
        {
            $this->session->flash('error', htmlentities($courseCode).' not found');
    
            redirect_(array_get($_REQUEST, 'next', getAddr('ccd_home')));
        }
	    
	    $this->exportContent->exportCourse($course);
	    
	    die('DOne');
	}
		
}
