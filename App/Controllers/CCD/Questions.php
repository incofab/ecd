<?php
namespace App\Controllers\CCD;

class Questions extends BaseCCD
{    
    private $topicsModel;
    
    function __construct(
        \Bootstrap\Container\MyContainer $c,
        \Session $session,
        \App\Models\Topics $topicsModel
    ){
        parent::__construct($c, $session);
        
        $this->topicsModel = $topicsModel;
    }
	
	function viewAllQuestions($courseCode, $session_id) {  
 
		$obj = (new \App\Models\Questions());
		
		$allSessionQuestions =  $obj->where(SESSION_ID, '=', $session_id)
				->where(COURSE_CODE, '=', $courseCode)->get();
		
		$acadSession = [];
		if($allSessionQuestions->first()){
			$acadSession = $allSessionQuestions->first()->session()->first();
		}
		
		return  $this->view('ccd/question/view_session_questions', [
				'allCourseYearQuestions' => $allSessionQuestions, 
				'error' => \Session::getFlash('error') ,
				'success' => \Session::getFlash('success'),  
				'report' => \Session::getFlash('report'),
				'year_id' => $session_id, 'courseName' => $courseCode,
				'year' => getValue($acadSession, SESSION),
		]);
		 
	} 
	
	function addNewQuestion($courseCode, $session_id, $num = 1) {
		
	    if(!$_POST)
	    {
	        $obj = (new \App\Models\Questions());
	        
	        $courseSession = \App\Models\Sessions::where(TABLE_ID, '=', $session_id)
	               ->with(['course', 'course.topics'])->first();
	        
	        $allSessionQuestions =  $obj->where(SESSION_ID, '=', $session_id)
	               ->where(COURSE_CODE, '=', $courseCode)->get();
	        
           if(!$allSessionQuestions->first()) $num = 1;
           
           else $num = $allSessionQuestions->last()[QUESTION_NO] + 1;
           
           $allTopics = $courseSession['course']['topics'];
           
    		return $this->view('ccd/question/add_question', [
                'errorMsg' => \Session::getFlash('errorMsg'), 
                'errors' => \Session::getFlash('errors'),
                'post' =>  \Session::getFlash('post', []),
                'year_id' =>  $session_id, 'num' => $num,
                'courseName' => $courseCode,
    		    'allTopics' => $allTopics,
    		    'topic' => null,
                'year' => $courseSession[SESSION]
    		]);
	    }
	    
	    $ret = (new \App\Models\Questions())->insert($_POST);
	    \Session::flash($ret[SUCCESSFUL] ? 'success' : 'errorMsg', $ret[MESSAGE]);
		
		if ($ret[SUCCESSFUL])
		{
			redirect_(getAddr('ccd_new_session_question', [$courseCode, $session_id, $_POST[QUESTION_NO] + 1 ]));
		}
		
	    \Session::flash('post', $_POST);
	    
		redirect_(null);
	}
	
	function addNewQuestionAPI() 
	{
	    $ret = (new \App\Models\Questions())->insert($_POST);
	    
        return json_encode($ret);
	}
	
	function editQuestion($courseCode, $session_id, $table_id) {
	
		if (!empty($_POST['update_question'])){
			(new \App\Models\Questions())->updateRecord($_POST);
			redirect_($_SERVER['REQUEST_URI']);
		}
		
		if (\Session::getFlash('success')){
// 			\Session::keepFlash();
			redirect_(array_get($_REQUEST, 'next', getAddr('ccd_all_session_questions', [$courseCode, $session_id])));
		}
		
		$question = \App\Models\Questions::where(TABLE_ID, '=', $table_id)->with(['topic'])->first();
		
		if (empty($question)){ 
			\Session::flash('error', 'Record not found or invalid or expired');
			redirect_(getAddr('ccd_all_session_questions', [$courseCode, $session_id]));
		}
		
		$post = \Session::getFlash('post', $question);
		
		$courseSession = \App\Models\Sessions::where(TABLE_ID, '=', $session_id)
		          ->with(['course', 'course.topics'])->first();
		
        $topic = $question['topic'];
		
		$post[TOPIC_ID] = array_get($topic, TABLE_ID);
		
		$post['topic_title'] = array_get($topic, TITLE);
		
		$allTopics = $courseSession['course']['topics'];
		
		return $this->view('ccd/question/add_question', [
				'errorMsg' => \Session::getFlash('errorMsg'), 
				'errors' => \Session::getFlash('errors'),
				'post' =>  $post, 'year_id' => $post[SESSION_ID], 
				'edit' => 'true', 'num' => $post[QUESTION_NO],
    		    'courseName' => $courseCode, 'table_id' => $table_id,
    		    'year' => $courseSession[SESSION],
    		    'allTopics' => $allTopics,
		        'topic' => $topic,
    		    'next' => array_get($_REQUEST, 'next')
		]);
		
	}
	 
	function delete($courseCode, $session_id, $table_id) {
		
		$success = \App\Models\Questions::where(TABLE_ID, '=', $table_id)->delete();
		
		if ($success){
			\Session::flash('success', 'Record deleted successfully');
		}else {
			\Session::flash('error', 'Delete failed');
		}
		
		redirect_(getAddr('ccd_all_session_questions', [$courseCode, $session_id]));
		
	}
	 
	function previewSingleSessionQuestion($courseCode, $session_id, $table_id) {
		 
		$questionObj = \App\Models\Questions::where(TABLE_ID, '=', $table_id)
		          ->with(['session', 'session.course', 'topic'])->first();
		
		if (!$questionObj) return $this->view('common/message', [ 'errors' => 'No Record found' ]);
		
// 		$session = $questionObj->session()->first();
		$session = $questionObj['session'];
		
		$allPassages = $questionObj->session()->first()->passages()->get();
		$allInstructions = $questionObj->session()->first()->instructions()->get();
		
		return $this->view('ccd/question/preview_single_question', [
	        'year' => $session[SESSION],
			'courseName' => $courseCode,
			'questionObj' =>  $questionObj,
// 			'topic' =>  $questionObj['topic'],
			'year_id' => $session_id,
			'allPassages' => $allPassages,
		    'allInstructions' => $allInstructions,
		]);
		
	}
	
	
	
}
