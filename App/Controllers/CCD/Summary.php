<?php

namespace App\Controllers\CCD;

class Summary extends BaseCCD{
    
    function __construct(
        \Bootstrap\Container\MyContainer $c,
        \Session $session
    ){
        parent::__construct($c, $session);
    }
	 
	function viewAll($courseCode) {  
 		
		$allCourseSummary = \App\Models\Summary::where(COURSE_CODE, '=', $courseCode)->get();
			
		return  $this->view('ccd/summary/view_all', [
				'allCourseChapterSummary' => $allCourseSummary, 
				'error' => \Session::getFlash('error') ,
				'success' => \Session::getFlash('success'), 
				'report' => \Session::getFlash('report'),
				'courseName' => $courseCode,
		]);
		
	}
	
	function newChapter($courseCode) {
		
		if (!empty($_POST['register_course_summary_chapter'])){
			(new \App\Models\Summary())->insert($_POST);
			redirect_($_SERVER['REQUEST_URI']);
		}
		
		if (\Session::getFlash('success')){
			\Session::keepFlash();
			redirect_(getAddr('ccd_view_all_chapter', $courseCode));
		}
		
		return $this->view('ccd/summary/add_new_chapter', [
				'errorMsg' => \Session::getFlash('errorMsg'), 
				'errors' => \Session::getFlash('errors'),
				'post' =>  \Session::getFlash('post', []),
				'courseName' =>  $courseCode,
		]);
		  
	}
	 
	function editChapter($courseCode, $chapter_id) {
	
		if (!empty($_POST['update_course_summary_chapter'])){
			(new \App\Models\Summary())->updateRecord($_POST);
			redirect_($_SERVER['REQUEST_URI']);
		}
		
		if (\Session::getFlash('success')){
			\Session::keepFlash();
			redirect_(getAddr('ccd_view_all_chapter', $courseCode));
		}
	
		$post = \Session::getFlash('post', \App\Models\Summary::where(TABLE_ID, '=', $chapter_id)->first());
		
		if (empty($post)){
			\Session::flash('error', 'Record not found or invalid or expired');
			redirect_(getAddr('ccd_view_all_chapter', $courseCode));
		}
		
		return $this->view('ccd/summary/add_new_chapter', [
				'errorMsg' => \Session::getFlash('errorMsg'), 
				'errors' => \Session::getFlash('errors'),
				'post' =>  $post, 'edit' => 'true',
				'chapter_id' => $chapter_id,
				'courseName' => $courseCode,
		]);
		
	}
	 
	function preview($courseCode, $chapter_id) {
		
		$previewData = \App\Models\Summary::where(TABLE_ID, '=', $chapter_id)->first();
		
			return $this->view('ccd/summary/preview_chapter', [
					'previewData' =>  $previewData,
					'chapter_id' => $chapter_id,
					'courseName' => $courseCode,
			]);
		
	}
	
	function delete($courseCode, $chapter_id) {
		
		$success = \App\Models\Summary::where(TABLE_ID, '=', $chapter_id)->delete();
		
		if ($success){
			\Session::flash('success', 'Record deleted successfully');
		}else {
			\Session::flash('error', 'Delete failed');
		}
		 
		redirect_(getAddr('ccd_view_all_chapter', $courseCode));
		
	}
	
	
	
}
