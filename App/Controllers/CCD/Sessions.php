<?php
namespace App\Controllers\CCD;

class Sessions extends BaseCCD{
	
    function __construct(
        \Bootstrap\Container\MyContainer $c,
        \Session $session
    ){
        parent::__construct($c, $session);
    }
	
	/**
	 * Retrieves all academic session of the supplied course
	 * @param unknown $courseName
	 * @return unknown
	 */
	function viewAllSessions($courseCode) { 
		
		$allCoursesYears =  \App\Models\Sessions::where(COURSE_CODE, '=', $courseCode)->get();
					
		return  $this->view('ccd/session/view_all_sessions', [
				'allCoursesYears' => $allCoursesYears, 
				'courseName' => $courseCode, 
				'error' => \Session::getFlash('error') ,
				'success' => \Session::getFlash('success'), 
				'report' => \Session::getFlash('report')
		]);
		 
	}
	
	/**
	 * Register a new academic session for the supplied course
	 * @param unknown $courseCode
	 * @return unknown
	 */
	function registerSession($courseCode) {
		
		if (!empty($_POST['register_course_year'])){ 
			
			(new \App\Models\Sessions())->insert($_POST);
			redirect_($_SERVER['REQUEST_URI']);
		}
		
		if (\Session::getFlash('success')){
			\Session::keepFlash();
			redirect_(getAddr('ccd_all_sessions', $courseCode));
		}
		
		$post = \Session::getFlash('post', []);
		$allInstructions = \App\Models\Sessions::joinUpInstruction($post);
		$allPassages = \App\Models\Sessions::joinUpPassage($post);
		
		return $this->view('ccd/session/register_session', [
				'errorMsg' => \Session::getFlash('errorMsg'), 
				'errors' => \Session::getFlash('errors'),
				'post' =>  $post,
				'allInstructions' =>  $allInstructions,
				'allPassages' =>  $allPassages,
				'courseName' => $courseCode
		]);
		 
	} 
	 
	function editSession($courseCode, $session_id) { 
	
		if (!empty($_POST['update_regd_course_year'])){
			(new \App\Models\Sessions())->updateRecord($_POST);
			redirect_($_SERVER['REQUEST_URI']);
		}
		
		if (\Session::getFlash('success')){
			\Session::keepFlash();
			redirect_(getAddr('ccd_all_sessions', $courseCode)); 
		}
		
		$post = \Session::getFlash('post', \App\Models\Sessions::where(TABLE_ID, '=', $session_id)->first());

		$post = \Session::getFlash('post', []);
		if(!empty($post)){
			$allInstructions = \App\Models\Sessions::joinUpInstruction($post);
			$allPassages = \App\Models\Sessions::joinUpPassage($post);
		}else{
			
			$post = \App\Models\Sessions::where(TABLE_ID, '=', $session_id)->first();

			if (empty($post)){
				\Session::flash('error', 'Record not found or invalid or expired');
				redirect_(getAddr('ccd_all_sessions', $courseCode));
			}
			
			$allInstructions = $post->instructions()->get();
			$allPassages 	 = $post->passages()->get();
		}
		
		
		return $this->view('ccd/session/register_session', [
				'errorMsg' => \Session::getFlash('errorMsg'), 
				'errors' => \Session::getFlash('errors'),
				'post' =>  $post,
				'allInstructions' =>  $allInstructions,
				'allPassages' =>  $allPassages, 
				'edit' => 'true',
				'year_id' => $session_id, 'courseName' => $courseCode
		]);
		
	}  
	 
	function delete($courseCode, $session_id) {
		
		$obj = new \App\Models\Sessions();
		
		$success = $obj->where(TABLE_ID, '=', $session_id)->delete();
		
		if ($success){
			\Session::flash('success', 'Record deleted successfully');
		}else {
			\Session::flash('error', 'Delete failed');
		}
		
		redirect_(getAddr('ccd_all_sessions', $courseCode));
		
	} 
	
	function previewAllSesssionQuestions($courseCode, $session_id) { 
		 
		$obj = new \App\Models\Sessions();
		
		$sessionDetails = $obj->where(TABLE_ID, '=', $session_id)
		  ->with(['questions', 'instructions', 'passages', 'questions.topic'])->first();
		
		if (!$sessionDetails){
			return $this->view('layout', [ 'error' => 'No Record found' ]);
		}
		
		$allSessionQuestions = $sessionDetails['questions']; 
// 		$allSessionQuestions = $sessionDetails->questions()->get(); 

		$allPassages = $sessionDetails['passages'];
		$allInstructions = $sessionDetails['instructions'];
// 		$allPassages = $sessionDetails->passages()->get();
// 		$allInstructions = $sessionDetails->instructions()->get();

		if (!$allSessionQuestions->first()){
			return $this->view('layout', [ 'error' => 'No question recorded for this course. ' 
					. '<a href="' . getAddr('ccd_new_session_question', [$courseCode, $session_id])
					. '">Click here to start recording question</a>'
			]);
		}
		
		return $this->view('ccd/session/preview_session', [
				'year' => $sessionDetails[SESSION],
				'courseName' => $courseCode,
				'allCourseYearQuestions' =>  $allSessionQuestions,
				'year_id' => $session_id,
				'session' => $sessionDetails[SESSION], 
				'allPassages' => $allPassages,
				'allInstructions' => $allInstructions,
		]);
		
	}
	
	
	
}
