<?php
namespace App\Parser;

// Usage 
// $mcrypt = new MCrypt();

// $str = 'testing'; 
	//file_get_contents(APP_DIR . 'output/all_courses.json');
// Encrypt
// $encrypted = $mcrypt->encrypt($str);
// echo $encrypted . '<br />================================<br />';
// // Decrypt
// $decrypted = $mcrypt->decrypt($encrypted);
// echo $decrypted . '<br /><br />';

class MCrypt{
	
	// Key and IV must be 16 in number
	private $iv = '47jhS!+=%&E|:CN)';
	public $key = '!($&h*@|%_+JH7*#';
// 	private $iv = '47jhSKHD#&E|:CN)';
// 	public $key = '!($&hfh4w_+JH7*#';
	
	function __construct() {
		
	}
	
	function encrypt($str) 
	{
		$iv = $this->iv;
		$td = mcrypt_module_open('rijndael-128', '', 'cbc', $iv);
		mcrypt_generic_init($td, $this->key, $iv);
		$encrypted = mcrypt_generic($td, $str);
		
		mcrypt_generic_deinit($td);
		mcrypt_module_close($td);
		
		return bin2hex($encrypted);
		
	}
	
	function decrypt($code) {
		
		$code = $this->hex2bin($code); 
		$iv = $this->iv;
		
		$td = mcrypt_module_open('rijndael-128', '', 'cbc', $iv);
		mcrypt_generic_init($td, $this->key, $iv);
		
		$decrypted = mdecrypt_generic($td, $code);
		mcrypt_generic_deinit($td);
		mcrypt_module_close($td);
		
		return utf8_encode(trim($decrypted)); 
		
	}
	
	function hex2bin($hexData) {
		 
		$binData = '';
		$len = strlen($hexData); 
		
		for ($i = 0; $i < $len; $i += 2) {
			$binData .= chr(hexdec(substr($hexData, $i, 2)));
		}
		
		return $binData;
		
	}
	
}



