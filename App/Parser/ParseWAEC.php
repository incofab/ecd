<?php
namespace App\Parser;

class ParseWAEC
{
//     const INPUT_BASE_DIR = 'D:/Users/user/Desktop/players/Exam Driller/Subjects/';// biology/';
    const INPUT_BASE_DIR = 'C:/Users/USER PC/Desktop/Project Files/SSCE - HTML/';// biology/';
    
    const IMG_OUTPUT_BASE_DIR = 'C:/wamp64/www/mock/public/img/waec/min/'; 
    
    const WAEC_TAG = '_waec'; 
    
    const ALL_ADDITIONAL_TAG = [
        self::WAEC_TAG,
        \App\Parser\ParseWAECOral::ORAL_TAG, 
        \App\Parser\ParseWAECPractical::PRACTICAL_TAG,
        \App\Parser\ParseWAECTheory::THEORY_TAG,
        \App\Parser\ParseWAECProse::PROSE_TAG,
        \App\Parser\ParseWAECDrama::DRAMA_TAG,
    ]; 
    
    private $jsonFilesDir = APP_DIR . '../public/output/waec/';
    
    function __construct() 
    {
        
    }
    
    private $imgID = 0;
    
    private $subject = '';
    
    private $year = '';
    private $htmExt = '.min.htm';
    
    /**
     * Parse the HTML format of JAMB questions and save them in DB.
     * Copy their images to the image folder self::IMG_OUTPUT_BASE_DIR
     */
    function parse() 
    {
//         dDie((new \App\Parser\FileCript(new \App\Parser\MCrypt(), $this->jsonFilesDir)));
//         die('Dont run yet');
//         dDie((new \App\Parser\OutputJson())->generateJSON());
//         dDie($this->parseOptions(self::INPUT_BASE_DIR.'Geography/Geography_2001_Option.htm'));
        ini_set('max_execution_time', 480);  
        $i = 0;
        foreach (self::COURSE_TITLES as $subject => $courseTitle)
        {
            $subjectDir = self::INPUT_BASE_DIR . $this->subjectFolder($subject); 
            
            $this->subject = $subject;
            
            $subjectModel = new \App\Models\Courses();
            
            $subjectModel->insert([
                
                COURSE_CODE => $this->subject,
                
                COURSE_TITLE => array_get(self::COURSE_TITLES, $this->subject, $this->subject),
                
                DESCRIPTION => '',
            ]);
            $i++;
            echo "loops $i <br />";
            if(empty($subjectModel[COURSE_CODE]))
            {
                echo 'subjectModel empty, Probable cause: '.$subject.' already exists <br /><br />';
                
                continue;
            }
            
            /* For Theory*/
            $parserWaecTheory = null;
//             $parserWaecTheory = new \App\Parser\ParseWAECTheory($this->subject, $subjectModel[COURSE_TITLE], $this);
            /* // For Theory */
            /* For Practical*/
            $parserWaecPractical = null;
//             $parserWaecPractical = new \App\Parser\ParseWAECPractical($this->subject, $subjectModel[COURSE_TITLE], $this);
            /* // For Practical */
            $parserWaecOral = null;
            $parserWaecProse = null;
            $parserWaecDrama= null;
            
            $files = scandir($subjectDir);
            
            foreach ($files as $file)
            {
                if(substr($file, -10) != 'Answer.txt') continue;
                
                $prefix = substr($file, 0, -10);
                
                $answersfileName   = $subjectDir.'/'.$prefix.'Answer.txt';
                $optionsfileName   = $subjectDir.'/'.$prefix.'Option'.$this->htmExt;
                $questionsfileName = $subjectDir.'/'.$prefix.'Question'.$this->htmExt;
                
                $theoryQuestionsfileName = $subjectDir.'/'.$prefix.'Theory_Question'.$this->htmExt;
                $theoryAnswersfileName = $subjectDir.'/'.$prefix.'Theory_Answer'.$this->htmExt;
                
                $practicalQuestionsfileName = $subjectDir.'/'.$prefix.'Practical_Question'.$this->htmExt;
                $practicalAnswersfileName = $subjectDir.'/'.$prefix.'Practical_Answer'.$this->htmExt;

                $oralQuestionsfileName = $subjectDir.'/'.$prefix.'Oral_Question'.$this->htmExt;
                $oralOptionsfileName   = $subjectDir.'/'.$prefix.'Oral_Option'.$this->htmExt;
                $oralAnswersfileName   = $subjectDir.'/'.$prefix.'Oral_Answer.txt';
                
                $proseQuestionsfileName = $subjectDir.'/'.$prefix.'Prose_Question'.$this->htmExt;
                $proseAnswersfileName = $subjectDir.'/'.$prefix.'Prose_Answer'.$this->htmExt;
                
                $dramaQuestionsfileName = $subjectDir.'/'.$prefix.'Drama_Question'.$this->htmExt;
                $dramaAnswersfileName = $subjectDir.'/'.$prefix.'Drama_Answer'.$this->htmExt;
                
                $this->processSubjectYear($questionsfileName, $optionsfileName, $answersfileName);
                
                if(file_exists($theoryQuestionsfileName))
                {
                    if(!$parserWaecTheory)
                    {
                        $parserWaecTheory = new \App\Parser\ParseWAECTheory($this->subject, $subjectModel[COURSE_TITLE], $this);
                    }
                    $parserWaecTheory->parseTheory($theoryQuestionsfileName, $theoryAnswersfileName);
                }
                
                if(file_exists($practicalQuestionsfileName))
                {
                    if(!$parserWaecPractical)
                    {
                        $parserWaecPractical = new \App\Parser\ParseWAECPractical($this->subject, $subjectModel[COURSE_TITLE], $this);
                    }
                    $parserWaecPractical->parsePractical($practicalQuestionsfileName, $practicalAnswersfileName);
                }
                
                if(file_exists($oralQuestionsfileName))
                {
                    if(!$parserWaecOral)
                    {
                        $parserWaecOral = new \App\Parser\ParseWAECOral($this->subject, $subjectModel[COURSE_TITLE], $this);
                    }
                    $parserWaecOral->parseOral($oralQuestionsfileName, $oralAnswersfileName, $oralOptionsfileName);
                }
                
                if(file_exists($proseQuestionsfileName))
                {
                    if(!$parserWaecProse)
                    {
                        $parserWaecProse = new \App\Parser\ParseWAECProse($this->subject, $subjectModel[COURSE_TITLE], $this);
                    }
                    $parserWaecProse->parseProse($proseQuestionsfileName, $proseAnswersfileName);
                }
                
                if(file_exists($dramaQuestionsfileName))
                {
                    if(!$parserWaecDrama)
                    {
                        $parserWaecDrama = new \App\Parser\ParseWAECDrama($this->subject, $subjectModel[COURSE_TITLE], $this);
                    }
                    $parserWaecDrama->parseDrama($dramaQuestionsfileName, $dramaAnswersfileName);
                }

            }
            
            echo $subject.' Done <br /><br />';
        }
        
        die('Operation Complete');
    }
    
    private function processSubjectYear($questionsfileName, $optionsfileName, $answersfileName) 
    {
        $sujectDetail = explode('_', pathinfo($questionsfileName, PATHINFO_FILENAME));
        
        $this->year = $sujectDetail[1];
        
        $this->imgID = 0;
        
        $answers = $this->parseAnswers($answersfileName);
        
        $options = $this->parseOptions($optionsfileName);
        
        $questions = $this->parseQuestion($questionsfileName);
        
        $session = new \App\Models\Sessions();
        
        $session = $session->insert([
            COURSE_CODE => $this->subject,
            SESSION => $this->year,
            GENERAL_INSTRUCTIONS => 'Answer all questions',
        ]);
        
        if(empty($session[SESSION])) 
        {
            dDie("Failure on Acad Session - {$this->subject} of {$this->year}");
        }
                
        foreach ($questions as $questionNo => $question) 
        {
            $questionsModel = new \App\Models\Questions();
            
            if(empty($answers[$questionNo])) continue;

            if(empty($options[$questionNo])) 
            {
                dlog("questionNo = $questionNo");
                
                dDie("Failure on options[questionNo] - {$this->subject} of {$this->year}");
            }
            
            $thisOption = $options[$questionNo];
            
            $questionsArr = [
                COURSE_CODE => $this->subject,
                SESSION_ID  => $session[TABLE_ID],
                QUESTION_NO => $questionNo,
                QUESTION => empty($question['content']) ? "Null" : $question['content'],
                OPTION_A => $thisOption['A']['option-content'],
                OPTION_B => $thisOption['B']['option-content'],
                OPTION_C => isset($thisOption['C']) ? $thisOption['C']['option-content'] : null,
                OPTION_D => isset($thisOption['D']) ? $thisOption['D']['option-content'] : null,
                OPTION_E => isset($thisOption['E']) ? $thisOption['E']['option-content'] : null,
                ANSWER   => $answers[$questionNo]['answer'],
                ANSWER_META => null,
            ];
            
            $questionRet = $questionsModel->insert($questionsArr);
            
            if(empty($questionRet[SUCCESSFUL]))
            {
                dlog(array_merge($questionsArr, $questionRet));
                
                dDie($questionRet);
            }
        }
    }
    
    function parseAnswers($fileName)
    {
        $answers = file_get_contents($fileName);
        
        $answersArr = explode(PHP_EOL, $answers);
        
        $arr = [];
        
        foreach ($answersArr as $answer) 
        {
            if (empty($answer)) continue;
            
            $ansArr = explode('.', $answer);
            
            $questionNo = $ansArr[0];
            
            $ans = $ansArr[1];
            
            $arr[$questionNo] = [
                'answer' => $ans,
            ];
        }
        
        return $arr;
    }
    
    function parseOptions($fileName)
    {
        $dom = new \DOMDocument();
        
        $dom->loadHTMLFile($fileName);
        
        $this->handleImgs($dom, $fileName);
        
        /** @var \DOMNodeList $paragraphs */
        $paragraphs = $dom->getElementsByTagName('p');
        
        $arr = [];
        
        $content = '';
        
        $currentQuestionNoAndOption = null;
        
        $nextQuestionNoAndOption = null;
        
        /** @var \DOMNode $p */
        foreach ($paragraphs as $p)
        {
            $text = $p->nodeValue;
            
            $questionNoAndOption = $this->getQuestionNo($text); 
            
            if(!$questionNoAndOption)
            {
                $content .= $dom->saveHtml($p);
                
                continue;
            }
            
            $nextQuestionNoAndOption = $questionNoAndOption['question_no'];
            
            if(!$currentQuestionNoAndOption) 
            {
                $currentQuestionNoAndOption = $nextQuestionNoAndOption;
                
                $content = '';
                
                continue;
            }
            
            $questionNo = substr($currentQuestionNoAndOption, 0, -1);
            
            $optionLetter = substr($currentQuestionNoAndOption, -1);
            
            $arr[$questionNo][$optionLetter] = [
                'option-content' => $content,
            ];
            
            $currentQuestionNoAndOption = $nextQuestionNoAndOption;
            
            $content = $questionNoAndOption['text'];
        }
        
        $questionNo = substr($currentQuestionNoAndOption, 0, -1);
        
        $optionLetter = substr($currentQuestionNoAndOption, -1);
        
        $arr[$questionNo][$optionLetter] = [
            'option-content' => $content,
        ];
        
        return $arr;
    }
    
    function parseQuestion($fileName)
    {
        $arr = [];
        
        $dom = new \DOMDocument();
        
        $dom->loadHTMLFile($fileName);
        
        $this->handleImgs($dom, $fileName);
        
        /** @var \DOMNodeList $paragraphs */
        $paragraphs = $dom->getElementsByTagName('p');
        
        $content = '';
        
        $currentQuestionNo = null;
        
        $nextQuestionNo = null;
        
        /** @var \DOMNode $p */
        foreach ($paragraphs as $p)
        {
            $text = $p->nodeValue;
            
            $questionNo = $this->getQuestionNo($text); 
            
            if(!$questionNo) 
            {
                $content .= $dom->saveHtml($p);
                
                continue;
            }
            
            $nextQuestionNo = $questionNo['question_no'];
            
            if(!$currentQuestionNo)
            {
                $currentQuestionNo = $nextQuestionNo;
                
                $content = '';
                
                continue;
            }
            
            $arr[$currentQuestionNo] = [
                'content' => $content
            ];
            
            $currentQuestionNo = $nextQuestionNo;
            
            $content = $questionNo['text'];
        }
        
        $arr[$nextQuestionNo] = [
            'content' => $content
        ];
        
        return $arr; // die(json_encode($arr, JSON_PRETTY_PRINT));
    }
    
    function getQuestionNo($text) 
    {   
        $text = trim($text);

//         if(substr($text, 0, 3) != '/@Q') return null;
        $startPos = strpos($text, '/@Q');
        
        if($startPos === false) return null;
        
        $text = substr($text, $startPos + 3);
        
        $pos = strpos($text, '/');
        
        $questionNo = substr($text, 0, $pos);
        
        $otherText = substr($text, $pos+1);
        
        return  ['question_no' => $questionNo, 'text' => $otherText];
    }
    
    private function handleImgs($dom, $fileName) 
    {
        $imgs = $dom->getElementsByTagName('img');
        
        /** @var \DOMNode $img */
        foreach ($imgs as $img)
        {
            $src = $img->getAttribute('src');
            
            $imgExt = pathinfo($src, PATHINFO_EXTENSION);
            
            $imgName = pathinfo($fileName, PATHINFO_FILENAME);
            
            $this->imgID++;
            
            $imgSrc = "$imgName-00{$this->imgID}.$imgExt";
            
            $imgDir = self::IMG_OUTPUT_BASE_DIR . $this->subject.'/'.$this->year.'/';
            
            if(!file_exists($imgDir))mkdir($imgDir, 0777, true);
            
            $imgFile = self::INPUT_BASE_DIR.$this->subjectFolder($this->subject).'/'.$src;
            
            copy($imgFile, $imgDir . $imgSrc);
            
            $img->setAttribute('src', $imgSrc);
        }
    }
    
    function subjectFolder($subject)
    {
        return str_replace(self::WAEC_TAG, '', $subject);
    }
    
    function zCount() 
    {
        die('djdjdjdjdjdj');
        $courses = \App\Models\Courses::where(COURSE_CODE, 'LIKE', '%_waec%')
            ->with(['sessions'])
            ->get();
        $arr = [];
        foreach($courses as $course)
        {
//             dDie($course->toArray());
            $sessions = $course['sessions'];
//             $session = $course->sessions()->first();
            $zCountSessions = [];
            foreach ($sessions as $session) 
            {
                $questions = $session->questions()->get();
                $count = $questions->count();
//                 echo("Course Code = {$course[COURSE_CODE]}, session = {$session[SESSION]}, count = $count,  <br />");
                $zCountSessions[$session[SESSION]] = $count;
            }
            $arr[$course[COURSE_CODE]] = $zCountSessions;
        }
        $filename = 'C:/wamp64/www/mock/public/output/waec/zcount.json';
        file_put_contents($filename, json_encode($arr));
        echo '<br /> Operation ended <br />';
        dDie($arr);
//         dlog($msg)
    }
    
    const COURSE_TITLES = [
    /*
        'Account'.self::WAEC_TAG => 'Accounting',
        'Agriculture'.self::WAEC_TAG => 'Agriculture',
        'Biology'.self::WAEC_TAG => 'Biology',
        'Chemistry'.self::WAEC_TAG => 'Chemistry',
        'Civic'.self::WAEC_TAG => 'Civic',
        'Commerce'.self::WAEC_TAG => 'Commerce',
        'Computer'.self::WAEC_TAG => 'Computer',
        'CRK'.self::WAEC_TAG => 'Christian Religious Knowledge',
        'Economics'.self::WAEC_TAG => 'Economics',
        'Geography'.self::WAEC_TAG => 'Geography',
        'Government'.self::WAEC_TAG => 'Government',
        'Literature'.self::WAEC_TAG => 'Literature in English',
        'Mathematics'.self::WAEC_TAG => 'Mathematics',
        'Physics'.self::WAEC_TAG => 'Physics',
        'English'.self::WAEC_TAG => 'English Language',
    */
    /*
     * */
    ];
    
}







