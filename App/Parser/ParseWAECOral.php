<?php
namespace App\Parser;

class ParseWAECOral
{
    const INPUT_BASE_DIR = ParseWAEC::INPUT_BASE_DIR;
//     'C:/Users/USER PC/Desktop/Project Files/SSCE - HTML/';// biology/';
    
    const IMG_OUTPUT_BASE_DIR = ParseWAEC::IMG_OUTPUT_BASE_DIR;
    
    const ORAL_TAG = '_or';
    
    function __construct($subject, $subjectTitle, \App\Parser\ParseWAEC $parseWaec) 
    {
        $this->subject = $subject.self::ORAL_TAG;
        $this->subjectTitle = $subjectTitle;
        $this->parseWaec = $parseWaec;
        
        $this->init();
    }
    
    private $parseWaec;
    private $imgID = 0;
    
    private $subject = '';
    private $subjectTitle = '';
    
    private $year = '';
    
    private function init() 
    {
        $subjectModel = new \App\Models\Courses();
        
        $subjectModel->insert([
            
            COURSE_CODE => $this->subject,
            
            COURSE_TITLE => $this->subjectTitle.' Oral',
            
            DESCRIPTION => '',
        ]);
        
        if(empty($subjectModel[COURSE_CODE]))
        {
            echo 'subjectModel empty, Probable cause: '.$this->subject.' already exists <br /><br />';
        }
    }
    
    function parseOral($oralQuestionsfileName, $oralAnswersfileName, $oralOptionsfileName) 
    {
        $sujectDetail = explode('_', pathinfo($oralQuestionsfileName, PATHINFO_FILENAME));
        
        $this->year = $sujectDetail[1];
        
        $this->imgID = 0;
        
        $questions = $this->parseOralQuestion($oralQuestionsfileName);
        $answers = $this->parseOralAnswers($oralAnswersfileName);
        $options = $this->parseOralOptions($oralOptionsfileName);
        
        $session = new \App\Models\Sessions();
        
        $session = $session->insert([
            COURSE_CODE => $this->subject,
            SESSION => $this->year,
            GENERAL_INSTRUCTIONS => '',
        ]);
        
        if(empty($session[SESSION]))
        {
            dDie("Failure on Acad Session - {$this->subject} of {$this->year}");
        }
        
        foreach ($questions as $questionNo => $question)
        {
            $questionsModel = new \App\Models\Questions();
            
            if(empty($answers[$questionNo]['answer']))
            {
                dlog("No answer for {$this->subject} of {$this->year} - Question number $questionNo");
                
                continue;
            }
            
            if(empty($options[$questionNo]))
            {
                dlog("questionNo = $questionNo");
                
                dDie("Failure on options[questionNo] - {$this->subject} of {$this->year}");
            }
            
            $thisOption = $options[$questionNo];
            
            $questionsArr = [
                COURSE_CODE => $this->subject,
                SESSION_ID  => $session[TABLE_ID],
                QUESTION_NO => $questionNo,
                QUESTION => empty($question['content']) ? "Null" : $question['content'],
                OPTION_A => $thisOption['A']['option-content'],
                OPTION_B => $thisOption['B']['option-content'],
                OPTION_C => isset($thisOption['C']) ? $thisOption['C']['option-content'] : null,
                OPTION_D => isset($thisOption['D']) ? $thisOption['D']['option-content'] : null,
                OPTION_E => isset($thisOption['E']) ? $thisOption['E']['option-content'] : null,
                ANSWER   => $answers[$questionNo]['answer'],
                ANSWER_META => null,
            ];
            
            $questionRet = $questionsModel->insert($questionsArr);
            
            if(empty($questionRet[SUCCESSFUL]))
            {
                dlog(array_merge($questionsArr, $questionRet));
                
                dDie($questionRet);
            }
        }
        
    }
    
    private function parseOralOptions($fileName)
    {
        return $this->parseWaec->parseOptions($fileName);
    }
    
    private function parseOralAnswers($fileName)
    {
        return $this->parseWaec->parseAnswers($fileName);
    }
        
    private function parseOralQuestion($fileName)
    {
        return $this->parseWaec->parseQuestion($fileName);
    }
    
    private function handleImgs($dom, $fileName) 
    {
        $imgs = $dom->getElementsByTagName('img');
        
        /** @var \DOMNode $img */
        foreach ($imgs as $img)
        {
            $src = $img->getAttribute('src');
            
            $imgExt = pathinfo($src, PATHINFO_EXTENSION);
            
            $imgName = pathinfo($fileName, PATHINFO_FILENAME);
            
            $this->imgID++;
            
            $imgSrc = "$imgName-00{$this->imgID}.$imgExt";
            
            $imgDir = self::IMG_OUTPUT_BASE_DIR . $this->subject.'/'.$this->year.'/';
            
            if(!file_exists($imgDir))mkdir($imgDir, 0777, true);
            
            $imgFile = self::INPUT_BASE_DIR.$this->subjectFolder($this->subject).'/'.$src;
            
            copy($imgFile, $imgDir . $imgSrc);
            
            $img->setAttribute('src', $imgSrc);
        }
    }
    
    function subjectFolder($subject)
    {
        $subject = str_replace(ParseWAEC::WAEC_TAG, '', $subject);
        $subject = str_replace(self::ORAL_TAG, '', $subject);
        return $subject;
    }
}







