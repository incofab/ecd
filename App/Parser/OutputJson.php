<?php
namespace App\Parser;

class OutputJson
{
    private $toEncrpt;
    
    private $baseDir = APP_DIR . '../public/output/jamb/';
    
    function __construct($toEncrpt = false) 
    {
        $this->toEncrpt = $toEncrpt;
    }
    
    static function outputJamb()
    {
//         die('Dont run yet');
        ini_set('max_execution_time', 480);  
        dDie((new \App\Parser\OutputJson())->generateJSON());
    }
    
    function generateJSON() 
    {
    	$obj = (new \App\Models\Courses());
    	 
    	$allCourseDetails = $obj->all();
    	 
    	if (!$allCourseDetails->first()) die('No record found');
    	
    	$arr = [];
    	
    	foreach ($allCourseDetails as $CourseDetails) 
    	{    	    
    	    if($CourseDetails[COURSE_CODE] != 'Sweet16') continue;
    		$arr[] = [
    			
    		    TABLE_ID => $CourseDetails[TABLE_ID],
    		    
    			'course_name' => $CourseDetails[COURSE_CODE],
    		    
    			'course_fullname' => $CourseDetails[COURSE_TITLE],
    		    
    			DESCRIPTION => $CourseDetails[DESCRIPTION],
    		];
    		
    		$this->generateYearsJSON($CourseDetails[COURSE_CODE]);
    		
    		$this->generateSummaryJSON($CourseDetails[COURSE_CODE]);    		
    	}
    	
    	$filename = "{$this->baseDir}all_courses.json" ;
    	
    	file_put_contents($filename, json_encode($arr));
    	
    	echo('<br /><br /> Finally Done'); 
    }
    
    private function generateYearsJSON($courseName) 
    {
    	$obj = (new \App\Models\Sessions());
    	
    	$allSessions =  $obj->where(COURSE_CODE, '=', $courseName)->get();
    	
    	if (!$allSessions->first())
    	{
    		dlog("No record found for $courseName, create a new one");
    	    return; 
    	    
    		die("No record found for $courseName, create a new one");
    	}
    	
    	$arr = [];
    	
    	$yearIDs = [];
    	
    	/** @var \App\Models\Sessions $session */
    	foreach ($allSessions as $session)
    	{
    	    if(\App\Parser\Parse::startFrom2000)
    	    {
    	        if(2000 > $session[SESSION]) continue;
    	    }
    	    
    	    $per_question_instr = $session->instructions()->get();
    		
    	    $passages = $session->passages()->get();
    		
    		$arr[] = [
    				'year_id' => $session[TABLE_ID],
    		    
    				'year' => $session[SESSION],
    		    
    				CATEGORY => $session[CATEGORY],
    		    
    				GENERAL_INSTRUCTIONS => $session[GENERAL_INSTRUCTIONS],
    		    
    		       'per_question_instruction' => $this->formatPerQuestionInstructions($per_question_instr),
    		    
    		       'passages' => $this->formatPassages($passages),
    		];
    		
    		$yearIDs[] = $session[TABLE_ID];
    	}
    	
    	$folder = "{$this->baseDir}$courseName/";
    	
    	// Create the directory if it doesnt exists
    	if(!file_exists($folder)) mkdir($folder, 0777, true);
    	
    	$filename = $folder . strtolower(str_replace(' ', '_', $courseName)) . '.json';
    	
    	file_put_contents($filename, json_encode($arr));
    	
    	echo('<br />generateCourseYearJSON method Done'); 
    	
    	$this->generateCryptYearsQuestionsJSON($courseName, $yearIDs);
    } 
     
    private function generateCryptYearsQuestionsJSON($courseName, $yearIDs) 
    {
    	$obj = (new \App\Models\Questions());
    	
    	foreach ($yearIDs as $year_id) 
    	{
    	    $allCourseYearQuestions =  $obj->where(SESSION_ID, '=', $year_id)->get();
    		
    		if (!$allCourseYearQuestions->first())
    		{
    			continue; 
    		}
    	    
    		$yearquestions = [];
    		
    		foreach ($allCourseYearQuestions as $courseYear) 
    		{
    			$yearquestions[] = [
    					QUESTION_NO => $courseYear[QUESTION_NO],
    					QUESTION => $courseYear[QUESTION],
    					OPTION_A => $courseYear[OPTION_A],
    					OPTION_B => $courseYear[OPTION_B],
    					OPTION_C => $courseYear[OPTION_C],
    					OPTION_D => $courseYear[OPTION_D],
    					OPTION_E => $courseYear[OPTION_E],
    					ANSWER => $courseYear[ANSWER],
    					ANSWER_META => $courseYear[ANSWER_META],
    			];
    		}
    		
    		$folder = "{$this->baseDir}$courseName/";
    		
    		// Create the directory if it doesnt exists
    		if(!file_exists($folder)) mkdir($folder, 0777, true);
    	
    		$filename = $folder 
    				. strtolower(str_replace(' ', '_', $courseName) . "_$year_id").'.json';
    		
    		file_put_contents($filename, json_encode($yearquestions));
    	}
    	
    	echo('<br /> generateYearsQuestionsJSON method Done');
    } 
    
    private function formatPerQuestionInstructions($allInstructions) 
    {
        $instr = [];
        $from = [];
        $to = [];
        
        /** @var \App\Models\Instructions $instruction */
        foreach ($allInstructions as $instruction) 
        {
            $instr[] =  $instruction[INSTRUCTION];
            
            $from[] =  $instruction[FROM_];
            
            $to[] =  $instruction[TO_];
        }
        
        return [
            'per_question_instruction' => $instr,
            
            FROM_ => $from,
            
            TO_ => $to,
        ];
    }
    
    private function formatPassages($allPassages)  
    {
        $passagesArr = []; 
        $from = [];
        $to = [];
        
        /** @var \App\Models\Passages $passage */
        foreach ($allPassages as $passage)  
        {
            $passagesArr[] =  $passage[PASSAGE];
            
            $from[] =  $passage[FROM_];
            
            $to[] =  $passage[TO_];
        }
        
        return [
            'per_question_instruction' => $passagesArr,
            
            FROM_ => $from,
            
            TO_ => $to,
        ];
    }
    
    private function generateSummaryJSON($courseName)
    {
        $obj = (new \App\Models\Summary());
        
        $allSummary =  $obj->where(COURSE_CODE, '=', $courseName)->get();
        
        if (!$allSummary->first()) return;
        
        $arr = [];
        
        foreach ($allSummary as $summaryChapter)
        {            
            $arr[] = [
                TABLE_ID => $summaryChapter[TABLE_ID],
                
                COURSE_CODE => $summaryChapter[COURSE_CODE],
                
                CHAPTER_NO => $summaryChapter[CHAPTER_NO],
                
                TITLE => $summaryChapter[TITLE],
                
                DESCRIPTION => $summaryChapter[DESCRIPTION],
                
                SUMMARY => $summaryChapter[SUMMARY],
            ];
        }
        
        $folder = "{$this->baseDir}/summary/$courseName/";
        
        // Create the directory if it doesnt exists
        if(!file_exists($folder)) mkdir($folder, 0777, true);
        
        $filename = $folder . strtolower(str_replace(' ', '_', $courseName)) . '.json';
        
        file_put_contents($filename, json_encode($arr));
        
        echo('<br />generateSummaryJSON method Done');
    }
}





