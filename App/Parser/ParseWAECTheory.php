<?php
namespace App\Parser;

class ParseWAECTheory
{
    const INPUT_BASE_DIR = ParseWAEC::INPUT_BASE_DIR;
//     'C:/Users/USER PC/Desktop/Project Files/SSCE - HTML/';// biology/';
    
    const IMG_OUTPUT_BASE_DIR = ParseWAEC::IMG_OUTPUT_BASE_DIR;
    
    const THEORY_TAG = '_th';
    
    function __construct($subject, $subjectTitle, \App\Parser\ParseWAEC $parseWaec) 
    {
        $this->subject = $subject.self::THEORY_TAG;
        $this->subjectTitle = $subjectTitle;
        $this->parseWaec = $parseWaec;
        
        $this->init();
    }
    
    private $parseWaec;
    private $imgID = 0;
    
    private $subject = '';
    private $subjectTitle = '';
    
    private $year = '';
    
    private function init() 
    {
        $subjectModel = new \App\Models\Courses();
        
        $subjectModel->insert([
            
            COURSE_CODE => $this->subject,
            
            COURSE_TITLE => $this->subjectTitle.' Theory',
            
            DESCRIPTION => '',
        ]);
        
        if(empty($subjectModel[COURSE_CODE]))
        {
            echo 'subjectModel empty, Probable cause: '.$this->subject.' already exists <br /><br />';
        }
    }
    
    function parseTheory($theoryQuestionsfileName, $theoryAnswersfileName) 
    {
        $sujectDetail = explode('_', pathinfo($theoryQuestionsfileName, PATHINFO_FILENAME));
        
        $this->year = $sujectDetail[1];
        
        $this->imgID = 0;
        
        $questions = $this->parseTheoryQuestion($theoryQuestionsfileName);
        $answers = $this->parseTheoryAnswers($theoryAnswersfileName);
        
        $session = new \App\Models\Sessions();
        
        $session = $session->insert([
            COURSE_CODE => $this->subject,
            SESSION => $this->year,
            GENERAL_INSTRUCTIONS => '',
        ]);
        
        if(empty($session[SESSION]))
        {
            dDie("Failure on Acad Session - {$this->subject} of {$this->year}");
        }
        
        foreach ($questions as $questionNo => $question)
        {
            $questionsModel = new \App\Models\Questions();
            
            $questionAnswer = array_get($answers, $questionNo);
            
            if(empty($questionAnswer))
            {
                dlog("Waec Theory - Answers of questionNo = $questionNo, session={$session[SESSION]} - {$session[COURSE_CODE]} not found ");
//                 continue;
            }
            
            $questionsArr = [
                COURSE_CODE => $this->subject,
                SESSION_ID  => $session[TABLE_ID],
                QUESTION_NO => $questionNo,
                QUESTION => empty($question['content']) ? "Null" : $question['content'],
                OPTION_A => '-',// $thisOption['A']['option-content'],
                OPTION_B => '-',// $thisOption['B']['option-content'],
                OPTION_C => '-',// isset($thisOption['C']) ? $thisOption['C']['option-content'] : null,
                OPTION_D => '-',// isset($thisOption['D']) ? $thisOption['D']['option-content'] : null,
                OPTION_E => '-',// isset($thisOption['E']) ? $thisOption['E']['option-content'] : null,
                ANSWER   => '-',// $answers[$questionNo]['answer'],
                ANSWER_META => array_get($questionAnswer, 'content', 'Answer not available'),
            ];
            
            $questionRet = $questionsModel->insert($questionsArr);
            
            if(empty($questionRet[SUCCESSFUL]))
            {
                dlog(array_merge($questionsArr, $questionRet));
                
                dDie($questionRet);
            }
        }
        
    }
    
    private function parseTheoryAnswers($fileName)
    {
        $arr = [];
        
        $dom = new \DOMDocument();
        
        $dom->loadHTMLFile($fileName);
        
        $this->handleImgs($dom, $fileName);
        
        /** @var \DOMNodeList $paragraphs */
        $paragraphs = $dom->getElementsByTagName('p');
        
        $content = '';
        
        $currentQuestionNo = null;
        
        $nextQuestionNo = null;
        
        /** @var \DOMNode $p */
        foreach ($paragraphs as $p)
        {
            $text = $p->nodeValue;
            
            $questionNo = $this->getQuestionNo($text);
            
            if(!$questionNo)
            {
                $content .= $dom->saveHtml($p);
                
                continue;
            }
            
            $nextQuestionNo = $questionNo['question_no'];
            
            if(!$currentQuestionNo)
            {
                $currentQuestionNo = $nextQuestionNo;
                
                $content = '';
                
                continue;
            }
            
            $arr[$currentQuestionNo] = [
                'content' => $content
            ];
            
            $currentQuestionNo = $nextQuestionNo;
            
            $content = $questionNo['text'];
        }
        
        $arr[$nextQuestionNo] = [
            'content' => $content
        ];
        
        return $arr; // die(json_encode($arr, JSON_PRETTY_PRINT));
    }
        
    private function parseTheoryQuestion($fileName)
    {
        $arr = [];
        
        $dom = new \DOMDocument();
        
        $dom->loadHTMLFile($fileName);
        
        $this->handleImgs($dom, $fileName);
        
        /** @var \DOMNodeList $paragraphs */
        $paragraphs = $dom->getElementsByTagName('p');
        
        $content = '';
        
        $currentQuestionNo = null;
        
        $nextQuestionNo = null;
        
        /** @var \DOMNode $p */
        foreach ($paragraphs as $p)
        {
            $text = $p->nodeValue;
            
            $questionNo = $this->getQuestionNo($text); 
            
            if(!$questionNo) 
            {
                $content .= $dom->saveHtml($p);
                
                continue;
            }
            
            $nextQuestionNo = $questionNo['question_no'];
            
            if(!$currentQuestionNo)
            {
                $currentQuestionNo = $nextQuestionNo;
                
                $content = '';
                
                continue;
            }
            
            $arr[$currentQuestionNo] = [
                'content' => $content
            ];
            
            $currentQuestionNo = $nextQuestionNo;
            
            $content = $questionNo['text'];
        }
        
        $arr[$nextQuestionNo] = [
            'content' => $content
        ];
        
        return $arr; // die(json_encode($arr, JSON_PRETTY_PRINT));
    }
    
    private function getQuestionNo($text) 
    {   
        $text = trim($text);

//         if(substr($text, 0, 3) != '/@Q') return null;
        $startPos = strpos($text, '/@Q');
        
        if($startPos === false) return null;
        
        $text = substr($text, $startPos + 3);
        
        $pos = strpos($text, '/');
        
        $questionNo = substr($text, 0, $pos);
        
        $otherText = substr($text, $pos+1);
        
        return  ['question_no' => $questionNo, 'text' => $otherText];
    }
    
    private function handleImgs($dom, $fileName) 
    {
        $imgs = $dom->getElementsByTagName('img');
        
        /** @var \DOMNode $img */
        foreach ($imgs as $img)
        {
            $src = $img->getAttribute('src');
            
            $imgExt = pathinfo($src, PATHINFO_EXTENSION);
            
            $imgName = pathinfo($fileName, PATHINFO_FILENAME);
            
            $this->imgID++;
            
            $imgSrc = "$imgName-00{$this->imgID}.$imgExt";
            
            $imgDir = self::IMG_OUTPUT_BASE_DIR . $this->subject.'/'.$this->year.'/';
            
            if(!file_exists($imgDir))mkdir($imgDir, 0777, true);
            
            $imgFile = self::INPUT_BASE_DIR.$this->subjectFolder($this->subject).'/'.$src;
            
            copy($imgFile, $imgDir . $imgSrc);
            
            $img->setAttribute('src', $imgSrc);
        }
    }
    
    function subjectFolder($subject)
    {
        $subject = str_replace(ParseWAEC::WAEC_TAG, '', $subject);
        $subject = str_replace(self::THEORY_TAG, '', $subject);
        return $subject;
    }
}







