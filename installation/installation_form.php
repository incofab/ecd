<?php
if (!defined('ROOT_FOLDER')){
    die('Wrong route');
}
require_once APP_DIR.'../installation/installation.php';

$obj = new Installation();

if(!empty($_POST))
{
    $db_name  = 'ecd';//$_POST['dbname'];
    $username = 'root';//$_POST['dbusername'];
    $password = '';//$_POST['dbpassword'];
    $title = 'Content Developer';
    
    $obj->runInstallation($db_name, $username, $password, $title);
    
    exit();
    return;
}
?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="<?= ROOT_FOLDER.'favicon.ico' ?>">
    <title>Fresh Installation</title>
    <!-- Bootstrap core CSS -->
    <link href="<?= ROOT_FOLDER.'public/lib/bootstrap4/css/bootstrap.min.css'?>" rel="stylesheet">
	<script type="text/javascript" src="<?= ROOT_FOLDER.'public/lib/jquery.min.js' ?>"></script>
  </head>
  <body>
    <header>
      <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
        <a class="navbar-brand" href="#">Fresh Installation</a>
      </nav>
    </header>
	<br /><br />
	<br /><br />
    <div class="container-fluid">
    	<div class="row">
    		<div class="offset-md-2 col-md-8 offset-sm-3 col-sm-6">
				<div class="card">
					<div class="card-header"><div class="h3"><b>Examdriller Content Developer</b></div></div>
					<div class="card-body">
						<p class="card-text h3">Installation Page</p>
						<form action="" method="post">
							<div class="clearfix">
								<input type="hidden" name="fresh_install" value="true" />
								<button type="submit" class="btn btn-primary mt-4" style="float: right;" 
									onclick="return confirm('are you sure?')">Run Installations Now</button>
							</div>
						</form>
					</div>
					<div class="card-footer text-muted">
						<small><i>powered by <a href="mailto:incofabikenna@gmail.com">Inco Technologies Ltd</a></i></small>
					</div>
				</div>
			</div>
    	</div>
    </div>
	<script type="text/javascript" src="<?= ROOT_FOLDER.'public/lib/bootstrap4/js/bootstrap.bundle.min.js' ?>"></script>
  </body>
</html>
