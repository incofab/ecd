<?php

use Interop\Container\ContainerInterface;

return [
    
    
    
    \Session::class => function (ContainerInterface $c) { return new \Session(); },

    /*
    \App\Core\BrowserDetector::class => function (ContainerInterface $c) { return new \App\Core\BrowserDetector(); },
    \App\Core\CodeGenerator::class => function (ContainerInterface $c) { return new \App\Core\CodeGenerator(); },
    \App\Core\Emitter::class => function (ContainerInterface $c) { return new \App\Core\Emitter(); },
    \App\Core\ErrorCodes::class => function (ContainerInterface $c) { return new \App\Core\ErrorCodes(); },
    \App\Core\JWT::class => function (ContainerInterface $c) { return new \App\Core\JWT(); },
    \App\Core\Settings::class => function (ContainerInterface $c) { return new \App\Core\Settings(); },

    \Carbon\Carbon::class => function (ContainerInterface $c) { return new \Carbon\Carbon(); },
    
    
    
    
    
    \App\Models\Admin::class => function (ContainerInterface $c) { return new \App\Models\Admin(INJECTED); },
    \App\Models\PasswordReset::class => function (ContainerInterface $c) { return new \App\Models\PasswordReset(INJECTED); },
    \App\Models\Session::class => function (ContainerInterface $c) { return new \App\Models\Session(INJECTED); },

    */
    
    
];



















