<?php
namespace Bootstrap\Container;

use DI\ContainerBuilder;

class MyContainer 
{
    private static $sigletonDIContainer;
    
    function __construct() 
    {
        if(self::$sigletonDIContainer) return;
        
        $builder = new ContainerBuilder();
        
        $builder->useAnnotations(true);
        
        $builder->addDefinitions(APP_DIR . '../Bootstrap/Container/config/container_config.php');
        
        $container = $builder->build();
        
        self::$sigletonDIContainer = $container;
    }
    
    /**
     * @return \DI\Container
     */
    function getContainerInstance() 
    {
        return self::$sigletonDIContainer;
    }
    
}