<?php
use Illuminate\Database\Capsule\Manager as Capsule;
use Philo\Blade\Blade;



//Bootstrap database connections
$capsule = new Capsule;
$capsule->addConnection([
		'driver'    => 'mysql',
		'host'      => 'localhost',
		'database'  => DB_NAME,
		'username'  => DB_USERNAME,
		'password'  => DB_PASSWORD,
		'charset'   => 'utf8',
		'collation' => 'utf8_unicode_ci',
		'prefix'    => '',
], 'default');

$capsule->setEventDispatcher(new \Illuminate\Events\Dispatcher(new \Illuminate\Container\Container));
// Make this Capsule instance available globally via static methods... (optional)
$capsule->setAsGlobal();
// Setup the Eloquent ORM... (optional; unless you've used setEventDispatcher())
$capsule->bootEloquent();


require_once 'routes/routes.php';

return;

//Bootstrap the route
// if(!isset($dontRoute)) {
// 	$router = new Gears\Router();
// 	$router->routesPath = __DIR__ . '/../app/routes';
// // 	$router->routesPath = __DIR__ . '/../app/routes.php';
// 	$router->dispatch(); //This ends program execution, kills the scrip.
// }

