<?php

if(isset($dontRoute)) return;
    
global $routesArr;

$routesArr = require_once APP_DIR . '../Bootstrap/routes/names/routeNames.php';


$addr = rtrim(ADDR, '/');

/**
 * @var \FastRoute\Dispatcher
 */
$dispatcher = \FastRoute\simpleDispatcher(function (\FastRoute\RouteCollector $r)use ($routesArr, $addr) {

    /* Home Routes */
    $r->addRoute(['GET'], $addr.$routesArr['home'], ['\App\Controllers\Home\Home', 'index']);
    $r->addRoute(['GET'], $addr.$routesArr['home_message'].'[/{messageType}]', [ '\App\Controllers\Home\Home', 'message']);
    $r->addRoute(['GET'], $addr.$routesArr['home_developers'], [ '\App\Controllers\Home\Home', 'developers']);
    
    $r->addRoute(['POST','GET'], $addr.$routesArr['admin_login'], [ '\App\Controllers\Home\Login', 'adminLogin']);
    $r->addRoute(['POST','GET'], $addr.$routesArr['forgot_password'], [ '\App\Controllers\Home\Login', 'forgotPassword']);
    $r->addRoute(['POST','GET'], $addr.$routesArr['reset_password'], [ '\App\Controllers\Home\Login', 'passwordReset']);
  
    $r->addRoute(['GET'], $addr.$routesArr['home_how_it_works'], [ '\App\Controllers\Home\Home', 'howItWorks']);
    $r->addRoute(['GET'], $addr.$routesArr['home_privacy_policy'], [ '\App\Controllers\Home\Home', 'privacyPolicy']);
    $r->addRoute(['GET'], $addr.$routesArr['home_about_us'], [ '\App\Controllers\Home\Home', 'aboutUs']);
    $r->addRoute(['GET'], $addr.$routesArr['home_contact_us'], [ '\App\Controllers\Home\Home', 'contactUs']);
    $r->addRoute(['GET'], $addr.$routesArr['home_terms'], [ '\App\Controllers\Home\Home', 'terms']);
    
    
    $r->addRoute(['GET'], $addr.$routesArr['admin_dashboard'], [ '\App\Controllers\Admin\Admin', 'index']);
    $r->addRoute(['GET'], $addr.$routesArr['admin_all'], [ '\App\Controllers\Admin\Admin', 'all']);
    $r->addRoute(['POST','GET'], $addr.$routesArr['admin_add'], [ '\App\Controllers\Admin\Admin', 'addNew']);
    $r->addRoute(['POST','GET'], $addr.$routesArr['admin_edit'], [ '\App\Controllers\Admin\Admin', 'edit']);
    $r->addRoute(['POST','GET'], $addr.$routesArr['admin_delete'] . '/{table_id}', [ '\App\Controllers\Admin\Admin', 'delete']);
    $r->addRoute(['POST','GET'], $addr.$routesArr['admin_changepassword'], [ '\App\Controllers\Admin\Admin', 'changePassword']);
    $r->addRoute(['GET'], $addr.$routesArr['admin_logout'], [ '\App\Controllers\Admin\Admin', 'logout']);
    
    $r->addRoute(['GET'], $addr.$routesArr['admin_view_all_users'], [ '\App\Controllers\Admin\Users', 'viewAllUsers']);
    $r->addRoute(['GET'], $addr.$routesArr['admin_search_user'], [ '\App\Controllers\Admin\Users', 'searchUsers']);
    $r->addRoute(['POST','GET'], $addr.$routesArr['admin_send_user_broadcast'], [ '\App\Controllers\Admin\Users', 'sendBroadcastSMS']);
    $r->addRoute(['GET'], $addr.$routesArr['admin_export_users'], [ '\App\Controllers\Admin\Users', 'exportUsers']);
    $r->addRoute(['GET'], $addr.$routesArr['admin_view_user_profile'] . '/{table_id}', [ '\App\Controllers\Admin\Users', 'viewUserProfile']);
    $r->addRoute(['GET'], $addr.$routesArr['admin_user_suspend'] . '/{table_id}', [ '\App\Controllers\Admin\Users', 'suspendUser']);
    $r->addRoute(['GET'], $addr.$routesArr['admin_user_unsuspend'] . '/{table_id}', [ '\App\Controllers\Admin\Users', 'unSuspendUser']);
    $r->addRoute(['GET'], $addr.$routesArr['admin_delete_user'] . '/{table_id}', [ '\App\Controllers\Admin\Users', 'deleteUser']);
    
    $r->addRoute(['GET'], $addr.$routesArr['admin_user_withdrawals'] . '[/{username}]', [ '\App\Controllers\Admin\Withdrawals', 'listWithdrawals']);
    $r->addRoute(['GET'], $addr.$routesArr['admin_cancel_withdrawal_request'] . '/{tableID}', [ '\App\Controllers\Admin\Withdrawals', 'cancelWithdrawalRequest']);
    $r->addRoute(['GET'], $addr.$routesArr['admin_confirm_withdrawal_request'] . '/{username}/{tableID}', [ '\App\Controllers\Admin\Withdrawals', 'confirmWithdrawal']);
    
    $r->addRoute(['GET'], $addr.$routesArr['admin_bank_deposits']. '[/{username}]', [ '\App\Controllers\Admin\BankDeposits', 'listDeposits']);
    $r->addRoute(['GET'], $addr.$routesArr['admin_cancel_bank_deposit'] . '/{tableID}', [ '\App\Controllers\Admin\BankDeposits', 'cancelBankDeposit']);
    $r->addRoute(['GET'], $addr.$routesArr['admin_confirm_bank_deposit'] . '/{username}/{tableID}', [ '\App\Controllers\Admin\BankDeposits', 'creditUser']);
    $r->addRoute(['GET'], $addr.$routesArr['admin_delete_bank_deposit'] . '/{tableID}', [ '\App\Controllers\Admin\BankDeposits', 'deleteBankDeposit']);
    
    $r->addRoute(['GET'], $addr.$routesArr['admin_complaints'], [ '\App\Controllers\Admin\Complaints', 'getComplaints']);
    $r->addRoute(['GET'], $addr.$routesArr['admin_sort_complaints'], [ '\App\Controllers\Admin\Complaints', 'sortByCategory']);
    $r->addRoute(['GET'], $addr.$routesArr['admin_delete_complaint'] . '/{table_id}[/{username}]', [ '\App\Controllers\Admin\Complaints', 'deleteComplaint']);
    $r->addRoute(['GET'], $addr.$routesArr['admin_resolved_complaint'] . '/{table_id}[/{username}]', [ '\App\Controllers\Admin\Complaints', 'markAsResolved']);
    $r->addRoute(['GET'], $addr.$routesArr['admin_user_complaints'] . '/{username}', [ '\App\Controllers\Admin\Complaints', 'getUsersComplaints']);

    $r->addRoute(['POST','GET'], $addr.$routesArr['admin_send_sms'] . '[/{phoneNo}]', [ '\App\Controllers\Admin\Admin', 'sendSMS']);
    $r->addRoute(['POST','GET'], $addr.$routesArr['admin_send_notification'] . '/{username}', [ '\App\Controllers\Admin\Admin', 'sendNotification']);
    
    $r->addRoute(['POST','GET'], $addr.$routesArr['admin_install_courses'].'[/{courseCode}]', [ \App\Controllers\CCD\UploadContent::class, 'installCourse']);
    $r->addRoute(['POST','GET'], $addr.$routesArr['admin_uninstall_course'].'/{courseCode}', [ \App\Controllers\CCD\UploadContent::class, 'unInstallCourse']);
    $r->addRoute(['GET'], $addr.$routesArr['admin_export_content'].'[/{courseCode}]', [ \App\Controllers\CCD\UploadContent::class, 'exportCourse']);
    
    /* CCD */
    // View all the registered courses without need to log in
    $r->addRoute(['GET'], $addr.$routesArr['ccd_home'], [ \App\Controllers\CCD\Courses::class, 'viewAll']);
    $r->addRoute(['POST'], $addr.$routesArr['ccd_upload_question_images'].'/{courseCode}/{year_id}', [ \App\Controllers\CCD\Home::class, 'uploadImage']);
    $r->addRoute(['GET'], $addr.$routesArr['ccd_all_courses'], [ \App\Controllers\CCD\Courses::class, 'viewAll']);
    $r->addRoute(['POST','GET'], $addr.$routesArr['ccd_register_course'], [ \App\Controllers\CCD\Courses::class, 'registerCourses']);
    $r->addRoute(['POST','GET'], $addr.$routesArr['ccd_edit_course'].'/{id}', [ \App\Controllers\CCD\Courses::class, 'editCourse']);
    $r->addRoute(['POST','GET'], $addr.$routesArr['ccd_delete_course'].'/{id}', [ \App\Controllers\CCD\Courses::class, 'delete']);
    
    $r->addRoute(['POST','GET'], $addr.$routesArr['ccd_add_topic'].'/{courseCode}', [ \App\Controllers\CCD\Topic::class, 'addTopic']);
    $r->addRoute(['POST','GET'], $addr.$routesArr['ccd_edit_topic'].'/{id}', [ \App\Controllers\CCD\Topic::class, 'editTopic']);
    $r->addRoute(['POST','GET'], $addr.$routesArr['ccd_delete_topic'].'/{id}', [ \App\Controllers\CCD\Topic::class, 'delete']);
    
    $r->addRoute(['GET'], $addr.$routesArr['ccd_all_sessions'].'/{courseName}', [ \App\Controllers\CCD\Sessions::class, 'viewAllSessions']);
    $r->addRoute(['POST','GET'], $addr.$routesArr['ccd_register_session'].'/{courseName}', [ \App\Controllers\CCD\Sessions::class, 'registerSession']);
    $r->addRoute(['POST','GET'], $addr.$routesArr['ccd_edit_session'].'/{courseName}/{session_id}', [ \App\Controllers\CCD\Sessions::class, 'editSession']);
    $r->addRoute(['POST','GET'], $addr.$routesArr['ccd_delete_session'].'/{courseName}/{session_id}', [ \App\Controllers\CCD\Sessions::class, 'delete']);
    $r->addRoute(['GET'], $addr.$routesArr['ccd_preview_session'].'/{courseName}/{session_id}', [ \App\Controllers\CCD\Sessions::class, 'previewAllSesssionQuestions']);
    
    $r->addRoute(['GET'], $addr.$routesArr['ccd_all_session_questions'].'/{courseName}/{year_id}', [ \App\Controllers\CCD\Questions::class, 'viewAllQuestions']);
    $r->addRoute(['POST','GET'], $addr.$routesArr['ccd_new_session_question'].'/{courseName}/{year_id}[/{num}]', [ \App\Controllers\CCD\Questions::class, 'addNewQuestion']);
    $r->addRoute(['POST','GET'], $addr.$routesArr['ccd_edit_session_question'].'/{courseName}/{year_id}/{id}', [ \App\Controllers\CCD\Questions::class, 'editQuestion']);
    $r->addRoute(['POST','GET'], $addr.$routesArr['ccd_delete_session_question'].'/{courseName}/{year_id}/{id}', [ \App\Controllers\CCD\Questions::class, 'delete']);
    $r->addRoute(['GET'], $addr.$routesArr['ccd_preview_session_question'].'/{courseName}/{year_id}/{id}', [ \App\Controllers\CCD\Questions::class, 'previewSingleSessionQuestion']);
    
    $r->addRoute(['POST','GET'], $addr.$routesArr['ccd_new_session_question_api'], [ \App\Controllers\CCD\Questions::class, 'addNewQuestionAPI']);

    $r->addRoute(['POST','GET'], $addr.$routesArr['ccd_new_chapter'].'/{courseName}', [ \App\Controllers\CCD\Summary::class, 'newChapter']);
    $r->addRoute(['GET'], $addr.$routesArr['ccd_view_all_chapter'].'/{courseName}', [ \App\Controllers\CCD\Summary::class, 'viewAll']);
    $r->addRoute(['POST','GET'], $addr.$routesArr['ccd_edit_chapter'].'/{courseName}/{chapter_id}', [ \App\Controllers\CCD\Summary::class, 'editChapter']);
    $r->addRoute(['POST','GET'], $addr.$routesArr['ccd_delete_chapter'].'/{courseName}/{chapter_id}', [ \App\Controllers\CCD\Summary::class, 'delete']);
    $r->addRoute(['GET'], $addr.$routesArr['ccd_preview_chapter'].'/{courseName}/{chapter_id}', [ \App\Controllers\CCD\Summary::class, 'preview']);

    $r->addRoute(['POST','GET'], $addr.$routesArr['ccd_upload_content'], [ \App\Controllers\CCD\UploadContent::class, 'uploadContent']);
    $r->addRoute(['POST','GET'], $addr.$routesArr['ccd_installed_bonus_content'].'[/{which}]', [ \App\Controllers\CCD\UploadContent::class, 'installBonusContent']);

    
    /* // CCD */
    
    require_once APP_DIR . 'rough.php';
    
});
    
    // Fetch method and URI from somewhere
    $httpMethod = $_SERVER['REQUEST_METHOD'];
    
    $uri = $_SERVER['REQUEST_URI'];
    
    // Strip query string (?foo=bar) and decode URI
    if (false !== $pos = strpos($uri, '?')) 
    {
        $uri = substr($uri, 0, $pos);
    }
    
    $uri = rawurldecode($uri);
    
    $routeInfo = $dispatcher->dispatch($httpMethod, $uri);
    
    switch ($routeInfo[0]) 
    {
        case FastRoute\Dispatcher::NOT_FOUND:
//             require APP_DIR . '../resources/views/home/404.php';
            echo view('home/404');
            break;
            
        case FastRoute\Dispatcher::METHOD_NOT_ALLOWED:
            $allowedMethods = $routeInfo[1];
            echo '405 Method Not Allowed';
            break;
            
        case FastRoute\Dispatcher::FOUND:
            $handler = $routeInfo[1];
            $parameters = $routeInfo[2];
            $parameters = array_values($parameters);
//             dlog($uri); 
//             dlog($parameters); 
//             dDie($handler); 
//             call_user_func_array([new $handler[0], $handler[1]], $parameters);
            try { 
                $container = (new \Bootstrap\Container\MyContainer())->getContainerInstance();
                $content = $container->call($handler, $parameters);
                if(is_array($content)) $content = json_encode($content);
                echo $content;
            } catch (Exception $e) {
//                 dlog("Error in url $uri, SERVER['REQUEST_URI'] = {$_SERVER['REQUEST_URI']}");
//                 dlog("Error in file {$e->getFile()}, line = {$e->getLine()}");
//                 dlog($e->getTrace());
                throw $e;
            }
            break;
    }
