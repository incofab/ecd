<?php

$routesArr = [
    
    /* Login Routes */
    'admin_login' => '/admin/login',
    'forgot_password' => '/forgot_password',
    'reset_password' => '/reset_password',
    
    'home' => '/',
    'home_message' => '/message',
    'home_developers' => '/developers',
    'home_resend_activation' => '/resend-activation-code',
    'home_how_it_works' => '/how-it-works',
    'home_privacy_policy' => '/privacy-policy',
    'home_about_us' => '/about-us',
    'home_contact_us' => '/contact-us',
    'home_terms' => '/terms-and-conditions',
    
    /* Admin Routes */
    'admin_dashboard' => '/admin',
    'admin_all' => '/admin/all-admin-users',
    'admin_add' => '/admin/add-new-admin-user',
    'admin_edit' => '/admin/edit-admin-user',
    'admin_delete' => '/admin/delete-admin-user',
    'admin_changepassword' => '/admin/changepassword',
    'admin_logout' => '/admin/logout',
    
    'admin_view_all_users' => '/admin/all_users',
    'admin_search_user' => '/admin/search_users',
    'admin_send_user_broadcast' => '/admin/users/send-broadcast-sms',
    'admin_export_users' => '/admin/export_users',
    'admin_view_user_profile' => '/admin/user_profile',
    'admin_user_suspend' => '/admin/suspend_user',
    'admin_user_unsuspend' => '/admin/unsuspend_user',
    'admin_delete_user' => '/admin/delet_user',    
    
    'admin_view_all_centers' => '/admin/centers/all',
    'admin_view_add_center' => '/admin/centers/add',
    'admin_edit_center' => '/admin/centers/edit',
    'admin_view_center' => '/admin/centers/view-detail',
    'admin_center_suspend' => '/admin/centers/suspend',
    'admin_center_unsuspend' => '/admin/centers/unsuspend',
    'admin_delete_center' => '/admin/centers/delete',
    
    'admin_user_withdrawals' => '/admin/user/transactions/withdrawals',
    'admin_cancel_withdrawal_request' => '/admin/transaction/cancel-withdrawal-request',
    'admin_confirm_withdrawal_request' => '/admin/transaction/confirm-withdrawal-requests',
    
    'admin_deposits' => '/admin/transaction/airtime-deposits',
    
    'admin_bank_deposits' => '/admin/transaction/bank-deposits',
    'admin_cancel_bank_deposit' => '/admin/bank/cancel-deposits',
    'admin_confirm_bank_deposit' => '/admin/bank/confirm-deposit',
    'admin_delete_bank_deposit' => '/manage/backend/admin/dashboard/bank/delete-deposit',

    'admin_merchants' => '/admin/transaction/merchants',
    
    'admin_quick_cash_deposits' => '/admin/transaction/quick-cash-deposits',
    
    
    'admin_user_complaints' => '/admin/user-complaints',
    'admin_complaints' => '/admin/all-complaints',
    'admin_sort_complaints' => '/admin/sort-complaints',
    'admin_delete_complaint' => '/admin/delete-complaint',
    'admin_resolved_complaint' => '/admin/resolve-complaint',
    
    'admin_send_sms' => '/admin/send-sms',
    'admin_send_notification' => '/admin/send-notification',
    
    'admin_install_courses' => '/admin/install-courses',
    'admin_uninstall_course' => '/admin/uninstall-course',
    'admin_export_content' => '/admin/export-content', 	
    
    
    /* CCD */
    'ccd_home' => '/ccd/home',
    'ccd_upload_question_images' => '/ccd/home/upload/question/images',
    'ccd_all_courses' => '/ccd/all_courses',
    'ccd_register_course' =>'/ccd/register_course',
    'ccd_edit_course' =>'/ccd/edit_course',
    'ccd_delete_course' => '/ccd/delete_course',
    
    'ccd_add_topic' => '/ccd/topics/create-topic',
    'ccd_edit_topic' => '/ccd/topics/edit-topic',
    'ccd_delete_topic' => '/ccd/topics/delete-topic',
    
    'ccd_all_sessions' => '/ccd/view_all_sessions',
    'ccd_register_session' => '/ccd/register_session',
    'ccd_edit_session' => '/ccd/edit_session',
    'ccd_delete_session' => '/ccd/delete_session',
    'ccd_preview_session' => '/ccd/preview_session', // Views all the questions recorded for the year
    
    'ccd_all_session_questions' => '/ccd/all_session_questions',
    'ccd_new_session_question' => '/ccd/new_session_question',
    'ccd_edit_session_question' => '/ccd/edit_session_question',
    'ccd_delete_session_question' => '/ccd/delete_session_question',
    'ccd_preview_session_question' => '/ccd/preview_session_question', //Just preview a particular question
    'ccd_new_session_question_api' => '/ccd/api/new_session_question',
    
    'ccd_new_chapter' => '/ccd/new_chapter',
    'ccd_view_all_chapter' => '/ccd/view_all_chapter',
    'ccd_edit_chapter' => '/ccd/edit_chapter',
    'ccd_delete_chapter' => '/ccd/delete_chapter',
    'ccd_preview_chapter' => '/ccd/preview_chapter', 	
    
    'ccd_upload_content' => '/ccd/upload-content', 	
    'ccd_installed_bonus_content' => '/ccd/install-bonus-content', 	
    /* // CCD */
    
];


return $routesArr;
